
Name "Items Editor"

RequestExecutionLevel highest
SetCompressor /SOLID lzma
ShowInstDetails show

# General Symbol Definitions
!define REGKEY "SOFTWARE\$(^Name)"
!define VERSION 0.3.0.0
!define COMPANY "Items Editor ltd"
!define URL "https://itemseditor.com"

# MUI Symbol Definitions
#!define MUI_ICON "barcode.ico"
#!define MUI_WELCOMEFINISHPAGE_BITMAP "../share/pixmaps/nsis-wizard.bmp"
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_RIGHT
#!define MUI_HEADERIMAGE_BITMAP "../share/pixmaps/nsis-header.bmp"
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_STARTMENUPAGE_REGISTRY_ROOT HKLM
!define MUI_STARTMENUPAGE_REGISTRY_KEY ${REGKEY}
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME StartMenuGroup
!define MUI_STARTMENUPAGE_DEFAULTFOLDER ItemsEditor
!define MUI_FINISHPAGE_RUN $INSTDIR\ItemsEditor.exe
#!define MUI_FINISHPAGE_SHOWREADME $INSTDIR\Readme.txt

# Included files
!include Sections.nsh
!include MUI2.nsh
!include nsDialogs.nsh
!include winmessages.nsh
!include logiclib.nsh

# Variables
Var StartMenuGroup

# Installer pages
# Execution flow of installer windows
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
# Disabled for now. Use the bat
;Page custom mode_selection # Meeh's hack for installing and starting service.
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuGroup
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

# Uninstall pages
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

# Installer languages
!insertmacro MUI_LANGUAGE English


# Installer attributes
!define /date MyTIMESTAMP "%d.%m.%Y_%H.%M"

OutFile ./../release/ItemsEditorInstaller_${MyTIMESTAMP}.exe
InstallDir $PROGRAMFILES\ItemsEditor
CRCCheck on
XPStyle on
BrandingText " "
ShowInstDetails show
VIProductVersion 0.3.0.0
VIAddVersionKey ProductName ItemsEditor
VIAddVersionKey ProductVersion "${VERSION}"
VIAddVersionKey CompanyName "${COMPANY}"
VIAddVersionKey CompanyWebsite "${URL}"
VIAddVersionKey FileVersion "${VERSION}"
VIAddVersionKey FileDescription ""
VIAddVersionKey LegalCopyright ""
InstallDirRegKey HKCU "${REGKEY}" Path
ShowUninstDetails show


# Installer sections
Section -Main SEC0000
    SetOutPath $INSTDIR
    SetOverwrite on

    File /oname=ItemsEditor.exe ..\bin\ItemsEditor.exe
    File /oname=Qt5Core.dll ..\bin\Qt5Core.dll
    File /oname=Qt5Gui.dll ..\bin\Qt5Gui.dll
    File /oname=Qt5Network.dll ..\bin\Qt5Network.dll
    File /oname=Qt5Widgets.dll ..\bin\Qt5Widgets.dll
    File /oname=libgcc_s_dw2-1.dll ..\bin\libgcc_s_dw2-1.dll
    File /oname=libstdc++-6.dll ..\bin\libstdc++-6.dll
    File /oname=libwinpthread-1.dll ..\bin\libwinpthread-1.dll
    File /oname=libeay32.dll ..\bin\libeay32.dll
    File /oname=libssl32.dll ..\bin\libssl32.dll
    File /oname=ssleay32.dll ..\bin\ssleay32.dll

    File /oname=app.png ..\bin\app.png
    File /oname=category.png ..\bin\category.png
    File /oname=folder.png ..\bin\folder.png
    File /oname=item.png ..\bin\item.png

    # CreateDirectory $INSTDIR\platforms
    SetOutPath $INSTDIR\platforms
    File /oname=qwindows.dll ..\bin\platforms\qwindows.dll

    CreateDirectory $SMPROGRAMS\$StartMenuGroup
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\ItemsEditor.lnk" $INSTDIR\ItemsEditor.exe
SectionEnd

Section -post SEC0001
    WriteRegStr HKCU "${REGKEY}" Path $INSTDIR
    SetOutPath $INSTDIR
    WriteUninstaller $INSTDIR\uninstall.exe
    !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    CreateDirectory $SMPROGRAMS\$StartMenuGroup
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\ItemsEditor.lnk" $INSTDIR\ItemsEditor.exe
    CreateShortcut "$SMPROGRAMS\$StartMenuGroup\Uninstall ItemsEditor.lnk" $INSTDIR\uninstall.exe
    !insertmacro MUI_STARTMENU_WRITE_END
    WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayName "$(^Name)"
    WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayVersion "${VERSION}"
    WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" Publisher "${COMPANY}"
    WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" URLInfoAbout "${URL}"
    WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" DisplayIcon $INSTDIR\uninstall.exe
    WriteRegStr HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" UninstallString $INSTDIR\uninstall.exe
    WriteRegDWORD HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoModify 1
    WriteRegDWORD HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)" NoRepair 1
SectionEnd

# Macro for selecting uninstaller sections
!macro SELECT_UNSECTION SECTION_NAME UNSECTION_ID
    Push $R0
    ReadRegStr $R0 HKCU "${REGKEY}\Components" "${SECTION_NAME}"
    StrCmp $R0 1 0 next${UNSECTION_ID}
    !insertmacro SelectSection "${UNSECTION_ID}"
    GoTo done${UNSECTION_ID}
next${UNSECTION_ID}:
    !insertmacro UnselectSection "${UNSECTION_ID}"
done${UNSECTION_ID}:
    Pop $R0
!macroend


# Uninstaller sections
Section -un.Main UNSEC0000

SectionEnd

Section -un.post UNSEC0001
    Delete $INSTDIR\ItemsEditor.exe
    Delete $INSTDIR\Qt5Core.dll
    Delete $INSTDIR\Qt5Gui.dll
    Delete $INSTDIR\Qt5Network.dll
    Delete $INSTDIR\Qt5Widgets.dll
    Delete $INSTDIR\libstdc++-6.dll
    Delete $INSTDIR\libwinpthread-1.dll
    Delete $INSTDIR\libgcc_s_dw2-1.dll
    Delete $INSTDIR\libeay32.dll
    Delete $INSTDIR\libssl32.dll
    Delete $INSTDIR\ssleay32.dll
    
    Delete $INSTDIR\app.png
    Delete $INSTDIR\category.png
    Delete $INSTDIR\folder.png
    Delete $INSTDIR\item.png
    
    RMDir /r $INSTDIR\platforms
    DeleteRegValue HKCU "${REGKEY}\Components" Main
    
    DeleteRegKey HKCU "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\$(^Name)"
    Delete "$SMPROGRAMS\$StartMenuGroup\Uninstall ItemsEditor.lnk"
    Delete "$SMPROGRAMS\$StartMenuGroup\ItemsEditor.lnk"
    Delete $INSTDIR\uninstall.exe
    DeleteRegValue HKCU "${REGKEY}" StartMenuGroup
    DeleteRegValue HKCU "${REGKEY}" Path
    DeleteRegKey /IfEmpty HKCU "${REGKEY}\Components"
    DeleteRegKey /IfEmpty HKCU "${REGKEY}"
    RmDir $SMPROGRAMS\$StartMenuGroup
    RmDir $INSTDIR
    Push $R0
    StrCpy $R0 $StartMenuGroup 1
    StrCmp $R0 ">" no_smgroup
no_smgroup:
    Pop $R0
SectionEnd

# Installer functions
Function .onInit
    InitPluginsDir
    !insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd

# Uninstaller functions
Function un.onInit
    ReadRegStr $INSTDIR HKCU "${REGKEY}" Path
    !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuGroup
    !insertmacro SELECT_UNSECTION Main ${UNSEC0000}
    !insertmacro MUI_UNGETLANGUAGE
FunctionEnd


