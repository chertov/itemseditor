#include "DescriptionEditor.hpp"

DescriptionEditor::DescriptionEditor(BaseProps *props,
                                     QWidget *parent) : QDialog(parent) {
    ui.setupUi(this);

    Qt::WindowFlags flags = windowFlags();
    flags |= Qt::WindowMaximizeButtonHint;
    flags |= Qt::WindowContextHelpButtonHint;
    setWindowFlags( flags );

    switch (props->Type()) {
    case FOLDER: {
        setWindowTitle(QString("Редактирование описания папки \"") +
                       QString::fromStdString(props->GetName()) +
                       QString("\""));
        break; }
    case CATEGORY: {
        setWindowTitle(QString("Редактирование описания категории \"") +
                       QString::fromStdString(props->GetName()) +
                       QString("\""));
        break; }
    case ITEM: {
        setWindowTitle(QString("Редактирование описания позиции \"") +
                       QString::fromStdString(props->GetName()) +
                       QString("\""));
        break; }
    }

    ui.fullDescriptionEdit->setPlainText(QString::fromStdString(props->GetDescription()));
    ui.shortDescriptionEdit->setPlainText(QString::fromStdString(props->GetShortDescription()));

}

DescriptionEditor::~DescriptionEditor() {

}

bool DescriptionEditor::FullDescriptionValid() {
    return true;
}
bool DescriptionEditor::ShortDescriptionValid() {
    return true;
}

QString DescriptionEditor::FullDescription() {
    return ui.fullDescriptionEdit->toPlainText();
}
QString DescriptionEditor::ShortDescription() {
    return ui.shortDescriptionEdit->toPlainText();
}

void DescriptionEditor::LivePreviewUpdate() {

}
