#pragma once
#include "ui_DescriptionEditor.h"

#include <QtWidgets/QDialog>

#include "collection/collections.hpp"

class DescriptionEditor : public QDialog{
    Q_OBJECT

public:
    DescriptionEditor(BaseProps *props, QWidget *parent = 0);
    ~DescriptionEditor();

    bool FullDescriptionValid();
    bool ShortDescriptionValid();

    QString FullDescription();
    QString ShortDescription();

private slots:
    void LivePreviewUpdate();

private:
    Ui::DescriptionEditor ui;
};

