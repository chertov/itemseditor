#pragma once
#include <sstream>


#define LOG LoggerSingleton::Instance() << std::endl << __FILE__ << ":" << __LINE__ << "    " << __FUNCTION__ << "   "
#define LOGGER LoggerSingleton::Instance()
#define LOGSTR LoggerSingleton::Instance().str()
class LoggerSingleton : public std::stringstream {
public:
    static LoggerSingleton& Instance() {
        static LoggerSingleton instance;
        return instance;
    }
    LoggerSingleton();
    ~LoggerSingleton();
};
