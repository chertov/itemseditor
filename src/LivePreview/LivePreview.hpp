#pragma once
#include "ui_LivePreview.h"

#include <QtWidgets/QWidget>

#include "collection/collections.hpp"

class LivePreview : public QWidget{
    Q_OBJECT

public:
    LivePreview(BaseProps *props, QWidget *parent = 0);
    ~LivePreview();
    
    void Update();

private:
    Ui::LivePreview ui;
};

