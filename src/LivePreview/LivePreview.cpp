#include "LivePreview.hpp"

LivePreview::LivePreview(BaseProps *props, QWidget *parent) : QWidget(parent) {
    ui.setupUi(this);

//    Qt::WindowFlags flags = windowFlags();
//    flags |= Qt::Window;
//    //flags |= Qt::Popup;
////    flags |= Qt::WindowMaximizeButtonHint;
////    flags |= Qt::WindowContextHelpButtonHint;
//    setWindowFlags( flags );

    switch (props->Type()) {
    case FOLDER: {
        setWindowTitle(QString("Предпросмотр папки \"") +
                       QString::fromStdString(props->Name) +
                       QString("\""));
        break; }
    case CATEGORY: {
        setWindowTitle(QString("Предпросмотр категории \"") +
                       QString::fromStdString(props->Name) +
                       QString("\""));
        break; }
    case ITEM: {
        setWindowTitle(QString("Предпросмотр позиции \"") +
                       QString::fromStdString(props->Name) +
                       QString("\""));
        break; }
    }

    ui.webView->load(QUrl("http://nordelectra.ru"));
}

LivePreview::~LivePreview() {
}

void LivePreview::Update() {
}
