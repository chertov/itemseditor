#pragma once

#include <QTreeWidgetItem>
#include "collection/collections.hpp"

//enum ModelType {
//	FOLDER,
//	CATEGORY,
//	ITEM
//};

class ModelItem : public QTreeWidgetItem {

public:
    ModelItem(ModelType type) {
        //void* ptr;
        setData(0, Qt::UserRole, QVariant(type));
        //setData(1, Qt::UserRole, QVariant::fromValue((void *)ptr));
		if (type == FOLDER) {
            setIcon(0,QIcon("folder.png"));
		}
		if (type == CATEGORY) {
            setIcon(0,QIcon("category.png"));
		}
	}

    ModelType Type() { return static_cast<ModelType>(data(0, Qt::UserRole).toInt()); }

    Folder* FolderPtr() { return reinterpret_cast<Folder*>(data(1, Qt::UserRole).value<void*>()); }
    Category* CategoryPtr() { return reinterpret_cast<Category*>(data(1, Qt::UserRole).value<void*>()); }
    Item* ItemPtr() { return reinterpret_cast<Item*>(data(1, Qt::UserRole).value<void*>()); }
};
