#pragma once
#include <QAbstractListModel>
#include "collection/collections.hpp"

class EnumModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit EnumModel(QObject *parent = 0);
    ~EnumModel();

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void emitLayoutAboutToBeChanged() { emit layoutAboutToBeChanged(); }
    void emitLayoutChanged() { emit layoutChanged(); }

    Qt::DropActions supportedDragActions() const;
    Qt::DropActions supportedDropActions() const;

    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

//    bool insertRows(int position, int rows, const QModelIndex &parent);
//    bool removeRows(int position, int rows, const QModelIndex &parent);
    bool NewEnumValue();

    void SetEnum(Enum *enum_ptr);

    Enum *enum_obj = 0;
private:
    enum Columns
    {
        RamificationColumn,
        NameColumn = RamificationColumn,
        ColumnCount
    };

    QVariant nameData(const EnumValue &enumValue, int role) const;
};
