#pragma once
#include "ui_PropertyEditor.h"

#include <QtWidgets/QDialog>

#include "collection/collections.hpp"

#include "EnumModel.hpp"

class PropertyEditor : public QDialog
{
    Q_OBJECT

public:
    PropertyEditor(const IdType property_id, QWidget *parent = 0);
    ~PropertyEditor();

    EnumModel *enumModel;
    QListView *enumListView;

private:
    Property *property;

    Ui::PropertyEditor ui;
    bool init = false;

public slots:
    void addEnum();
    void removeEnum();
    void enumSelectionChanged();
    void changeData();
};

