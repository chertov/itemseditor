#include "PropertyEditor.hpp"

#include <iostream>
#include <fstream>
#include <QDesktopWidget>

#include "collection/collections.hpp"
#include "exception.hpp"

PropertyEditor::PropertyEditor(const IdType property_id, QWidget *parent) : QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);

    this->property = DB.PropertyById(property_id);
    enumModel = new EnumModel();
    enumListView = ui.enumListView;
    enumListView->setModel(enumModel);
    connect(enumListView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(enumSelectionChanged()));

    ui.nameEdit->setText(QString::fromStdString(property->GetName()));
    ui.showInSearchCheckBox->setChecked(property->GetShowInSearch());
    ui.showOnSiteCheckBox->setChecked(property->GetShowOnSite());

    ui.enumBox->setHidden(true);
    switch (property->GetType()) {
    case PROPERTY_BOOL:
        ui.radioButton_bool->setChecked(true);
        break;
    case PROPERTY_INT:
        ui.radioButton_int->setChecked(true);
        break;
    case PROPERTY_REAL:
        ui.radioButton_real->setChecked(true);
        break;
    case PROPERTY_ENUM: {
        enumModel->SetEnum(property->GetEnumPtr());
        ui.radioButton_enum->setChecked(true);
        ui.enumBox->setHidden(false);
        break;
        }
    }
    init = true;
    enumSelectionChanged();
    changeData();
}

PropertyEditor::~PropertyEditor() { }

//void PropertyEditor::addEnum() {
//    enumModel->insertRow(-1);
//}
//void PropertyEditor::removeEnum() {
//    QModelIndexList indexies = enumListView->selectionModel()->selectedIndexes();
//    for (int i = 0; i < indexies.size(); ++i) {
//        enumModel->removeRow(indexies[i].row());
//    }
//}
void PropertyEditor::addEnum() {
    enumModel->NewEnumValue();
    enumSelectionChanged();
}
void PropertyEditor::removeEnum() {
//    QModelIndexList indexies = enumListView->selectionModel()->selectedIndexes();
//    enumModel->emitLayoutAboutToBeChanged();
//    for (int i = indexies.size(); i --> 0; ) {
//        if (indexies[i].row() < 0) continue;
//        EnumList enums = property.GetEnums();
//        if (indexies[i].row() >= enums.size()) continue;
//        enums.erase(enums.begin() + indexies[i].row());
//        property.SetEnums(enums);
//    }
//    enumModel->emitLayoutChanged();
//    enumSelectionChanged();
    {
        ExceptionSaver ss; ss << "PropertyEditor::removeEnum: template";
    }
}
void PropertyEditor::enumSelectionChanged() {
    QModelIndexList indexies = enumListView->selectionModel()->selectedIndexes();
    if (indexies.size() > 0)
        ui.removeEnumButton->setEnabled(true);
    else
        ui.removeEnumButton->setEnabled(false);
}
void PropertyEditor::changeData() {
    if (!init) return;

    ui.enumBox->setHidden(true);
    property->SetName(ui.nameEdit->text().toStdString());
    property->SetShowInSearch(ui.showInSearchCheckBox->isChecked());
    property->SetShowOnSite(ui.showOnSiteCheckBox->isChecked());

    if(ui.radioButton_bool->isChecked()) property->SetType(PROPERTY_BOOL);
    if(ui.radioButton_int->isChecked()) property->SetType(PROPERTY_INT);
    if(ui.radioButton_real->isChecked()) property->SetType(PROPERTY_REAL);
    if(ui.radioButton_enum->isChecked()) {
        property->SetType(PROPERTY_ENUM);
        enumModel->SetEnum(property->GetEnumPtr());
        ui.enumBox->setHidden(false);
    }
    QApplication::processEvents();
    adjustSize();
    QApplication::processEvents();
    const QRect screen = QApplication::desktop()->screenGeometry();
    move( screen.center() - this->rect().center() );
}
