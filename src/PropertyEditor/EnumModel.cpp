#include "EnumModel.hpp"
#include <algorithm>
#include <QDebug>
#include <QMimeData>
#include <QIcon>
#include "collection/collections.hpp"
#include "exception.hpp"

EnumModel::EnumModel(QObject *parent) : QAbstractListModel(parent) {
}

EnumModel::~EnumModel() { }

void EnumModel::SetEnum(Enum *enum_ptr) {
    this->enum_obj = enum_ptr;
    resetInternalData();
}

int EnumModel::rowCount(const QModelIndex &) const {
    if(enum_obj == 0) return 0;
    return enum_obj->GetValuesSize();
}

int EnumModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return ColumnCount;
}

QVariant EnumModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return QVariant();
    if (enum_obj == 0) return QVariant();
    if (index.row() >= enum_obj->GetValuesSize()) return QVariant();

    EnumValue enumValue = enum_obj->GetValueByIndex(index.row());
    switch (index.column()) {
    case NameColumn:
        return nameData(enumValue, role);
    default:
        break;
    }
    return QVariant();
}

bool EnumModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (enum_obj == 0) return false;
    if (index.isValid() && role == Qt::EditRole) {
        EnumValue enumValue = enum_obj->GetValueByIndex(index.row());
        enumValue.SetValue(value.toString().toStdString());
        enum_obj->UpsertValue(enumValue);

        emit dataChanged(index, index);
        return true;
    }
    return false;
}


QVariant EnumModel::nameData(const EnumValue &enumValue, int role) const {
    switch (role) {
    case Qt::EditRole:
        return QVariant(QString::fromStdString(enumValue.GetValue()));
    case Qt::DisplayRole:
        return QVariant(QString::fromStdString(enumValue.GetValue()));
    default:
        return QVariant();
    }
    Q_UNREACHABLE();
}

Qt::ItemFlags EnumModel::flags(const QModelIndex &index) const {
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEnabled | Qt::ItemIsEditable | flags;
}

Qt::DropActions EnumModel::supportedDragActions() const {
    return Qt::MoveAction;
}
Qt::DropActions EnumModel::supportedDropActions() const {
    return Qt::MoveAction;
}

QMimeData *EnumModel::mimeData(const QModelIndexList &indexes) const {
    QMimeData *mimeData = QAbstractItemModel::mimeData(indexes);
    if (indexes.size() == 1) {
        QModelIndex index = indexes[0];
        mimeData->setProperty("source_index", QVariant(index.row()));
    }
    return mimeData;
}

bool EnumModel::dropMimeData(const QMimeData *mime, Qt::DropAction action, int row, int column, const QModelIndex &parent) {
    if (action == Qt::IgnoreAction) return true;
    if (enum_obj == 0) return true;

    int source_index = mime->property("source_index").toInt();
    emitLayoutAboutToBeChanged();
    bool res = enum_obj->MoveValue(source_index, row);
    emitLayoutChanged();

    return res;
}

bool EnumModel::NewEnumValue() {
    if (enum_obj == 0) return false;

    emitLayoutAboutToBeChanged();
    EnumValue enumValue;
    if(!enum_obj->UpsertValue(enumValue)) {
        ExceptionSaver ss; ss << "EnumModel::NewEnumValue: SetValues == false";
    }
    emitLayoutChanged();
    return true;
}


//bool EnumModel::insertRows(int position, int rows, const QModelIndex &parent) {
//    if (position < 0) position = enumList->size();
//    beginInsertRows(QModelIndex(), position, position+rows-1);
//    for (int row = 0; row < rows; ++row) {
//        EnumValue enumValue;
//        enumValue.Value = (QString("Новое значение ") + QString::number(row)).toStdString();
//        (*enumList).insert((*enumList).begin()+position, enumValue);
//    }
//    endInsertRows();
//    return true;
//}

//bool EnumModel::removeRows(int position, int rows, const QModelIndex &parent) {
//    if (position < 0) return false;
//    if (position+rows > enumList->size()) return false;
//    beginRemoveRows(QModelIndex(), position, position+rows-1);
//    for (int row = 0; row < rows; ++row)
//        (*enumList).erase((*enumList).begin()+row);
//    endRemoveRows();
//    return true;
//}
