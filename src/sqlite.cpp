#include "sqlite.hpp"
#include "assert.hpp"

#include <sstream>

bool writeBlob(
  sqlite3 *db,                  /* Database to insert data into */
  const std::string &table,
  const IdType &key,       /* Null-terminated key string */
  const char *data,     /* Pointer to blob of data */
  unsigned int size                     /* Length of data pointed to by zBlob */
) {
    std::stringstream ss;
    ss << "INSERT INTO " << table << "(key, value) VALUES(?, ?)";
    std::string sql = ss.str();
    sqlite3_stmt *pStmt;
    int rc = sqlite3_prepare(db, sql.c_str(), -1, &pStmt, 0);
    if( rc!=SQLITE_OK ){
        return false;
    }

    sqlite3_bind_int64(pStmt, 1, key);
    sqlite3_bind_blob(pStmt, 2, data, size, SQLITE_STATIC);

    do {
    rc = sqlite3_step(pStmt);
    assert( rc!=SQLITE_ROW );

    rc = sqlite3_finalize(pStmt);

  } while( rc==SQLITE_SCHEMA );

  return true;
}

bool readBlob(sqlite3 *db, const std::string &table, std::vector<std::string> &strs) {
    std::stringstream ss;
    ss << "SELECT value FROM " << table;
    std::string sql = ss.str();
    sqlite3_stmt *pStmt;
    int rc = sqlite3_prepare(db, sql.c_str(), -1, &pStmt, 0);
    if( rc!=SQLITE_OK ){
        return false;
    }
    while( true ) {
        rc = sqlite3_step(pStmt);
        if( rc==SQLITE_ROW ){
            unsigned int size = sqlite3_column_bytes(pStmt, 0);
            std::string str(reinterpret_cast<const char*>(sqlite3_column_blob(pStmt, 0)), size);
            strs.push_back(str);
        }
        if (rc == SQLITE_DONE) break;
    }

    rc = sqlite3_finalize(pStmt);
    return true;
}

bool setKey(
  sqlite3 *db,                  /* Database to insert data into */
  const std::string &key,            /* Null-terminated key string */
  const char *data,             /* Pointer to blob of data */
  unsigned int size             /* Length of data pointed to by zBlob */
) {
    std::stringstream ss;
    ss << "INSERT INTO KeyValue(key, value) VALUES(?, ?)";
    std::string sql = ss.str();
    sqlite3_stmt *pStmt;
    int rc;
    do {
        rc = sqlite3_prepare(db, sql.c_str(), -1, &pStmt, 0);
        if( rc!=SQLITE_OK ){
            return false;
        }
        sqlite3_bind_text(pStmt, 1, key.c_str(), key.size(), SQLITE_STATIC);
        sqlite3_bind_blob(pStmt, 2, data, size, SQLITE_STATIC);

        rc = sqlite3_step(pStmt);
        assert( rc!=SQLITE_ROW );

        rc = sqlite3_finalize(pStmt);
  } while( rc==SQLITE_SCHEMA );

  return true;
}

bool getKey(sqlite3 *db, const std::string &key, std::string &str) {
    std::stringstream ss;
    ss << "SELECT value FROM KeyValue WHERE key = ?";
    std::string sql = ss.str();
    sqlite3_stmt *pStmt;
    int rc = sqlite3_prepare(db, sql.c_str(), -1, &pStmt, 0);
    if( rc!=SQLITE_OK ){
        return false;
    }
    sqlite3_bind_text(pStmt, 1, key.c_str(), key.size(), SQLITE_STATIC);

    while( true ) {
        rc = sqlite3_step(pStmt);
        if( rc==SQLITE_ROW ){
            unsigned int size = sqlite3_column_bytes(pStmt, 0);
            str = std::string(reinterpret_cast<const char*>(sqlite3_column_blob(pStmt, 0)), size);
        }
        if (rc == SQLITE_DONE) break;
    }

    rc = sqlite3_finalize(pStmt);
    return true;
}


