#pragma once

#include <QtWidgets/QMainWindow>
#include <QStandardItemModel>
#include <QTreeWidget>
#include "ui_qteditor.h"

#include "collection/collections.hpp"

#include "CategoryTree.hpp"
#include "ItemsTree/TreeModel.hpp"
#include "ItemsTree/TreeView.hpp"
#include "ItemsTree/ItemsTree.hpp"
#include "PropertySetEditor/PropertySetEditor.hpp"
#include "ImageEditor/ImagePreview.hpp"
#include "ItemsEditor/ItemPropertyWidget.hpp"

class qtEditor : public QMainWindow {
    Q_OBJECT

public:
    qtEditor(QWidget *parent = 0);
    ~qtEditor();

    ItemPropertyWidget *propertyWidget;

    PropertySetEditor *propertySetEditor;
private:
    Ui::qtEditorClass ui;
    Category* selectedCategory;
    ImagePreview* imagePreview;
    ItemsTree* itemsTree;

public slots:
    void updateItem();
    void save();
    void open();
    void cancelButton();
    void editDescription();

    QString dateFileName();
    QStringList SaveDialog(const QString &ext);
    std::string OpenDialog(const QString &ext);

    void ToJson();
    void FromJson();
    void ToSQLite();
    void FromSQLite();
    void ToProtoBuf();
    void FromProtoBuf();

    void editItemsTypes();
    void editProducers();

    void imagesListShow();

    void settings();

private slots:
    void selectionChanged(std::vector<BaseProps*> items);
};

