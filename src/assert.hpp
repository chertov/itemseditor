#pragma once

#include <assert.h>
#include <stdexcept>
#include <sstream>

#define UNREACHABLE (!"Unreachable code executed!")
#define UNKNOWN_ASSERT (!"Непредвиденная ошибка. Отправьте лог разработчику!")

