#include "tools.hpp"

#include <iostream>
#include <sstream>

#ifdef _WIN32
    #include <Windows.h>
#else
    #include <sys/time.h>
#endif

TXTSaver::TXTSaver(std::string path, std::string name) {
    if (name.size()==0) name = path;
    if (path.size()!=0)
        this->path = path;
    else
        this->path = "log.txt";
}
TXTSaver::~TXTSaver() {
    std::cout << str() << std::endl;
    std::string utf_str = str();
    {
        std::ofstream file(path.data(), std::ios::out | std::ios::binary | std::ios::trunc);
        if (file.is_open())
            file << utf_str.data();
        file.close();
    }
}

std::string getFileName(const std::string& path) {
    size_t iLastSeparator = 0;

    // ищем последний слеш
    iLastSeparator = path.find_last_of("/");

    // если не нашли, то ищем последний обратный слеш
    if (iLastSeparator == std::string::npos)
        iLastSeparator = path.find_last_of("\\");

    // отсекаем расширение и возвращаем
    if (iLastSeparator != std::string::npos)
        return path.substr(iLastSeparator+1, path.size() - path.find_last_of("."));
    else
        return path.substr(0, path.size() - path.find_last_of("."));

    //return path.substr((iLastSeparator = path.find_last_of("\\")) != std::string::npos ? iLastSeparator + 1 : 0, path.size() - path.find_last_of("."));
}

// TODO: в линуксе не удаляет пустые элементы
std::vector<std::string> str_split(const std::string& input_str, const std::string& separator, bool remove_empty) {
    std::string input = input_str;
    if (remove_empty)
        input = ReplaceString(input, "\r", "");
    //std::vector<std::string> lst;
    //std::ostringstream word;
    //for (size_t n = 0; n < input.size(); ++n)
    //{
    //	if (std::string::npos == separators.find(input[n]))
    //		word << input[n];
    //	else
    //	{
    //		if (!word.str().empty() || !remove_empty)
    //			lst.push_back(word.str());
    //		word.str("");
    //	}
    //}
    //if (!word.str().empty() || !remove_empty)
    //	lst.push_back(word.str());

    std::vector<std::string> lst;
    std::ostringstream word;
    for (size_t n = 0; n < input.size(); ++n)
    {
        if (input.find(separator, n) != n)
            word << input[n];
        else
        {
            n += separator.size() - 1;
            if (!word.str().empty() || !remove_empty)
                lst.push_back(word.str());
            word.str("");
        }
    }
    if (!word.str().empty() || !remove_empty)
        lst.push_back(word.str());

    return lst;
}


bool str_start_with(const std::string &str, const std::string &start) {
    return (str.find(start) == 0);
}

std::string str_trim(const std::string &str) {
    std::string::const_iterator it = str.begin();
    while (it != str.end() && isspace(*it))
        it++;

    std::string::const_reverse_iterator rit = str.rbegin();
    while (rit.base() != it && isspace(*rit))
        rit++;

    return std::string(it, rit.base());
}

bool str_empty(const std::string &str) {
    return (str.size() == 0);
}

std::string ReplaceString(std::string subject, const std::string &search, const std::string &replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
        subject.replace(pos, search.length(), replace);
        pos += replace.length();
    }
    return subject;
}

bool str_equal(const std::string &str1, const std::string &str2) {
    return (str1.compare(str2) == 0);
}

unsigned long long getTickCount() {
#ifdef _WIN32
    return GetTickCount();
#else
    struct timeval tv;
    gettimeofday(&tv, 0);
    return static_cast<unsigned long long>((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
#endif
}
