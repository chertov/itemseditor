#include "category.hpp"

#include "parser.hpp"

#include "sqlite.hpp"
#include "db.hpp"
#include "item.hpp"
#include "exception.hpp"

Category::Category() : BaseFolder(CATEGORY) { }

PtrInfo* Category::GetParent() const {
    return DB.GetCategoryParent(GetId());
}
void Category::SetParent(PtrInfo* parent) {
    ExceptionSaver ss;
    ss << "Call Category::SetParent. This is a unreachable code.";

}

void Category::Clear() { items.clear(); }

std::vector<PtrInfo*> Category::Childrens() const {
    std::vector<PtrInfo*> childrens;
    for(unsigned int i = 0; i < items.size(); ++i) childrens.push_back(DB.ItemById(items[i]));
    return childrens;
}

size_t Category::ChildrenSize() const { return items.size(); }

PtrInfo* Category::Children(unsigned int index) const {
    if (index < items.size())
        return DB.ItemById(items[index]);
    return 0;
}

void Category::ChildrensRemove() {
    while(items.size() > 0)
        DB.DeletePtr(DB.ItemById(items[0]));
}
void Category::ChildrenDrop(Item *item) {
    items.erase(std::remove(items.begin(), items.end(), item->GetId()), items.end());
}
void Category::ChildrenAppend(Item *item, int row) {
    DB.SetItemParent(item->GetId(), this);
    if (row >= 0 && row < static_cast<int>(items.size()))
        items.insert(items.begin() + row, item->GetId());
    else
        items.push_back(item->GetId());
}

void Category::SetShowOnSiteForChildrens(bool value) {
    SetShowOnSite(value);
    for(auto item : items) DB.ItemById(item)->SetShowOnSite(value);
}

bool Category::ToJSON(json_t *json) {
    if(!BaseFolder::ToJSON(json)) return false;

    json_t *json_items = json_array();
    for(unsigned int i = 0; i < items.size(); ++i) {
        json_t *json_obj = IdToJson(items[i]);
        json_array_append(json_items, json_obj);
    }
    if (json_object_set(json, "items", json_items) != 0) return log_error("items");

    if (!PropertySet::ToJSON(json)) {
        std::cout << "Category render error: " << std::endl
                  << "    Id: " << this->GetId() << std::endl
                  << "    Name: " << this->GetName() << std::endl;
        return false;
    }

    return true;
}
bool Category::FromJSON(json_t *json) {
    if(!BaseFolder::FromJSON(json)) return false;
    if (!PropertySet::FromJSON(json)) {
        std::cout << "Category parse error: " << std::endl
                  << "    Id: " << this->GetId() << std::endl
                  << "    Name: " << this->GetName() << std::endl;
        return false;
    }

    json_t *items_json = json_object_get(json, "items");
    if (items_json == NULL) return log_error("items json is NULL");
    if (!json_is_array(items_json)) return log_error("items is not array");
    for (unsigned int i = 0; i < json_array_size(items_json); ++i) {
        json_t *json_item = json_array_get(items_json, i);
        if (json_item == NULL) return log_error("item json is NULL");

        IdType id;
        if(!IdFromJson(json_item, id)) return log_error("json_item is not id");
        items.push_back(id);
        DB.SetItemParent(id, this);
    }

    return true;
}

bool Category::ToProtoBuf(ItemsDB::Category *pbf) const {
    if(!BaseFolder::ToProtoBuf(pbf->mutable_basefolder())) log_error("Category::ToProtoBuf BaseFolder error");
    for(const IdType &item : items) pbf->add_items(item);
    if(!PropertySet::ToProtoBuf(pbf->mutable_propertyset())) log_error("Category::ToProtoBuf PropertySet error");
    return true;
}
bool Category::FromProtoBuf(const ItemsDB::Category &pbf) {
    if(!BaseFolder::FromProtoBuf(pbf.basefolder())) log_error("Category::FromProtoBuf BaseFolder error");
    items.clear();
    for (unsigned int i = 0; i < pbf.items_size(); ++i) {
        IdType id = pbf.items(i);
        items.push_back(id);
        DB.SetItemParent(id, this);
    }
    if(!PropertySet::FromProtoBuf(pbf.propertyset())) log_error("Category::FromProtoBuf PropertySet error");
    return true;
}

Category *Categories::NewCategory(Folder *parent) {
    Category* obj = new Category();
    DB.CategoriesMap[obj->GetId()].insert(parent);
    categories.push_back(obj);
    categories_ptr[obj->GetId()] = obj;
    return obj;
}
void Categories::DeleteCategory(Category *category) {
   CategoriesMap.erase(category->GetId());
   categories_ptr.erase(category->GetId());
   categories.erase(std::remove(categories.begin(), categories.end(), category), categories.end());
   delete category;
   category = 0;
}

unsigned int Categories::CategoriesSize() const  { return categories.size(); }
Category* Categories::CategoryByIndex(unsigned int index) const {
    if (index >= categories.size()) return 0;
    return categories[index];
}
Category* Categories::CategoryById(const IdType &id) const {
    auto it = categories_ptr.find(id);
    if (it == categories_ptr.end()) {
        ExceptionSaver ss; ss << "Categories::CategoryById: Can't find category with id = " << id;

    }
    Category *category = it->second;
    if(category == 0 ) {
        ExceptionSaver ss;
        ss << "Categories::CategoryById: category == 0";

    }
    return category;
}
bool Categories::CategoryMove(unsigned int source_index, unsigned int destination_index) {
    return move_vector_element<Category*>(source_index, destination_index, categories);
}



bool Categories::ToJSON(json_t *json) {
    json_t *json_categories = json_array();
    for(unsigned int i = 0; i < categories.size(); ++i) {
        json_t *json_obj = json_object();
        if (!categories[i]->ToJSON(json_obj)) { std::cout << "Render Category " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_categories, json_obj);
    }
    if (json_object_set(json, "categories", json_categories) != 0) return log_error("categories");
    return true;
}

bool Categories::FromJSON(json_t *json) {
    json_t *categories_json = json_object_get(json, "categories");
    if (categories_json == NULL) return log_error("categories_json is NULL");
    if (!json_is_array(categories_json)) return log_error("categories_json is not array");
    for (unsigned int i = 0; i < json_array_size(categories_json); ++i) {
        json_t *json_category = json_array_get(categories_json, i);
        if (json_category == NULL) return log_error("json_category is NULL");

        if (!json_is_object(json_category)) return log_error("json_category is not object");
        Category *category = new Category();
        if (!category->FromJSON(json_category)) return false;
        categories.push_back(category);
        categories_ptr[category->GetId()] = category;
    }
    return true;
}

void Categories::clear() {
    for (unsigned int i = 0; i < categories.size(); ++i) delete categories[i];
    categories.clear();
    CategoriesMap.clear();
}

PtrInfo* Categories::GetCategoryParent(const IdType &category_id) const {
    auto it = CategoriesMap.find(category_id);
    if (it == CategoriesMap.end()) {
        ExceptionSaver ss;
        ss << "Can't find parent of category with id = " << category_id;

    }
    if(it->second.size() != 1) {
        ExceptionSaver ss;
        ss << "Category with id = " << category_id << " have incorrent parent size = " << it->second.size();

    }
    return *(it->second.begin());
}
bool Categories::SetCategoryParent(const IdType &category_id, Folder *parent) {
    CategoriesMap[category_id].clear();
    CategoriesMap[category_id].insert(parent);
}


bool Categories::ToSQLite(sqlite3 *db) {
    const char *sql = "CREATE TABLE Categories("  \
             "KEY    UNSIGNED BIG INT    PRIMARY KEY     NOT NULL," \
             "VALUE  BLOB                                NOT NULL);";
    char *zErrMsg = 0;
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        ExceptionSaver ss; ss << "Categories::ToSQLite SQL error: " << zErrMsg;
        sqlite3_free(zErrMsg);
    }

    for(unsigned int i = 0; i < categories.size(); ++i) {
        json_t *json = json_object();
        if (!categories[i]->ToJSON(json)) { std::cout << "Categories::ToSQLite Render Category " << i << " to json error!" << std::endl; return false; }
        char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
        if (str_data == NULL) return log_error("Categories::ToSQLite Render Category to json error");
        if(!writeBlob(db, "Categories", categories[i]->GetId(), str_data, strlen(str_data))) return false;
        delete[] str_data;
    }
    return true;
}

bool Categories::FromSQLite(sqlite3 *db) {
    std::vector<std::string> strs;
    if(!readBlob(db, "Categories", strs)){
        ExceptionSaver ss; ss << "Categories::FromSQLite SQL error: ";
    };
    for (std::string &str : strs) {
        json_error_t error;
        json_t *json = json_loads(str.c_str(), 0, &error);
        if (json == NULL) {
            std::cout << error.text << std::endl;
            return log_error("Categories::FromSQLite json is NULL");
        }
        Category *category = new Category();
        if (!category->FromJSON(json)) return false;
        categories.push_back(category);
        categories_ptr[category->GetId()] = category;
    }
    return true;
}


bool Categories::ToProtoBuf(ItemsDB::Db *db) const {
    for(unsigned int i = 0; i < categories.size(); ++i) {
        ItemsDB::Category* category_pbf = db->add_categories();
        if (!categories[i]->ToProtoBuf(category_pbf)) return log_error("Categories::ToProtoBuf category error");
    }
    return true;
}

bool Categories::FromProtoBuf(const ItemsDB::Db &db) {
    for(unsigned int i = 0; i < db.categories_size(); ++i) {
        Category *category = new Category();
        if(!category->FromProtoBuf(db.categories(i))) return log_error("Categories::FromProtoBuf category error");
        categories.push_back(category);
        categories_ptr[category->GetId()] = category;
    }
    return true;
}

