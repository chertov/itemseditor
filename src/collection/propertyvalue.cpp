#include "propertyvalue.hpp"

#include "assert.hpp"
#include "tools.hpp"
#include "parser.hpp"
#include "db.hpp"
#include "exception.hpp"

IdType PropertyValue::GetPropertyId() const {
    return PropertyId;
}
bool PropertyValue::SetPropertyId(const IdType &property_id) {
    this->PropertyId = property_id;
    return true;
}

bool PropertyValue::ToJSON(json_t *json) {
    if (!IdToJson(json, "property", PropertyId)) return false;

    Property *property = DB.PropertyById(PropertyId);
    switch (property->GetType()) {
    case PROPERTY_BOOL:
        if (json_object_set(json, "bool", json_boolean(Bool)) != 0) return log_error("Bool value");
        break;
    case PROPERTY_INT:
        if (json_object_set(json, "int", json_integer(Int)) != 0) return log_error("Int value");
        break;
    case PROPERTY_REAL:
        if (json_object_set(json, "real", json_real(Real)) != 0) return log_error("Real value");
        break;
    case PROPERTY_ENUM:
        //std::string str;
        //if (!property.GetEnumValue(Enum, str)) return false;
        if (!IdToJson(json, "enum", Enum)) return log_error("Enum value");
        break;
    }
    return true;
}

bool PropertyValue::FromJSON(json_t *json) {
    if (!IdFromJson(json, "property", PropertyId)) return log_error_parse("property_id");

    Property *property = DB.PropertyById(PropertyId);
    switch (property->GetType()) {
    case PROPERTY_BOOL:
        if (!json_parse_bool(json, "bool", Bool)) return false;
        break;
    case PROPERTY_INT:
        if (!json_parse_int(json, "int", Int)) return false;
        break;
    case PROPERTY_REAL:
        if (!json_parse_double_lib(json, "real", Real)) return false;
        break;
    case PROPERTY_ENUM:
        if (!IdFromJson(json, "enum", Enum)) return false;
        // Проверка на существование значения в Enum
        std::string str = property->GetEnumPtr()->GetValue(Enum);
        break;
    }
    return true;
}


bool PropertyValue::ToProtoBuf(ItemsDB::PropertyValue *pbf) const {
    pbf->set_propertyid(PropertyId);
    pbf->set_bool_(false);
    pbf->set_int_(0);
    pbf->set_real(0.0);
    pbf->set_enum_(0);

    Property *property = DB.PropertyById(PropertyId);
    switch (property->GetType()) {
    case PROPERTY_BOOL:
        pbf->set_bool_(Bool);
        break;
    case PROPERTY_INT:
        pbf->set_int_(Int);
        break;
    case PROPERTY_REAL:
        pbf->set_real(Real);
        break;
    case PROPERTY_ENUM:
        //std::string str;
        //if (!property.GetEnumValue(Enum, str)) return false;
        pbf->set_enum_(Enum);
        break;
    }
    return true;
}
bool PropertyValue::FromProtoBuf(const ItemsDB::PropertyValue &pbf) {
    PropertyId = pbf.propertyid();

    Bool = false;
    Int = 0;
    Real = 0.0;
    Enum = 0;

    Property *property = DB.PropertyById(PropertyId);
    switch (property->GetType()) {
    case PROPERTY_BOOL:
        Bool = pbf.bool_();
        break;
    case PROPERTY_INT:
        Int = pbf.int_();
        break;
    case PROPERTY_REAL:
        Real = pbf.real();
        break;
    case PROPERTY_ENUM:
        Enum = pbf.enum_();
        // Проверка на существование значения в Enum
        std::string str = property->GetEnumPtr()->GetValue(Enum);
        break;
    }
    return true;
}

//bool PropertyValues::ToJSON(json_t *json) {
//    json_t *json_propertyValues = json_array();
//    for(unsigned int i = 0; i < propertyValues.size(); ++i) {
//        json_t *json_obj = json_object();
//        if (!propertyValues[i]->ToJSON(json_obj)) { std::cout << "Render PropertyValue " << i << " to json error!" << std::endl; return false; }
//        json_array_append(json_propertyValues, json_obj);
//    }
//    if (json_object_set(json, "propertyValues", json_propertyValues) != 0) return log_error("propertyValues");
//    return true;
//}

//bool PropertyValues::FromJSON(json_t *json) {
//    json_t *json_propertyValues = json_object_get(json, "propertyValues");
//    if (json_propertyValues == NULL) return log_error("json_propertyValues is NULL");
//    if (!json_is_array(json_propertyValues)) return log_error("json_propertyValues is not array");
//    for (unsigned int i = 0; i < json_array_size(json_propertyValues); ++i) {
//        json_t *json_propertyValue = json_array_get(json_propertyValues, i);
//        if (json_propertyValue == NULL) return log_error("json_propertyValue is NULL");
//        if (!json_is_object(json_propertyValue)) return log_error("json_propertyValue is not object");
//        PropertyValue *propertyValue = new PropertyValue();
//        if (!propertyValue->FromJSON(json_propertyValue)) return false;
//        propertyValues.push_back(propertyValue);
//        propertyValuesMap[propertyValue->GetId()] = propertyValue;
//    }
//    return true;
//}

//PropertyValue* PropertyValues::NewPropertyValue() {
//    PropertyValue* propertyValue = new PropertyValue();
//    propertyValues.push_back(propertyValue);
//    propertyValuesMap[propertyValue->GetId()] = propertyValue;
//    return propertyValue;
//}

//bool PropertyValues::DeletePropertyValue(PropertyValue* propertyValue) {
//    propertyValuesMap.erase(propertyValue->GetId());
//    propertyValues.erase(std::remove(propertyValues.begin(), propertyValues.end(), propertyValue), propertyValues.end());
//    delete propertyValue;
//    propertyValue = 0;
//    return true;
//}

//void PropertyValues::clear() {
//    for (unsigned int i = 0; i < propertyValues.size(); ++i) delete propertyValues[i];
//    propertyValues.clear();
//    propertyValuesMap.clear();
//}

//unsigned int PropertyValues::PropertyValuesSize() const { return propertyValues.size(); }
//PropertyValue* PropertyValues::PropertyValueByIndex(unsigned int index) const {
//    if (index >= propertyValues.size()) {
//        ExceptionSaver ss; ss << "PropertyValues::PropertyValuesByIndex: index out of range. Max range = " << propertyValues.size();
//    }
//    return propertyValues[index];
//}
//PropertyValue* PropertyValues::PropertyValueById(const IdType &id) const {
//    auto it = propertyValuesMap.find(id);
//    if (it == propertyValuesMap.end()) {
//        ExceptionSaver ss; ss << "PropertyValues::PropertyValuesById: Can't find property with id = " << id;

//    }
//    return it->second;
//}

