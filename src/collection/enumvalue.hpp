#pragma once

#include <string>
#include <vector>
#include <jansson.h>
#include <unordered_map>

#include "idtype.hpp"
#include "tools.hpp"
#include "db.pb.h"

class EnumValue {
    friend class Enum;
public:
    EnumValue() {}
    virtual ~EnumValue() {}

    IdType GetId() const;
    bool SetId(IdType value);

    std::string GetValue() const;
    bool SetValue(std::string value);

private:
    IdType id = uuid();
    std::string value = "Новое значение";

    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToProtoBuf(ItemsDB::EnumValue *pbf) const;
    bool FromProtoBuf(const ItemsDB::EnumValue &pbf);
};
typedef std::vector<EnumValue> EnumValueList;

//class EnumValues {
//public:
//    EnumValue *NewEnumValue();
//    bool DeleteEnumValue(EnumValue *enumValue);
//    unsigned int EnumValuesSize() const;
//    EnumValue* EnumValueByIndex(unsigned int index) const;
//    EnumValue* EnumValueById(const IdType &id) const;
//protected:
//    bool ToJSON(json_t *json);
//    bool FromJSON(json_t *json);
//    void clear();
//private:
//    std::unordered_map<IdType, EnumValue*> enumValuesMap;
//    EnumValueList enumValues;
//};

