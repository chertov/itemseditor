#pragma once
#include <iostream>

#include "item.hpp"
#include "propertyset.hpp"

#include <QColor>
#include <QString>

class WarningValue {
public:
    QString message = "Неизвестная ошибка";
    QColor color = QColor(255, 198, 66);
};

Q_DECLARE_METATYPE(WarningValue)

enum PropertyWalkerResult{
    ALL_EQUAL,
    NOT_EQUAL,
    PROPERTY_NOT_FOUND,
    ITEM_INVALID,
    ENUM_INVALID
};

class PropertyWalker {
public:
    PropertyWalker(Property *property) {
        this->property = property;
        first = true;
    }
    PropertyWalkerResult Check(Item *item) {
        if(result != ALL_EQUAL) return result; // Как только нашли расхождение выходим
        if (item==0) { result = ITEM_INVALID; return result; }
        PropertyValue prop;
        bool prop_founded = item->GetPropertyValue(property->GetId(), prop);
        if(!prop_founded) { result = PROPERTY_NOT_FOUND; return result; }
        if(first) { value = prop; first = false; }
        switch(property->GetType()) {
        case PROPERTY_BOOL:
            if(value.Bool != prop.Bool) { result = NOT_EQUAL; return result; }
            break;
        case PROPERTY_INT:
            if(value.Int != prop.Int) { result = NOT_EQUAL; return result; }
            break;
        case PROPERTY_REAL:
            if(value.Real != prop.Real) { result = NOT_EQUAL; return result; }
            break;
        case PROPERTY_ENUM:
            if(value.Enum != prop.Enum) { result = NOT_EQUAL; return result; }
            std::string val;
            if(!property->GetEnumValue(prop.Enum, val)) { result = ENUM_INVALID; return ENUM_INVALID; }
            break;
        }
        return result;
    }
    PropertyWalkerResult Result() { return result; }
    PropertyValue Value() { return value; }

private:
    PropertyWalkerResult result = ALL_EQUAL;
    Property *property;
    PropertyValue value;
    bool first;
};

class ItemTypeWalker {
public:
    ItemTypeWalker() {
        first = true;
    }
    PropertyWalkerResult Check(Item *item) {
        if (result != ALL_EQUAL) return result;
        if (item == 0) { result = ITEM_INVALID; return result; }
        if (first) { value = item->GetItemType(); first = false; return result; }
        if (item->GetItemType() != value) { result = NOT_EQUAL; return result; }
        return result;
    }
    PropertyWalkerResult Result() { return result; }
    IdType Id() { return value; }

private:
    PropertyWalkerResult result = ALL_EQUAL;
    IdType value = 0;
    bool first;
};

class ProducerWalker {
public:
    ProducerWalker() {
        first = true;
    }
    PropertyWalkerResult Check(Item *item) {
        if(result != ALL_EQUAL) return result;
        if (item == 0) { result = ITEM_INVALID; return result; }
        if(first) { value = item->GetProducer(); first = false; return result; }
        if(item->GetProducer() != value) { result = NOT_EQUAL; return result; }
        return result;
    }
    PropertyWalkerResult Result() { return result; }
    IdType Id() { return value; }

private:
    PropertyWalkerResult result = ALL_EQUAL;
    IdType value = 0;
    bool first;
};
