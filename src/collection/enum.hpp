#pragma once

#include <string>
#include <vector>
#include <jansson.h>
#include <unordered_map>
#include "sqlite/sqlite3.h"
#include "db.pb.h"

#include "idtype.hpp"
#include "tools.hpp"
#include "enumvalue.hpp"

class Enum {
    friend class Enums;
public:
    IdType GetId() const;
    bool SetId(IdType value);

    std::string GetName() const;
    bool SetName(std::string value);

    std::string GetDescription() const;
    bool SetDescription(std::string value);

    EnumValueList GetValues() const;
    bool SetValues(const EnumValueList& values);

    unsigned int GetValuesSize() const;
    std::string GetValue(const IdType &value_id) const;
    bool UpsertValue(const EnumValue &value);
    EnumValue GetValueByIndex(const unsigned int &index) const;

    bool MoveValue(const unsigned int &source, const unsigned int &dest);
protected:
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToProtoBuf(ItemsDB::Enum *pbf) const;
    bool FromProtoBuf(const ItemsDB::Enum &pbf);
private:
    IdType Id = uuid();
    std::string Name;
    std::string Description;

    EnumValueList values;
};

class Enums {
public:
    Enum *NewEnum();
    bool DeleteEnum(Enum *enum_ptr);
    unsigned int EnumsSize() const;
    Enum* EnumByIndex(unsigned int index) const;
    Enum* EnumById(const IdType &id) const;
protected:
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db);
    bool ToProtoBuf(ItemsDB::Db *db) const;
    bool FromProtoBuf(const ItemsDB::Db &db);
    void clear();
private:
    std::unordered_map<IdType, Enum*> EnumsMap;
    std::vector<Enum*> enums;
};

