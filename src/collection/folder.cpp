#include "folder.hpp"

#include "parser.hpp"

#include "db.hpp"
#include "category.hpp"
#include "exception.hpp"
#include "sqlite.hpp"

PtrInfo* Folder::GetParent() const {
    return DB.GetFolderParent(GetId());
}
void Folder::SetParent(PtrInfo* parent) {
    ExceptionSaver ss;
    ss << "Call Folder::SetParent. This is a unreachable code.";
}

bool Folder::ToJSON(json_t *json) {
    if(!BaseFolder::ToJSON(json)) return log_error("Render BaseFolder to json error!");
    if(!FoldersCategories::ToJSON(json)) return log_error("Render FoldersCategories to json error!");
    return true;
}
bool Folder::FromJSON(json_t *json) {
    if(!BaseFolder::FromJSON(json)) return false;
    if(!FoldersCategories::FromJSON(json, this)) return false;
    return true;
}

bool Folder::ToProtoBuf(ItemsDB::Folder *pbf) const {
    if(!BaseFolder::ToProtoBuf(pbf->mutable_basefolder())) return log_error("Folder::ToProtoBuf BaseFolder error");
    if(!FoldersCategories::ToProtoBuf(pbf->mutable_folderscategories())) return log_error("Folder::ToProtoBuf FoldersCategories error");
    return true;
}

bool Folder::FromProtoBuf(const ItemsDB::Folder &pbf) {
    if(!BaseFolder::FromProtoBuf(pbf.basefolder())) return log_error("Folder::FromProtoBuf BaseFolder error");
    if(!FoldersCategories::FromProtoBuf(pbf.folderscategories(), this)) return log_error("Folder::FromProtoBuf FoldersCategories error");
    return true;
}

bool FoldersCategories::ToJSON(json_t *json) {
    json_t *json_folders = json_array();
    for(unsigned int i = 0; i < folders.size(); ++i) {
        //json_t *json_obj = json_stringn_nocheck(folders[i].data(), folders[i].size());
        json_t *json_obj = IdToJson(folders[i]);
        json_array_append(json_folders, json_obj);
    }
    if (json_object_set(json, "folders", json_folders) != 0) return log_error("folders");

    json_t *json_categories = json_array();
    for(unsigned int i = 0; i < categories.size(); ++i) {
        //json_t *json_obj = json_stringn_nocheck(categories[i].data(), categories[i].size());
        json_t *json_obj = IdToJson(categories[i]);
        json_array_append(json_categories, json_obj);
    }
    if (json_object_set(json, "categories", json_categories) != 0) return log_error("categories");

    return true;
}

bool FoldersCategories::FromJSON(json_t *json, Folder *self) {
    if (!json_is_object(json)) return log_error("FoldersCategories json is not object");

    json_t *json_categories = json_object_get(json, "categories");
    if (json_categories == NULL) return log_error("json_categories is NULL");
    if (!json_is_array(json_categories)) return log_error("json_categories is not array");
    for (unsigned int i = 0; i < json_array_size(json_categories); ++i) {
        json_t *json_category = json_array_get(json_categories, i);
        if (json_category == NULL) return log_error("json_category json is NULL");
        IdType id;
        if(!IdFromJson(json_category, id)) return log_error("json_category json is not id");
        categories.push_back(id);
        DB.CategoriesMap[id].insert(self);
    }

    json_t *json_folders = json_object_get(json, "folders");
    if (json_folders == NULL) return log_error("json_folders is NULL");
    if (!json_is_array(json_folders)) return log_error("json_folders is not array");
    for (unsigned int i = 0; i < json_array_size(json_folders); ++i) {
        json_t *json_folder = json_array_get(json_folders, i);
        if (json_folder == NULL) return log_error("json_folder json is NULL");
        IdType id;
        if(!IdFromJson(json_folder, id)) return log_error("json_folder json is not id");
        folders.push_back(id);
        DB.FoldersMap[id].insert(self);
    }
    return true;
}

bool FoldersCategories::ToSQLite(sqlite3 *db) {
    json_t *json = json_object();
    if (!ToJSON(json)) { std::cout << "FoldersCategories::ToSQLite Render FoldersCategories to json error!" << std::endl; return false; }
    char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
    if (str_data == NULL) return log_error("FoldersCategories::ToSQLite Render FoldersCategories to json error");
    if(!setKey(db, "root", str_data, strlen(str_data))) return false;
    delete[] str_data;
    return true;
}

bool FoldersCategories::FromSQLite(sqlite3 *db, Folder *self) {
    std::string str;
    if(!getKey(db, "root", str)){
        ExceptionSaver ss; ss << "FoldersCategories::FromSQLite SQL error: ";
    };

    json_error_t error;
    json_t *json = json_loads(str.c_str(), 0, &error);
    if (json == NULL) {
        std::cout << error.text << std::endl;
        return log_error("FoldersCategories::FromSQLite json is NULL");
    }
    if (!FromJSON(json, self)) return false;
    return true;
}

bool FoldersCategories::ToProtoBuf(ItemsDB::FoldersCategories *pbf) const {
    for(auto folder : folders) pbf->add_folders(folder);
    for(auto category : categories) pbf->add_categories(category);
    return true;
}

bool FoldersCategories::FromProtoBuf(const ItemsDB::FoldersCategories &pbf, Folder *self) {
    for (unsigned int i = 0; i < pbf.categories_size(); ++i) {
        IdType id = pbf.categories(i);
        categories.push_back(id);
        DB.CategoriesMap[id].insert(self);
    }
    for (unsigned int i = 0; i < pbf.folders_size(); ++i) {
        IdType id = pbf.folders(i);
        folders.push_back(id);
        DB.FoldersMap[id].insert(self);
    }
    return true;
}

void FoldersCategories::Clear() {
    ChildrensRemove();
}

void FoldersCategories::ChildrensRemove() {
    while(folders.size() > 0)
        DB.DeletePtr(DB.FolderById(folders[0]));
    while(categories.size() > 0)
        DB.DeletePtr(DB.CategoryById(categories[0]));
}
void FoldersCategories::ChildrenDrop(Folder *folder) {
    folders.erase(std::remove(folders.begin(), folders.end(), folder->GetId()), folders.end());
}
void FoldersCategories::ChildrenDrop(Category *category) {
    categories.erase(std::remove(categories.begin(), categories.end(), category->GetId()), categories.end());
}
void FoldersCategories::ChildrenAppend(Folder *folder, int row) {
    if (row >= 0 && row < static_cast<int>(folders.size()))
        folders.insert(folders.begin() + row, folder->GetId());
    else
        folders.push_back(folder->GetId());
}
void FoldersCategories::ChildrenAppend(Category *category, int row) {
    if (row >= 0 && row < static_cast<int>(categories.size()))
        categories.insert(categories.begin() + row, category->GetId());
    else
        categories.push_back(category->GetId());
}

std::vector<PtrInfo*> FoldersCategories::Childrens() const {
    std::vector<PtrInfo*> childrens;
    for(unsigned int i = 0; i < folders.size(); ++i) childrens.push_back(DB.FolderById(folders[i]));
    for(unsigned int i = 0; i < categories.size(); ++i) childrens.push_back(DB.CategoryById(categories[i]));
    return childrens;
}

size_t FoldersCategories::ChildrenSize() const { return folders.size() + categories.size(); }
PtrInfo* FoldersCategories::Children(unsigned int index) const {
    if (index < folders.size()) {
        return DB.FolderById(folders[index]);
    } else {
        index -= folders.size();
        if (index < categories.size())
            return DB.CategoryById(categories[index]);
    }
    return 0;
}

void FoldersCategories::SetShowOnSiteForChildrens(bool value) {
    for(auto folder : folders) DB.FolderById(folder)->SetShowOnSiteForChildrens(value);
    for(auto category : categories) DB.CategoryById(category)->SetShowOnSiteForChildrens(value);
}

void Folder::Clear() { FoldersCategories::Clear(); }
std::vector<PtrInfo*> Folder::Childrens() const { return FoldersCategories::Childrens(); }
size_t Folder::ChildrenSize() const { return FoldersCategories::ChildrenSize(); }
PtrInfo* Folder::Children(unsigned int index) const { return FoldersCategories::Children(index); }

void Folder::SetShowOnSiteForChildrens(bool value) {
    SetShowOnSite(value);
    FoldersCategories::SetShowOnSiteForChildrens(value);
}


Folder *Folders::NewFolder(Folder *parent) {
    Folder* obj = new Folder();
    DB.FoldersMap[obj->GetId()].insert(parent);
    folders.push_back(obj);
    folders_ptr[obj->GetId()] = obj;
    return obj;
}
void Folders::DeleteFolder(Folder *folder) {
   FoldersMap.erase(folder->GetId());
   folders_ptr.erase(folder->GetId());
   folders.erase(std::remove(folders.begin(), folders.end(), folder), folders.end());
   delete folder;
   folder = 0;
}

unsigned int Folders::FoldersSize() const  { return folders.size(); }
Folder* Folders::FolderByIndex(unsigned int index) const {
    if (index >= folders.size()) return 0;
    return folders[index];
}
Folder* Folders::FolderById(const IdType &id) const {
    auto it = folders_ptr.find(id);
    if (it == folders_ptr.end()) {
        ExceptionSaver ss; ss << "Folders::FolderById: Can't find folder with id = " << id;
    }
    Folder *folder = it->second;
    if(folder == 0 ) {
        ExceptionSaver ss; ss << "Folders::FolderById: folder == 0";
    }
    return folder;
}
bool Folders::FolderMove(unsigned int source_index, unsigned int destination_index) {
    return move_vector_element<Folder*>(source_index, destination_index, folders);
}

bool Folders::ToJSON(json_t *json) {
    json_t *json_folders = json_array();
    for(unsigned int i = 0; i < folders.size(); ++i) {
        json_t *json_obj = json_object();
        if (!folders[i]->ToJSON(json_obj)) { std::cout << "Render Folder " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_folders, json_obj);
    }
    if (json_object_set(json, "folders", json_folders) != 0) return log_error("folders");
    return true;
}
bool Folders::FromJSON(json_t *json) {
    json_t *json_folders = json_object_get(json, "folders");
    if (json_folders == NULL) return log_error("json_folders is NULL");
    if (!json_is_array(json_folders)) return log_error("json_folders is not array");
    for (unsigned int i = 0; i < json_array_size(json_folders); ++i) {
        json_t *json_folder = json_array_get(json_folders, i);
        if (json_folder == NULL) return log_error("json_folder is NULL");

        if (!json_is_object(json_folder)) return log_error("json_folder is not object");
        Folder *folder = new Folder();
        if (!folder->FromJSON(json_folder)) return false;
        folders.push_back(folder);
        folders_ptr[folder->GetId()] = folder;
    }

    return true;
}

void Folders::clear() {
    for (unsigned int i = 0; i < folders.size(); ++i) delete folders[i];
    folders.clear();
    FoldersMap.clear();
}


PtrInfo* Folders::GetFolderParent(const IdType &folder_id) const {
    auto it = FoldersMap.find(folder_id);
    if (it == FoldersMap.end()) {
        ExceptionSaver ss; ss << "Can't find parent of folder with id = " << folder_id;
    }
    if(it->second.size() != 1) {
        ExceptionSaver ss;
        ss << "Folder with id = " << folder_id << " have incorrent parent size = " << it->second.size();
    }
    return *(it->second.begin());
}
bool Folders::SetFolderParent(const IdType &folder_id, Folder *parent) {
    FoldersMap[folder_id].clear();
    FoldersMap[folder_id].insert(parent);
}


bool Folders::ToSQLite(sqlite3 *db) {
    const char *sql = "CREATE TABLE Folders("  \
             "KEY    UNSIGNED BIG INT    PRIMARY KEY     NOT NULL," \
             "VALUE  BLOB                                NOT NULL);";
    char *zErrMsg = 0;
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        ExceptionSaver ss; ss << "Folders::ToSQLite SQL error: " << zErrMsg;
        sqlite3_free(zErrMsg);
    }

    for(unsigned int i = 0; i < folders.size(); ++i) {
        json_t *json = json_object();
        if (!folders[i]->ToJSON(json)) { std::cout << "Folders::ToSQLite Render Folder " << i << " to json error!" << std::endl; return false; }
        char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
        if (str_data == NULL) return log_error("Folders::ToSQLite Render Folder to json error");
        if(!writeBlob(db, "Folders", folders[i]->GetId(), str_data, strlen(str_data))) return false;
        delete[] str_data;
    }
    return true;
}

bool Folders::FromSQLite(sqlite3 *db) {
    std::vector<std::string> strs;
    if(!readBlob(db, "Folders", strs)){
        ExceptionSaver ss; ss << "Folders::FromSQLite SQL error: ";
    };
    for (std::string &str : strs) {
        json_error_t error;
        json_t *json = json_loads(str.c_str(), 0, &error);
        if (json == NULL) {
            std::cout << error.text << std::endl;
            return log_error("Folders::FromSQLite json is NULL");
        }
        Folder *folder = new Folder();
        if (!folder->FromJSON(json)) return false;
        folders.push_back(folder);
        folders_ptr[folder->GetId()] = folder;
    }
    return true;
}


bool Folders::ToProtoBuf(ItemsDB::Db *db) const {
    for(unsigned int i = 0; i < folders.size(); ++i) {
        ItemsDB::Folder* folder_pbf = db->add_folders();
        if (!folders[i]->ToProtoBuf(folder_pbf)) return log_error("Folders::ToProtoBuf folder error");
    }
    return true;
}

bool Folders::FromProtoBuf(const ItemsDB::Db &db) {
    for(unsigned int i = 0; i < db.folders_size(); ++i) {
        Folder *folder = new Folder();
        if(!folder->FromProtoBuf(db.folders(i))) return log_error("Folders::FromProtoBuf folder error");
        folders.push_back(folder);
        folders_ptr[folder->GetId()] = folder;
    }
    return true;
}
