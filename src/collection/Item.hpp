#pragma once
#include <iostream>

#include <QVector>
#include <QString>
#include <QObject>
#include <QMap>
#include <vector>
#include <deque>
#include <set>
#include <unordered_map>
#include <memory>
#include <jansson.h>
#include "sqlite/sqlite3.h"

#include "base.hpp"
#include "propertyset.hpp"
#include "propertyvalue.hpp"
#include "idtype.hpp"

enum PriceCurrency {
    PRICE_RUB,
    PRICE_EUR,
    PRICE_USD
};

class Price {
public:
    PriceCurrency   Type;
    double          Value;
};

class Item : public BaseProps {
    friend class Category;
    friend class Db;
    friend class Items;

public:
    virtual PtrInfo* GetParent() const;
    virtual void SetParent(PtrInfo* parent);
    size_t ChildrenSize() const;

    std::string GetCode() const;
    bool SetCode(std::string value);

    std::string GetArticle() const;
    bool SetArticle(std::string value);

    IdType GetItemType() const;
    bool SetItemType(const IdType &value);

    IdType GetProducer() const;
    bool SetProducer(const IdType &value);

    IdVector GetImages() const;
    bool SetImages(const IdVector &value);

    PropertyValueList GetProperties() const;
    bool SetProperties(const PropertyValueList &value);

    bool GetPropertyValue(const IdType &property_id, PropertyValue &property_value) const;
    bool SetPropertyValue(const IdType &property_id, const PropertyValue &property_value);

private:
    Item();
    virtual ~Item() {}

    std::string Code;
    std::string Article;
    IdType ItemType = 0;
    IdType Producer = 0;
    IdVector Images;
    PropertyValueList Properties;

    Price price;
    int64_t Volume;

    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToProtoBuf(ItemsDB::Item *pbf) const;
    bool FromProtoBuf(const ItemsDB::Item &pbf);
};

class Category;

class ItemParents {
public:
    Item* item;
    std::set<Category*> parents;
};

class Items {
public:
    Item *NewItem(Category *parent);
    bool DeleteItem(Item *item);
    unsigned int ItemsSize() const;
    Item* ItemByIndex(unsigned int index) const;
    Item* ItemById(const IdType &id) const;
    bool ItemMove(unsigned int source_index, unsigned int destination_index);

    PtrInfo* GetItemParent(const IdType &item_id) const;
    std::set<Category*> GetItemParents(const IdType &item_id) const;
    bool SetItemParent(const IdType &item_id, Category *parent);

protected:
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db);
    bool ToProtoBuf(ItemsDB::Db *db) const;
    bool FromProtoBuf(const ItemsDB::Db &db);
    void clear();

private:
    std::unordered_map<IdType, ItemParents> ItemsMap;
    std::vector<Item*> items;
};
