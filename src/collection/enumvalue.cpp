#include "enum.hpp"
#include "parser.hpp"
#include "exception.hpp"

IdType EnumValue::GetId() const { return id; }
bool EnumValue::SetId(IdType id) { this->id = id; return true; }

std::string EnumValue::GetValue() const { return value; }
bool EnumValue::SetValue(std::string value) { this->value = value; return true; }

bool EnumValue::ToJSON(json_t *json) {
    if (!IdToJson(json, "id", id)) return false;
    if (!stringToJson(json, "value", value)) return false;
    return true;
}
bool EnumValue::FromJSON(json_t *json) {
    if (!IdFromJson(json, "id", id)) return log_error_parse("id");
    if (!json_parse_string(json, "value", value)) return log_error_parse("value");
    return true;
}

bool EnumValue::ToProtoBuf(ItemsDB::EnumValue *pbf) const {
    pbf->set_id(id);
    pbf->set_value(value);
    return true;
}
bool EnumValue::FromProtoBuf(const ItemsDB::EnumValue &pbf) {
    id = pbf.id();
    value = pbf.value();
    return true;
}

//bool EnumValues::ToJSON(json_t *json) {
//    json_t *json_enumValues = json_array();
//    for(unsigned int i = 0; i < enumValues.size(); ++i) {
//        json_t *json_obj = json_object();
//        if (!enumValues[i]->ToJSON(json_obj)) { std::cout << "Render enumValue " << i << " to json error!" << std::endl; return false; }
//        json_array_append(json_enumValues, json_obj);
//    }
//    if (json_object_set(json, "enum_values", json_enumValues) != 0) return log_error("enumValues");
//    return true;
//}

//bool EnumValues::FromJSON(json_t *json) {
//    json_t *json_enumValues = json_object_get(json, "enum_values");
//    if (json_enumValues == NULL) return log_error("json_enumValues is NULL");
//    if (!json_is_array(json_enumValues)) return log_error("json_enumValues is not array");
//    for (unsigned int i = 0; i < json_array_size(json_enumValues); ++i) {
//        json_t *json_enumValue = json_array_get(json_enumValues, i);
//        if (json_enumValue == NULL) return log_error("json_enumValue is NULL");

//        if (!json_is_object(json_enumValue)) return log_error("json_enumValue is not object");
//        EnumValue *enumValue = new EnumValue();
//        if (!enumValue->FromJSON(json_enumValue)) return false;
//        enumValues.push_back(enumValue);
//        enumValuesMap[enumValue->GetId()] = enumValue;
//    }
//    return true;
//}

//EnumValue* EnumValues::NewEnumValue() {
//    EnumValue* enumValue = new EnumValue();
//    enumValue->SetValue("Новое знаение");
//    enumValues.push_back(enumValue);
//    enumValuesMap[enumValue->GetId()] = enumValue;
//    return enumValue;
//}

//bool EnumValues::DeleteEnumValue(EnumValue* enumValue) {
//    enumValuesMap.erase(enumValue->GetId());
//    enumValues.erase(std::remove(enumValues.begin(), enumValues.end(), enumValue), enumValues.end());
//    delete enumValue;
//    enumValue = 0;
//    return true;
//}

//void EnumValues::clear() {
//    for (unsigned int i = 0; i < enumValues.size(); ++i) delete enumValues[i];
//    enumValues.clear();
//    enumValuesMap.clear();
//}

//unsigned int EnumValues::EnumValuesSize() const { return enumValues.size(); }
//EnumValue* EnumValues::EnumValueByIndex(unsigned int index) const {
//    if (index >= enumValues.size()) {
//        ExceptionSaver ss; ss << "EnumValues::EnumValueByIndex: index out of range. Max range = " << enumValues.size();

//    }
//    return enumValues[index];
//}
//EnumValue* EnumValues::EnumValueById(const IdType &id) const {
//    auto it = enumValuesMap.find(id);
//    if (it == enumValuesMap.end()) {
//        ExceptionSaver ss; ss << "EnumValues::EnumValueById: Can't find enumValue with id = " << id;

//    }
//    return it->second;
//}
