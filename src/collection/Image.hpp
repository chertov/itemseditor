#pragma once
#include <string>
#include <QImage>
#include "sqlite/sqlite3.h"
#include "db.pb.h"

#include "idtype.hpp"
#include "parser.hpp"

class Image {
    friend class Images;
public:
    IdType GetId() const;
    bool SetId(const IdType &value);

    std::string GetName() const;
    bool SetName(std::string value);

    bool GetPNGData(std::string &png);
    bool SetPNGData(const std::string &png);

    const QImage *GetPreview();

private:
    Image();
    virtual ~Image();

    IdType Id = uuid();
    std::string Name = "Новое изображение";
    std::string png; // image data in png
    QImage preview;

    virtual bool ToJSON(json_t *json);
    virtual bool FromJSON(json_t *json);
    bool ToProtoBuf(ItemsDB::Image *pbf) const;
    bool FromProtoBuf(const ItemsDB::Image &pbf);
};

class Images {
public:
    Image *NewImage();
    bool DeleteImage(Image *image);
    unsigned int ImagesSize() const;
    Image* ImageByIndex(unsigned int index) const;
    Image* ImageById(const IdType &id) const;
    bool ImageMove(unsigned int source_index, unsigned int destination_index);

protected:
    std::vector<Image*> images;
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db);
    bool ToProtoBuf(ItemsDB::Db *db) const;
    bool FromProtoBuf(const ItemsDB::Db &db);
    void clear();
};
