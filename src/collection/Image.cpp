#include "image.hpp"
#include <QByteArray>
#include "tools.hpp"
#include "exception.hpp"
#include "sqlite.hpp"

#include "db.hpp"

#include <fstream>

Image::Image() {

}

Image::~Image() {

}

bool Image::ToJSON(json_t *json) {
    if (!IdToJson(json, "id", Id)) return false;
    if (!stringToJson(json, "name", Name)) return false;
//    if (!stringToJson(json, "data", base64)) return false;
    return true;
}

bool Image::FromJSON(json_t *json) {
    if (!IdFromJson(json, "id", Id)) return log_error_parse("id");
    if (!json_parse_string(json, "name", Name)) return log_error_parse("name");
    // if (!json_parse_string(json, "data", base64)) return log_error_parse("data");
    return true;
}

bool Image::ToProtoBuf(ItemsDB::Image *pbf) const {
    pbf->set_id(Id);
    pbf->set_name(Name);
    return true;
}
bool Image::FromProtoBuf(const ItemsDB::Image &pbf) {
    Id = pbf.id();
    Name = pbf.name();
    return true;
}

IdType Image::GetId() const { return Id; }
bool Image::SetId(const IdType &value) { Id = value; return true; }

std::string Image::GetName() const { return Name; }
bool Image::SetName(std::string value) { Name = value; return true; }

bool Image::GetPNGData(std::string &png) {

    std::stringstream ss; ss << DB.get_images_path() << "/" << GetId() << ".png";
    std::string path = ss.str();

    std::ifstream file;
    file.open(path.c_str(), std::ios_base::binary);
    if (file.is_open()) {
        png = std::string((std::istreambuf_iterator<char>(file)),
                         std::istreambuf_iterator<char>());
        file.close();
    } else {
        ExceptionSaver ss; ss << "Can't open image by path: " << path << "    " << strerror(errno);
        return false;
    }

    QImage image = QImage::fromData(reinterpret_cast<const unsigned char *>(png.data()), png.size(), "PNG");
    preview = image.scaled(100, 100, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    return true;
}
bool Image::SetPNGData(const std::string &png) {
    if (png.size() == 0) return false;
    this->png = png;
    std::stringstream ss; ss << DB.get_images_path() << "/" << GetId() << ".png";
    std::string path = ss.str();

    std::ofstream file;
    file.open(path.c_str(), std::ios_base::binary);
    if (file.is_open()) {
        file.write(png.data(), png.size());
        file.close();
    } else {
        ExceptionSaver ss; ss << "Can't save image by path: " << path << "    " << strerror(errno);
        return false;
    }

    QImage image = QImage::fromData(reinterpret_cast<const unsigned char *>(png.data()), png.size(), "PNG");
    preview = image.scaled(100, 100, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    return true;
}

const QImage *Image::GetPreview() {
    if(png.size() == 0) GetPNGData(png);
    return &preview;
}

Image *Images::NewImage() {
    Image* obj = new Image();
    images.push_back(obj);
    return obj;
}

bool Images::DeleteImage(Image *image) {
    images.erase(std::remove(images.begin(), images.end(), image), images.end());
    delete image;
    image = 0;
    return true;
}
unsigned int Images::ImagesSize() const { return images.size(); }
Image* Images::ImageByIndex(unsigned int index) const {
    if (index >= images.size()) return 0;
    return images[index];
}
Image* Images::ImageById(const IdType &id) const {
    for (unsigned int i = 0; i < images.size(); ++i)
        if (id == images[i]->Id) return images[i];
    return 0;
}
bool Images::ImageMove(unsigned int source_index, unsigned int destination_index) {
    return move_vector_element<Image*>(source_index, destination_index, images);
}


bool Images::ToJSON(json_t *json) {
    json_t *json_images = json_array();
    for(unsigned int i = 0; i < images.size(); ++i) {
        json_t *json_obj = json_object();
        if (!images[i]->ToJSON(json_obj)) { std::cout << "Render Image " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_images, json_obj);
    }
    if (json_object_set(json, "images", json_images) != 0) return log_error("images");
    return true;
}

bool Images::FromJSON(json_t *json) {
    json_t *images_json = json_object_get(json, "images");
    if (images_json == NULL) return log_error_parse("images_json json is NULL");
    if (!json_is_array(images_json)) return log_error_parse("images_json is not array");
    for (unsigned int i = 0; i < json_array_size(images_json); ++i) {
        json_t *json_image = json_array_get(images_json, i);
        if (json_image == NULL) return log_error_parse("json_image is NULL");

        if (!json_is_object(json_image)) return log_error_parse("json_image is not object");
        Image *image = NewImage();
        if (!image->FromJSON(json_image)) return false;
    }
    return true;
}

bool Images::ToSQLite(sqlite3 *db) {
    const char *sql = "CREATE TABLE Images("  \
             "KEY    UNSIGNED BIG INT    PRIMARY KEY     NOT NULL," \
             "VALUE  BLOB                                NOT NULL);";
    char *zErrMsg = 0;
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        ExceptionSaver ss; ss << "Images::ToSQLite SQL error: " << zErrMsg;
        sqlite3_free(zErrMsg);
    }

    for(unsigned int i = 0; i < images.size(); ++i) {
        json_t *json = json_object();
        if (!images[i]->ToJSON(json)) { std::cout << "Render Image " << i << " to json error!" << std::endl; return false; }
        char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
        if (str_data == NULL) return log_error("Render Image to json error");
        if(!writeBlob(db, "Images", images[i]->GetId(), str_data, strlen(str_data))) return false;
        delete[] str_data;
    }
    return true;
}
bool Images::FromSQLite(sqlite3 *db) {
    std::vector<std::string> strs;
    if(!readBlob(db, "Images", strs)){
        ExceptionSaver ss; ss << "Images::FromSQLite SQL error: ";
    };
    for (std::string &str : strs) {
        json_error_t error;
        json_t *json = json_loads(str.c_str(), 0, &error);
        if (json == NULL) {
            std::cout << error.text << std::endl;
            ExceptionSaver ss; ss << "Images::FromSQLite json is NULL ";
            return log_error("Images::FromSQLite json is NULL");
        }
        Image *image = NewImage();
        if (!image->FromJSON(json)) {
            ExceptionSaver ss; ss << "image->FromJSON(json)";
            return false;
        }
//        std::stringstream ss; ss << DB.get_images_path() << "/" << image->GetId() << ".png";
//        std::string path = ss.str();
//        std::string base64 = image->GetBase64();
//        QByteArray ba = QByteArray::fromBase64(QByteArray::fromStdString(base64));

//        std::ofstream file;
//        file.open(ss.str().c_str(), std::ios_base::binary);
//        if (file.is_open()) {
//            file.write(ba.data(), ba.size());
//            file.close();
//        } else {
//            ExceptionSaver ss; ss << "Can't save image by path: " << path << "    " << strerror(errno);
//            return false;
//        }
    }
    return true;
}


void Images::clear() {
    for (unsigned int i = 0; i < images.size(); ++i) delete images[i];
    images.clear();
}

bool Images::ToProtoBuf(ItemsDB::Db *db) const {
    for(unsigned int i = 0; i < images.size(); ++i) {
        ItemsDB::Image* image_pbf = db->add_images();
        if (!images[i]->ToProtoBuf(image_pbf)) return log_error("Images::ToProtoBuf image error");
    }
    return true;
}
bool Images::FromProtoBuf(const ItemsDB::Db &db) {
    for(unsigned int i = 0; i < db.images_size(); ++i) {
        Image *image = NewImage();
        if(!image->FromProtoBuf(db.images(i))) return log_error("Images::FromProtoBuf image error");
    }
    return true;
}

