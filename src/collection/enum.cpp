#include "enum.hpp"

#include <algorithm>

#include "parser.hpp"
#include "db.hpp"
#include "exception.hpp"
#include "sqlite.hpp"

IdType Enum::GetId() const { return Id; }
bool Enum::SetId(IdType value) { Id = value; return true; }

std::string Enum::GetName() const { return Name; }
bool Enum::SetName(std::string value) { Name = value; return true; }

std::string Enum::GetDescription() const { return Description; }
bool Enum::SetDescription(std::string value) { Description = value; return true; }

unsigned int Enum::GetValuesSize() const { return values.size(); }
EnumValue Enum::GetValueByIndex(const unsigned int &index) const {
    if(index < values.size()) return values[index];
    ExceptionSaver ss; ss << "Enums::GetValuesSize: Index out of range in enum with id " << Id;
}

std::string Enum::GetValue(const IdType &value_id) const {
    for (unsigned int i = 0; i < values.size(); ++i) {
        if (values[i].GetId() == value_id)
            return values[i].GetValue();
    }
    ExceptionSaver ss; ss << "Enums::GetValue: Can't find enumValue with id = " << value_id <<
        " in enum with id " << Id;
}

bool Enum::UpsertValue(const EnumValue &value) {
    for (unsigned int i = 0; i < values.size(); ++i) {
        if (values[i].GetId() == value.GetId()) {
            values[i] = value;
            return true;
        }
    }
    values.push_back(value);
    return true;
}

bool Enum::MoveValue(const unsigned int &source, const unsigned int &dest) {
    return move_vector_element<EnumValue>(source, dest, values);
}

EnumValueList Enum::GetValues() const { return values; }
bool Enum::SetValues(const EnumValueList& values) { this->values = values; return true; }


bool Enum::ToJSON(json_t *json) {
    if (!IdToJson(json, "id", Id)) return false;
    if (!stringToJson(json, "name", Name)) return false;
    if (!stringToJson(json, "description", Description)) return false;

    json_t *json_values = json_array();
    for(unsigned int i = 0; i < values.size(); ++i) {
        json_t *json_obj = json_object();
        if (!values[i].ToJSON(json_obj)) { std::cout << "Render EnumValue " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_values, json_obj);
    }
    if (json_object_set(json, "values", json_values) != 0) return log_error("json_values");

    return true;
}

bool Enum::FromJSON(json_t *json) {
    if (!IdFromJson(json, "id", Id)) return log_error_parse("id");
    if (!json_parse_string(json, "name", Name)) return log_error_parse("name");
    if (!json_parse_string(json, "description", Description)) return log_error_parse("description");

    values.clear();
    json_t *json_values = json_object_get(json, "values");
    if (json_values == NULL) return log_error("json_values is NULL");
    if (!json_is_array(json_values)) return log_error("json_values is not array");
    for (unsigned int i = 0; i < json_array_size(json_values); ++i) {
        json_t *json_value = json_array_get(json_values, i);
        if (json_value == NULL) return log_error("json_value is NULL");
        if (!json_is_object(json_value)) return log_error("json_value is not object");
        EnumValue enumValue;
        if (!enumValue.FromJSON(json_value)) return log_error("json_value is not EnumValue");
        values.push_back(enumValue);
    }
    return true;
}

bool Enum::ToProtoBuf(ItemsDB::Enum *pbf) const {
    pbf->set_id(Id);
    pbf->set_name(Name);
    pbf->set_description(Description);

    for(unsigned int i = 0; i < values.size(); ++i) {
        ItemsDB::EnumValue* enumValue_pbf = pbf->add_values();
        if (!values[i].ToProtoBuf(enumValue_pbf)) return log_error("Enum::ToProtoBuf enumValue error");
    }
    return true;
}
bool Enum::FromProtoBuf(const ItemsDB::Enum &pbf) {
    Id = pbf.id();
    Name = pbf.name();
    Description = pbf.description();

    values.clear();
    for (unsigned int i = 0; i < pbf.values_size(); ++i) {
        EnumValue enumValue;
        if(!enumValue.FromProtoBuf(pbf.values(i))) return log_error("Enum::FromProtoBuf enumValue error");
        values.push_back(enumValue);
    }
    return true;
}

Enum* Enums::NewEnum() {
    Enum* enum_ptr = new Enum();
    enums.push_back(enum_ptr);
    EnumsMap[enum_ptr->GetId()] = enum_ptr;
    return enum_ptr;
}

bool Enums::DeleteEnum(Enum* enum_ptr) {
    EnumsMap.erase(enum_ptr->GetId());
    enums.erase(std::remove(enums.begin(), enums.end(), enum_ptr), enums.end());
    delete enum_ptr;
    enum_ptr = 0;
    return true;
}

void Enums::clear() {
    for (unsigned int i = 0; i < enums.size(); ++i) delete enums[i];
    enums.clear();
    EnumsMap.clear();
}

unsigned int Enums::EnumsSize() const { return enums.size(); }
Enum* Enums::EnumByIndex(unsigned int index) const {
    if (index >= enums.size()) {
        ExceptionSaver ss; ss << "Enums::EnumByIndex: index out of range. Max range = " << enums.size();
    }
    return enums[index];
}
Enum* Enums::EnumById(const IdType &id) const {
    auto it = EnumsMap.find(id);
    if (it == EnumsMap.end()) {
        ExceptionSaver ss; ss << "Enums::EnumById: Can't find enum with id = " << id;
    }
    return it->second;
}

bool Enums::ToJSON(json_t *json) {
    json_t *json_enums = json_array();
    for(unsigned int i = 0; i < enums.size(); ++i) {
        json_t *json_obj = json_object();
        if (!enums[i]->ToJSON(json_obj)) { std::cout << "Render Enum " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_enums, json_obj);
    }
    if (json_object_set(json, "enums", json_enums) != 0) return log_error("enums");
    return true;
}

bool Enums::FromJSON(json_t *json) {
    json_t *json_enums = json_object_get(json, "enums");
    if (json_enums == NULL) return true; // return log_error("json_enums is NULL");
    if (!json_is_array(json_enums)) return log_error("json_enums is not array");
    for (unsigned int i = 0; i < json_array_size(json_enums); ++i) {
        json_t *json_enum = json_array_get(json_enums, i);
        if (json_enum == NULL) return log_error("json_enum is NULL");

        if (!json_is_object(json_enum)) return log_error("json_enum is not object");
        Enum *enum_ptr = new Enum();
        if (!enum_ptr->FromJSON(json_enum)) return false;
        enums.push_back(enum_ptr);
        EnumsMap[enum_ptr->GetId()] = enum_ptr;
    }
    return true;
}

bool Enums::ToSQLite(sqlite3 *db) {
    const char *sql = "CREATE TABLE Enums("  \
             "KEY    UNSIGNED BIG INT    PRIMARY KEY     NOT NULL," \
             "VALUE  BLOB                                NOT NULL);";
    char *zErrMsg = 0;
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        ExceptionSaver ss; ss << "Enums::ToSQLite SQL error: " << zErrMsg;
        sqlite3_free(zErrMsg);
    }

    for(unsigned int i = 0; i < enums.size(); ++i) {
        json_t *json = json_object();
        if (!enums[i]->ToJSON(json)) { std::cout << "Enums::ToSQLite Render Enum " << i << " to json error!" << std::endl; return false; }
        char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
        if (str_data == NULL) return log_error("Enums::ToSQLite Render Enum to json error");
        if(!writeBlob(db, "Enums", enums[i]->GetId(), str_data, strlen(str_data))) return false;
        delete[] str_data;
    }
    return true;
}

bool Enums::FromSQLite(sqlite3 *db) {
    std::vector<std::string> strs;
    if(!readBlob(db, "Enums", strs)){
        ExceptionSaver ss; ss << "Enums::FromSQLite SQL error: ";
    };
    for (std::string &str : strs) {
        json_error_t error;
        json_t *json = json_loads(str.c_str(), 0, &error);
        if (json == NULL) {
            std::cout << error.text << std::endl;
            return log_error("Enums::FromSQLite json is NULL");
        }
        Enum *enum_ptr = new Enum();
        if (!enum_ptr->FromJSON(json)) return false;
        enums.push_back(enum_ptr);
        EnumsMap[enum_ptr->GetId()] = enum_ptr;
    }
    return true;
}


bool Enums::ToProtoBuf(ItemsDB::Db *db) const {
    for(unsigned int i = 0; i < enums.size(); ++i) {
        ItemsDB::Enum* enum_pbf = db->add_enums();
        if (!enums[i]->ToProtoBuf(enum_pbf)) return log_error("Enums::ToProtoBuf enum error");
    }
    return true;
}
bool Enums::FromProtoBuf(const ItemsDB::Db &db) {
    for(unsigned int i = 0; i < db.enums_size(); ++i) {
        Enum *enum_obj = new Enum();
        if(!enum_obj->FromProtoBuf(db.enums(i))) return log_error("Enums::FromProtoBuf enum error");
        enums.push_back(enum_obj);
        EnumsMap[enum_obj->GetId()] = enum_obj;
    }
    return true;
}


