#pragma once
#include <iostream>

#include <QVector>
#include <QString>
#include <QObject>
#include <QMap>
#include <vector>
#include <deque>
#include <memory>
#include <set>
#include <map>
#include <jansson.h>
#include "sqlite/sqlite3.h"
#include "db.pb.h"

#include "base.hpp"
#include "propertyset.hpp"

class Category : public BaseFolder, public PropertySet {
    friend class FoldersCategories;
    friend class Db;
    friend class Categories;
    friend class Items;

private:
    Category();
    virtual ~Category() {}

    virtual PtrInfo* GetParent() const;
    virtual void SetParent(PtrInfo* parent);

    void ChildrensRemove();
    void ChildrenDrop(Item *item);
    void ChildrenAppend(Item *item, int row = -1);

    virtual bool ToJSON(json_t *json);
    virtual bool FromJSON(json_t *json);
    virtual bool ToProtoBuf(ItemsDB::Category *pbf) const;
    virtual bool FromProtoBuf(const ItemsDB::Category &pbf);

public:
    virtual void Clear();
    virtual std::vector<PtrInfo*> Childrens() const;
    virtual size_t ChildrenSize() const;
    virtual PtrInfo* Children(unsigned int index) const;

    virtual void SetShowOnSiteForChildrens(bool value);

private:
    std::vector<IdType> items;
};


class Categories {
public:
    Category *NewCategory(Folder *parent);
    void DeleteCategory(Category *item);
    unsigned int CategoriesSize() const;
    Category* CategoryByIndex(unsigned int index) const;
    Category* CategoryById(const IdType &id) const;
    bool CategoryMove(unsigned int source_index, unsigned int destination_index);

    PtrInfo* GetCategoryParent(const IdType &category_id) const;
    bool SetCategoryParent(const IdType &category_id, Folder *parent);

protected:
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db);
    bool ToProtoBuf(ItemsDB::Db *db) const;
    bool FromProtoBuf(const ItemsDB::Db &db);
    void clear();

    std::map<IdType, std::set<Folder*>> CategoriesMap;

private:
    std::vector<Category*> categories;

    // Category Id - Category Ptr
    std::map<IdType, Category*> categories_ptr;
};
