#pragma once
#include <cstdint>
#include <jansson.h>
#include <string>
#include <vector>

typedef uint64_t IdType;
typedef std::vector<IdType> IdVector;

IdType uuid(const uint8_t type = 0);

json_t* IdToJson(const IdType &id);
bool IdToJson(json_t *json, std::string name, IdType id);
bool IdFromJson(const json_t *object, IdType &id);
bool IdFromJson(const json_t *object, const char * key, IdType &id);
