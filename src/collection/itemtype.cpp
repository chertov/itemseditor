#include "itemtype.hpp"
#include "sqlite.hpp"
#include "exception.hpp"

IdType ItemType::GetId() const { return Id; }
bool ItemType::SetId(const IdType &value) { Id = value; return true; }

std::string ItemType::GetName() const { return Name; }
bool ItemType::SetName(std::string value) { Name = value; return true; }

std::string ItemType::GetShortDescription() const { return ShortDescription; }
bool ItemType::SetShortDescription(std::string value) { ShortDescription = value; return true; }

std::string ItemType::GetDescription() const { return Description; }
bool ItemType::SetDescription(std::string value) { Description = value; return true; }

IdType ItemType::GetImage() const { return Image; }
bool ItemType::SetImage(const IdType &value) { Image = value; return true; }

bool ItemType::ToJSON(json_t *json) {
    if (!IdToJson(json, "id", Id)) return false;
    if (!stringToJson(json, "name", Name)) return false;
    if (!stringToJson(json, "short_description", ShortDescription)) return false;
    if (!stringToJson(json, "description", Description)) return false;
    if (!IdToJson(json, "image", Image)) return false;
    return true;
}

bool ItemType::FromJSON(json_t *json) {
    if (!IdFromJson(json, "id", Id)) return log_error_parse("id");
    if (!json_parse_string(json, "name", Name)) return log_error_parse("name");
    if (!json_parse_string(json, "short_description", ShortDescription)) return log_error_parse("short_description");
    if (!json_parse_string(json, "description", Description)) return log_error_parse("description");
    if (!IdFromJson(json, "image", Image)) return log_error_parse("image");
    return true;
}

bool ItemType::ToProtoBuf(ItemsDB::ItemType *pbf) const {
    pbf->set_id(Id);
    pbf->set_name(Name);
    pbf->set_shortdescription(ShortDescription);
    pbf->set_description(Description);
    pbf->set_image(Image);
    return true;
}
bool ItemType::FromProtoBuf(const ItemsDB::ItemType &pbf) {
    Id = pbf.id();
    Name = pbf.name();
    ShortDescription = pbf.shortdescription();
    Description = pbf.description();
    Image = pbf.image();
    return true;
}



ItemType *ItemsTypes::NewItemType() {
    ItemType* obj = new ItemType();
    itemsTypes.push_back(obj);
    return obj;
}

bool ItemsTypes::DeleteItemType(ItemType *itemType) {
    itemsTypes.erase(std::remove(itemsTypes.begin(), itemsTypes.end(), itemType), itemsTypes.end());
    delete itemType;
    return true;
}

bool ItemsTypes::ToJSON(json_t *json) {
    json_t *json_itemstypes = json_array();
    for(unsigned int i = 0; i < itemsTypes.size(); ++i) {
        json_t *json_obj = json_object();
        if (!itemsTypes[i]->ToJSON(json_obj)) { std::cout << "Render ItemType " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_itemstypes, json_obj);
    }
    if (json_object_set(json, "items_types", json_itemstypes) != 0) return log_error("items_types");
    return true;
}

bool ItemsTypes::FromJSON(json_t *json) {
    json_t *itemstypes_json = json_object_get(json, "items_types");
    if (itemstypes_json == NULL) return log_error("items_types json is NULL");
    if (!json_is_array(itemstypes_json)) return log_error("items_types is not array");
    for (unsigned int i = 0; i < json_array_size(itemstypes_json); ++i) {
        json_t *json_itemtype = json_array_get(itemstypes_json, i);
        if (json_itemtype == NULL) return log_error("json_itemtype is NULL");

        if (!json_is_object(json_itemtype)) return log_error("json_itemtype is not object");
        ItemType *itemType = NewItemType();
        if (!itemType->FromJSON(json_itemtype)) return false;
    }
    return true;
}

void ItemsTypes::clear() {
    for (unsigned int i = 0; i < itemsTypes.size(); ++i) delete itemsTypes[i];
    itemsTypes.clear();
}

bool ItemsTypes::ToSQLite(sqlite3 *db) {
    const char *sql = "CREATE TABLE ItemsTypes("  \
             "KEY    UNSIGNED BIG INT    PRIMARY KEY     NOT NULL," \
             "VALUE  BLOB                                NOT NULL);";
    char *zErrMsg = 0;
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        ExceptionSaver ss; ss << "ItemsTypes::ToSQLite SQL error: " << zErrMsg;
        sqlite3_free(zErrMsg);
    }

    for(unsigned int i = 0; i < itemsTypes.size(); ++i) {
        json_t *json = json_object();
        if (!itemsTypes[i]->ToJSON(json)) { std::cout << "ItemsTypes::ToSQLite Render ItemsType " << i << " to json error!" << std::endl; return false; }
        char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
        if (str_data == NULL) return log_error("ItemsTypes::ToSQLite Render ItemsType to json error");
        if(!writeBlob(db, "ItemsTypes", itemsTypes[i]->GetId(), str_data, strlen(str_data))) return false;
        delete[] str_data;
    }
    return true;
}

bool ItemsTypes::FromSQLite(sqlite3 *db) {
    std::vector<std::string> strs;
    if(!readBlob(db, "ItemsTypes", strs)){
        ExceptionSaver ss; ss << "ItemsTypes::FromSQLite SQL error: ";
    };
    for (std::string &str : strs) {
        json_error_t error;
        json_t *json = json_loads(str.c_str(), 0, &error);
        if (json == NULL) {
            std::cout << error.text << std::endl;
            return log_error("ItemsTypes::FromSQLite json is NULL");
        }
        ItemType *itemType = NewItemType();
        if (!itemType->FromJSON(json)) return false;
    }
    return true;
}

bool ItemsTypes::ToProtoBuf(ItemsDB::Db *db) const {
    for(unsigned int i = 0; i < itemsTypes.size(); ++i) {
        ItemsDB::ItemType* itemType_pbf = db->add_itemstypes();
        if (!itemsTypes[i]->ToProtoBuf(itemType_pbf)) return log_error("ItemsTypes::ToProtoBuf itemType error");
    }
    return true;
}
bool ItemsTypes::FromProtoBuf(const ItemsDB::Db &db) {
    for(unsigned int i = 0; i < db.itemstypes_size(); ++i) {
        ItemType *itemType = new ItemType();
        if(!itemType->FromProtoBuf(db.itemstypes(i))) return log_error("ItemsTypes::FromProtoBuf itemType error");
        itemsTypes.push_back(itemType);
    }
    return true;
}

