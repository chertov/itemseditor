#include "idtype.hpp"
#include <ctime>
#include <chrono>
#include <iostream>
#include <map>
#include <cstdlib>
#include <thread>
#include <bitset>
#include <climits>


// 011011000101001101001001100101101111^0000000000000000^10101^1000001
// timetimetimetimetimetimetimetimetime|countercounterco|rando|typetyp|
//  36 bit timestamp: milliseconds/50  |  counter 16bit | 5bit| type  |

static uint16_t counter = 0;
IdType uuid(const uint8_t type) {
    std::chrono::high_resolution_clock::time_point time = std::chrono::high_resolution_clock::now();
    uint64_t id = std::chrono::duration_cast<std::chrono::milliseconds>(time.time_since_epoch()).count() / 50;
    // 36 бит - timestamp (милисекунды) / 50
    // 16 бит - счетчик для устранения конфликтов
    // 5 бит - случайно число 0..32
    // 7 бит - тип объекта, которому принадлежит идентификатор
    id <<= 16; id += counter++;     // счетчик
    id <<= 5;  id += rand() % 32;   // случайное число
    id <<= 7;  id += type;          // тип
    return id;
}

json_t* IdToJson(const IdType &id) {
    return json_integer(id);
}

bool IdToJson(json_t *json, std::string name, IdType id) {
    json_t *json_id = json_integer(id);
    if (json_object_set(json, name.c_str(), json_id) != 0) {
        std::cout << "JSON render error id: " << name.c_str() << std::endl;
        return false;
    }
    return true;
}

std::map<std::string, IdType> ids;

bool IdFromJson(const json_t *json, IdType &id) {
    if (json == NULL) { std::cout << "json id is NULL" << std::endl; return false; }
    if (json_is_string(json)) {
        std::string val = json_string_value(json);
        auto it = ids.find(val);
        if (it == ids.end()) {
            if(val.size() == 0) { id = 0; return true; }
            id = uuid();
            ids[val] = id;
        } else {
            id = it->second;
        }
    } else {
        if (!json_is_integer(json)) { std::cout << "json id is not integer or string" << std::endl; return false; }
        id = json_integer_value(json);
    }
    return true;
}

bool IdFromJson(const json_t *object, const char *key, IdType &id) {
    if (object == NULL) { std::cout << "object id is NULL" << std::endl; return false; }
    json_t *json = json_object_get(object, key);
    return IdFromJson(json, id);
}
