#pragma once
#include <string>
#include "sqlite/sqlite3.h"
#include "db.pb.h"

#include "tools.hpp"
#include "idtype.hpp"
#include "parser.hpp"

class Producer {
    friend class Producers;
public:
    IdType GetId() const;
    bool SetId(IdType value);

    std::string GetName() const;
    bool SetName(std::string value);

    std::string GetURL() const;
    bool SetURL(std::string value);

    std::string GetShortDescription() const;
    bool SetShortDescription(std::string value);

    std::string GetDescription() const;
    bool SetDescription(std::string value);

    IdType GetImage() const;
    bool SetImage(const IdType &value);

private:
    Producer() {}
    virtual ~Producer() {}

    IdType Id = uuid();
    std::string Name = "Новый производитель";
    std::string URL;
    std::string ShortDescription;
    std::string Description;
    IdType Image = 0;

    virtual bool ToJSON(json_t *json);
    virtual bool FromJSON(json_t *json);
    virtual bool ToProtoBuf(ItemsDB::Producer *pbf) const;
    virtual bool FromProtoBuf(const ItemsDB::Producer &pbf);
};

class Producers {
public:
    Producer *NewProducer();
    bool DeleteProducer(Producer *producer);
    unsigned int ProducersSize() const { return producers.size(); }
    Producer* ProducerByIndex(unsigned int index) const {
        if (index >= producers.size()) return 0;
        return producers[index];
    }
    Producer* ProducerById(const IdType id) const {
        for (unsigned int i = 0; i < producers.size(); ++i)
            if (id == producers[i]->Id) return producers[i];
        return 0;
    }
    bool ProducerMove(unsigned int source_index, unsigned int destination_index) {
        return move_vector_element<Producer*>(source_index, destination_index, producers);
    }

protected:
    std::vector<Producer *> producers;
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db);
    bool ToProtoBuf(ItemsDB::Db *db) const;
    bool FromProtoBuf(const ItemsDB::Db &db);
    void clear();
};
