#include "db.hpp"

#include "parser.hpp"
#include "tools.hpp"

#include "category.hpp"
#include "item.hpp"
#include "exception.hpp"
#include "logger.hpp"
#include "db.pb.h"

#include <QFile>
#include <QSettings>


Db::Db() : FoldersCategories() {
    get_images_path();
}
Db::~Db() {

}

bool Db::ToJSON(std::string &str) {
    json_t *json = json_object();

    json_t *json_root = json_object();
    if(!FoldersCategories::ToJSON(json_root)) return log_error("Render FoldersCategories to json error");
    if (json_object_set(json, "root", json_root) != 0) return log_error("root");

    if(!Folders::ToJSON(json)) return log_error("Render Folders to json error");
    if(!Categories::ToJSON(json)) return log_error("Render Categories to json error");
    if(!Items::ToJSON(json)) return log_error("Render Items to json error");

    if(!Properties::ToJSON(json)) return log_error("Render Properties to json error");
    if(!Enums::ToJSON(json)) return log_error("Render Enums to json error");

    if(!Producers::ToJSON(json)) return log_error("Render Producers to json error");
    if(!ItemsTypes::ToJSON(json)) return log_error("Render ItemsTypes to json error");
    if(!Images::ToJSON(json)) return log_error("Render Images to json error");

    char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
    if (str_data == NULL) return log_error("Render DB to json error");
    str = std::string(str_data);
    delete[] str_data;
    return true;
}

bool Db::FromJSON(std::string &str) {
    Clear();

    json_error_t error;
    json_t *json = json_loads(str.c_str(), 0, &error);
    if (json == NULL) {
        std::cout << error.text << std::endl;
        return log_error("DB root json is NULL");
    }

    if(!Images::FromJSON(json)) return false;
    if(!Producers::FromJSON(json)) return false;
    if(!ItemsTypes::FromJSON(json)) return false;

    if(!Categories::FromJSON(json)) return false;
    if(!Folders::FromJSON(json)) return false;
    if(!Items::FromJSON(json)) return false;

    json_t *json_root = json_object_get(json, "root");
    if (json_root == NULL) return log_error("json_root is NULL");
    if(!FoldersCategories::FromJSON(json_root, 0)) return false;
    return true;
}


bool Db::ToSQLite(const std::string &path) {
    LOG << "Save sqlite to file: " << path;
    sqlite3 *db;
    char *zErrMsg = 0;
    QFile file (path.c_str()); file.remove();
    int rc = sqlite3_open(path.c_str(), &db);
    if( rc ){
        ExceptionSaver ss; ss << "Items::ToSQLite SQL error: " << zErrMsg;
        return false;
    }
    {
        char *zErrMsg = 0;
        int rc = sqlite3_exec(db, "BEGIN;", NULL, 0, &zErrMsg);
        if( rc != SQLITE_OK ){
            ExceptionSaver ss; ss << "Items::ToSQLite BEGIN error: " << zErrMsg;
            sqlite3_free(zErrMsg);
        }
    }
    {
        const char *sql = "CREATE TABLE KeyValue("  \
                 "KEY    TEXT    PRIMARY KEY    NOT NULL," \
                 "VALUE  BLOB                   NOT NULL);";
        char *zErrMsg = 0;
        int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
        if( rc != SQLITE_OK ){
            ExceptionSaver ss; ss << "Items::ToSQLite CREATE TABLE KeyValue sql error: " << zErrMsg;
            sqlite3_free(zErrMsg);
        }
    }

    if(!FoldersCategories::ToSQLite(db)) return log_error("ToSQL FoldersCategories error");
    //if (json_object_set(json, "root", json_root) != 0) return log_error("root");

    if(!Folders::ToSQLite(db)) return log_error("ToSQL Folders error");
    if(!Categories::ToSQLite(db)) return log_error("ToSQL Categories error");
    if(!Items::ToSQLite(db)) return log_error("ToSQL Items error");

    if(!Properties::ToSQLite(db)) return log_error("ToSQL Properties error");
    if(!Enums::ToSQLite(db)) return log_error("ToSQL Enums error");

    if(!Producers::ToSQLite(db)) return log_error("ToSQL Producers error");
    if(!ItemsTypes::ToSQLite(db)) return log_error("ToSQL ItemsTypes error");
    if(!Images::ToSQLite(db)) return log_error("ToSQL Images error");

    {
        char *zErrMsg = 0;
        int rc = sqlite3_exec(db, "COMMIT;", NULL, 0, &zErrMsg);
        if( rc != SQLITE_OK ){
            ExceptionSaver ss; ss << "Items::ToSQLite END error: " << zErrMsg;
            sqlite3_free(zErrMsg);
        }
    }
    sqlite3_close(db);

    return true;
}

bool Db::FromSQLite(const std::string &path) {
    LOG << "Load sqlite from file: " << path;
    if (path.size() == 0) return false;
    Clear();

    sqlite3 *db;
    char *zErrMsg = 0;
    int rc = sqlite3_open(path.c_str(), &db);
    if( rc ){
        ExceptionSaver ss; ss << "Items::FromSQLite SQL error: " << zErrMsg;
        return false;
    }

    if(!Images::FromSQLite(db)) return log_error("Images FromSQLite error");
    if(!Producers::FromSQLite(db)) return log_error("Producers FromSQLite error");
    if(!ItemsTypes::FromSQLite(db)) return log_error("ItemsTypes FromSQLite error");

    if(!Categories::FromSQLite(db)) return log_error("Categories FromSQLite error");
    if(!Folders::FromSQLite(db)) return log_error("Folders FromSQLite error");
    if(!Items::FromSQLite(db)) return log_error("Items FromSQLite error");

    if(!FoldersCategories::FromSQLite(db, 0)) return log_error("FoldersCategories FromSQLite error");

    sqlite3_close(db);
    return true;
}

bool Db::ToProtocolBuffers(const std::string &path) {
    ItemsDB::Db pbuf_db;
    FoldersCategories::ToProtoBuf(pbuf_db.mutable_root());

    if(!Folders::ToProtoBuf(&pbuf_db)) return log_error("Render Folders to protobuf error");
    if(!Categories::ToProtoBuf(&pbuf_db)) return log_error("Render Categories to protobuf error");
    if(!Items::ToProtoBuf(&pbuf_db)) return log_error("Render Items to protobuf error");

    if(!Properties::ToProtoBuf(&pbuf_db)) return log_error("Render Properties to protobuf error");
    if(!Enums::ToProtoBuf(&pbuf_db)) return log_error("Render Enums to protobuf error");

    if(!Producers::ToProtoBuf(&pbuf_db)) return log_error("Render Producers to protobuf error");
    if(!ItemsTypes::ToProtoBuf(&pbuf_db)) return log_error("Render ItemsTypes to protobuf error");
    if(!Images::ToProtoBuf(&pbuf_db)) return log_error("Render Images to protobuf error");

    std::ofstream file;
    file.open(path.c_str(), std::ios_base::binary);
    if (file.is_open()) {
        pbuf_db.SerializeToOstream(&file);
        file.close();
    } else {
        ExceptionSaver ss; ss << "Error: " << strerror(errno);
        return false;
    }
    return true;
}

bool Db::FromProtocolBuffers(const std::string &path) {
    ItemsDB::Db pbuf_db;

    std::ifstream file;
    file.open(path.c_str(), std::ios_base::binary);
    if (file.is_open()) {
        pbuf_db.ParseFromIstream(&file);
        file.close();
    } else {
        ExceptionSaver ss; ss << "Error: " << strerror(errno);
        return false;
    }
    std::cout << "Items: " << pbuf_db.items_size() << std::endl;
    std::cout << "Folders: " << pbuf_db.folders_size() << std::endl;
    std::cout << "Categories: " << pbuf_db.categories_size() << std::endl;

    if(!Images::FromProtoBuf(pbuf_db)) return false;
    if(!Producers::FromProtoBuf(pbuf_db)) return false;
    if(!ItemsTypes::FromProtoBuf(pbuf_db)) return false;

    if(!Categories::FromProtoBuf(pbuf_db)) return false;
    if(!Folders::FromProtoBuf(pbuf_db)) return false;
    if(!Items::FromProtoBuf(pbuf_db)) return false;

    if(!FoldersCategories::FromProtoBuf(pbuf_db.root(), 0)) return false;

    return true;
}

void Db::printStat() const {
    std::cout << "FoldersMap: " << FoldersMap.size() << std::endl;
    std::cout << "CategoriesMap: " << CategoriesMap.size() << std::endl;
    //std::cout << "ItemsMap: " << ItemsMap.size() << std::endl;
}

void Db::Clear() {
    FoldersCategories::Clear();

    //printStat();
    Images::clear();
    ItemsTypes::clear();
    Producers::clear();

    Folders::clear();
    Categories::clear();
    Items::clear();

    //ImagesList.clear();
}

bool Db::DeletePtr(PtrInfo *ptrInfo) {
    ptrInfo = const_cast<PtrInfo*>(check(ptrInfo));
    if (ptrInfo == 0) return error("DeletePtr ptrInfo == 0");

    switch (ptrInfo->Type()) {
    case FOLDER: {
        Folder* folder = static_cast<Folder*>(ptrInfo);
        folder->ChildrensRemove();
        for(auto parent : FoldersMap[folder->GetId()]) {
            if(parent == 0) ChildrenDrop(folder); else parent->ChildrenDrop(folder);
        }
        DeleteFolder(folder);
        break;
    }
    case CATEGORY: {
        Category* category = static_cast<Category*>(ptrInfo);
        category->ChildrensRemove();
        for(auto parent : CategoriesMap[category->GetId()]) {
            if(parent == 0) ChildrenDrop(category); else parent->ChildrenDrop(category);
        }
        DeleteCategory(category);
        break;
    }
    case ITEM: {
        Item* item = static_cast<Item*>(ptrInfo);
        for(auto parent : GetItemParents(item->GetId())) {
            if (parent == 0) return error("DeletePtr Item.parent == 0");
            parent->ChildrenDrop(item);
        }
        DeleteItem(item);
        break;
    }
    default:
        return error("DeletePtr unknown type");
        break;
    }
    return true;
}

bool Db::appendFolder(Folder *parent, Folder *folder) {
    if (folder == 0) return error("appendFolder folder == 0");
    if (folder->Type() != FOLDER) return error("appendFolder folder is not FOLDER");
    // TODO проверить дерево folder

    if (parent != 0) {
        if(checkPtr(parent)) {
            if(parent->Type() != FOLDER) return error("appendFolder parent not FOLDER");
            // TODO проверить дерево parent
            parent->folders.push_back(folder->GetId());
        } else {
            //folder->SetParent(0);
            FoldersCategories::folders.push_back(folder->GetId());
        }
    } else {
        FoldersCategories::folders.push_back(folder->GetId());
    }
    return true;
}
bool Db::appendCategory(Folder *parent, Category *category) {
    if (category == 0) return error("appendCategory category == 0");
    if (category->Type() != CATEGORY) return error("appendCategory category is not CATEGORY");
    // TODO проверить дерево category

    if (parent != 0) {
        if(checkPtr(parent)) {
            if(parent->Type() != FOLDER) return error("appendCategory parent is not FOLDER");
            // TODO проверить дерево parent
            parent->categories.push_back(category->GetId());
        } else {
            //category->SetParent(0);
            FoldersCategories::categories.push_back(category->GetId());
        }
    } else {
        FoldersCategories::categories.push_back(category->GetId());
    }
    return true;
}
bool Db::appendItem(Category *parent, Item *item) {
    if (item == 0) return error("appendItem item == 0");
    if (item->Type() != ITEM) return error("appendItem item is not ITEM");
    // TODO проверить дерево item

    if(!checkPtr(parent)) return false;
    if (parent == 0) return error("appendItem parent == 0");
    if(parent->Type() != CATEGORY) return error("appendItem parent is not CATEGORY");
    // TODO проверить дерево parent
    parent->items.push_back(item->GetId());
    return true;
}

bool Db::moveFolder(Folder *newParent, Folder *folder, int row) {
    if (folder == 0) return error("moveFolder folder == 0");
    if (folder->Type() != FOLDER) return error("moveFolder folder is not FOLDER");
    if(!checkPtr(folder)) return error("moveFolder checkPtr(folder) is false");

    if (newParent != 0) {
        if (newParent->Type() != FOLDER) return error("moveFolder newParent is not FOLDER");
        if (!checkPtr(newParent)) return error("moveFolder checkPtr(newParent) is false");
    }

    if(folder->GetParent() == 0) {
        ChildrenDrop(folder);
    } else {
        Folder *parent = static_cast<Folder*>(folder->GetParent());
        parent->ChildrenDrop(folder);
    }

    if (newParent != 0) {
        newParent->ChildrenAppend(folder, row);
    } else {
        ChildrenAppend(folder, row);
    }
    SetFolderParent(folder->GetId(), newParent);
    return true;
}
bool Db::moveCategory(Folder *newParent, Category *category, int row) {
    if (category == 0) return error("moveCategory category == 0");
    if (category->Type() != CATEGORY) return error("moveCategory category is not CATEGORY");
    if (!checkPtr(category)) return error("moveCategory checkPtr(category) is false");

    if (newParent != 0) {
        if (newParent->Type() != FOLDER) return error("moveCategory newParent is not FOLDER");
        if(!checkPtr(newParent)) return error("moveCategory checkPtr(newParent) is false");
    }

    if(category->GetParent() == 0) {
        ChildrenDrop(category);
    } else {
        Folder *parent = static_cast<Folder*>(category->GetParent());
        parent->ChildrenDrop(category);
    }

    if (newParent != 0) {
        newParent->ChildrenAppend(category, row);
    } else {
        ChildrenAppend(category, row);
    }
    SetCategoryParent(category->GetId(), newParent);

    return true;
}
bool Db::moveItem(Category *newParent, Item *item, int row) {
    if (item == 0) return error("moveItem item == 0");
    if (newParent == 0) return error("moveItem newParent == 0");
    if (item->Type() != ITEM) return error("moveItem item is not ITEM");
    if (newParent->Type() != CATEGORY) return error("moveItem newParent is not CATEGORY");

    if(!checkPtr(item)) return error("moveItem checkPtr(item) is false");
    if(!checkPtr(newParent)) return error("moveItem checkPtr(newParent) is false");

    if(item->GetParent() == 0) return error("moveItem parent == 0");

    Category *parent = static_cast<Category*>(item->GetParent());
    parent->ChildrenDrop(item);
    newParent->ChildrenAppend(item, row);
    SetItemParent(item->GetId(), newParent);
    return true;
}

bool Db::checkPtr(void *ptr){
    const PtrInfo* newPtr = check(ptr);
    return newPtr == ptr;
}
const PtrInfo* Db::check(void *ptr){
    PtrInfo* ptrInfo = static_cast<PtrInfo*>(ptr);
    if (ptrInfo == 0) return 0;

//    switch (ptrInfo->Type()) {
//    case FOLDER: {
//        Folder* folder = static_cast<Folder*>(ptrInfo);
//        if ( FoldersMap.find(folder->GetId()) != FoldersMap.end() ) {
//            if (FoldersMap[folder->GetId()].size() < 1) {
//                FoldersMap.erase(folder->GetId());
//                error("checkPtr folder not found parents");
//                return 0;
//            } else {
//                if (ptrInfo->Type() != FOLDER) { error("checkPtr folder have incorrect type"); return 0; }
//                return ptrInfo;
//            }
//        }
//        break; }

//    case CATEGORY: {
//        Category* category = static_cast<Category*>(ptrInfo);
//        if ( CategoriesMap.find(category->GetId()) != CategoriesMap.end() ) {
//            if (CategoriesMap[category->GetId()].size() < 1) {
//                CategoriesMap.erase(category->GetId());
//                error("checkPtr item not found parents");
//                return 0;
//            } else {
//                if (ptrInfo->Type() != CATEGORY) { error("checkPtr category have incorrect type"); return 0; }
//                return ptrInfo;
//            }
//        }
//        break; }

//    case ITEM: {
//        Item* item = static_cast<Item*>(ptrInfo);
//        item = ItemById(item->GetId());
//        if(item == 0) {
//            if (GetItemParents(item->GetId()).size() < 1) {
//                error("checkPtr item not found parents"); return 0;
//            } else {
//                if (ptrInfo->Type() != ITEM) { error("checkPtr item have incorrect type"); return 0; }
//                return ptrInfo;
//            }
//        }
//        break; }
//    }

    return ptrInfo;
}

void Db::set_images_path(std::string path) {
    {
        QSettings settings;
        QString str = QString::fromLocal8Bit(path.data(), path.size());
        settings.setValue("ItemsEditor/images_path", str);
    }
    images_path = "";
    get_images_path();
}
std::string Db::get_images_path() {
    if(images_path.size() == 0) {
        QSettings settings;
        QString str = settings.value("ItemsEditor/images_path").toString();
        images_path = str.toLocal8Bit().toStdString();
    }
    return images_path;
}
