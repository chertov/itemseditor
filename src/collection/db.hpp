#pragma once
#include <iostream>

#include <QVector>
#include <QString>
#include <QObject>
#include <QMap>
#include <vector>
#include <map>
#include <set>

#include <jansson.h>

#include "base.hpp"
#include "folder.hpp"
#include "producer.hpp"
#include "item.hpp"
#include "itemtype.hpp"
#include "category.hpp"
#include "image.hpp"
#include "enum.hpp"
#include "enumvalue.hpp"
#include "property.hpp"
#include "propertyvalue.hpp"

#define DB Db::Instance()

class Db : public FoldersCategories,
        public ItemsTypes,
        public Producers,
        public Images,
        public Items,
        public Categories,
        public Folders,
        public Enums,
        public Properties {
    friend class FoldersCategories;
    friend class Item;
    friend class Items;
    friend class Category;
    friend class Categories;
    friend class Folder;
    friend class Folders;
public:

    bool ToJSON(std::string &str);
    bool FromJSON(std::string &str);

    bool ToSQLite(const std::string &path);
    bool FromSQLite(const std::string &path);

    bool ToProtocolBuffers(const std::string &path);
    bool FromProtocolBuffers(const std::string &path);

    bool DeletePtr(PtrInfo *ptrInfo);

    bool appendFolder(Folder *parent, Folder *folder);
    bool appendCategory(Folder *parent, Category *category);
    bool appendItem(Category *parent, Item *item);

    bool moveFolder(Folder *newFolder, Folder *folder, int row = -1);
    bool moveCategory(Folder *newFolder, Category *category, int row = -1);
    bool moveItem(Category *newCategory, Item *item, int row = -1);

    bool checkPtr(void *ptr);
    const PtrInfo* check(void *ptr);

    void printStat() const;

    void set_images_path(std::string path);
    std::string get_images_path();

    static Db& Instance() {
        static Db instance;
        return instance;
    }

    virtual void Clear();

private:
    Db();
    virtual ~Db();

    std::string images_path;
};
