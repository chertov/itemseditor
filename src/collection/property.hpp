#pragma once

#include <string>
#include <vector>
#include <jansson.h>
#include <unordered_map>

#include "idtype.hpp"
#include "enum.hpp"
#include "tools.hpp"

enum PropertyType {
    PROPERTY_BOOL,
    PROPERTY_INT,
    PROPERTY_REAL,
    PROPERTY_ENUM,
    PROPERTY_UNKNOWN
};

inline std::string typeToStr(PropertyType type) {
    switch (type) {
    case PROPERTY_BOOL: return std::string("BOOL");
    case PROPERTY_INT:  return std::string("INT");
    case PROPERTY_REAL: return std::string("REAL");
    case PROPERTY_ENUM: return std::string("ENUM");
    case PROPERTY_UNKNOWN: return std::string("UNKNOWN");
    }
    return std::string("UNKNOWN");
}

inline PropertyType strToType(std::string str) {
    if (str.compare("BOOL") == 0) return PROPERTY_BOOL;
    if (str.compare("INT") == 0) return PROPERTY_INT;
    if (str.compare("REAL") == 0) return PROPERTY_REAL;
    if (str.compare("ENUM") == 0) return PROPERTY_ENUM;
    return PROPERTY_UNKNOWN;
}

class Property {
    friend class PropertySet;
    friend class Properties;
public:
    IdType GetId() const;
    bool SetId(IdType value);

    PropertyType GetType() const;
    bool SetType(PropertyType value);

    std::string GetName() const;
    bool SetName(std::string value);

    std::string GetDescription() const;
    bool SetDescription(std::string value);

    bool GetShowOnSite() const;
    bool SetShowOnSite(bool value);

    bool GetShowInSearch() const;
    bool SetShowInSearch(bool value);

    IdType GetEnum() const;
    Enum* GetEnumPtr() const;
    bool SetEnum(const IdType& enum_id);

    bool GetEnumValue(const IdType &Id, std::string &value) const;
    bool operator==(const Property& other);

private:
    Property() {}
    virtual ~Property() {}

    IdType Id = uuid();
    PropertyType Type = PROPERTY_BOOL;
    std::string Name;
    std::string Description;
    bool ShowOnSite;
    bool ShowInSearch;

    IdType enum_id = 0;

private:
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToProtoBuf(ItemsDB::Property *pbf) const;
    bool FromProtoBuf(const ItemsDB::Property &pbf);
};
typedef std::vector<Property*> PropertyList;

class Properties {
public:
    Property* NewProperty();
    bool DeleteProperty(Property* property);
    unsigned int PropertiesSize() const;
    Property* PropertyByIndex(unsigned int index) const;
    Property* PropertyById(const IdType &id) const;

protected:
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db);
    bool ToProtoBuf(ItemsDB::Db *db) const;
    bool FromProtoBuf(const ItemsDB::Db &db);
    void clear();

private:
    std::unordered_map<IdType, Property*> PropertiesMap;
    std::vector<Property*> properties;
};
