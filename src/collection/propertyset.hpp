#pragma once

#include <string>
#include <vector>
#include <jansson.h>

#include "db.pb.h"
#include "idtype.hpp"
#include "tools.hpp"
#include "property.hpp"

class PropertySet {

public:
    PropertySet() {}
    virtual ~PropertySet() {}

    IdVector GetProperties() const;
    unsigned int GetPropertiesSize() const;
    IdType GetPropertyByIndex(unsigned int index) const;
    bool SetProperties(const IdVector &value);

    PropertyList GetPropertiesPtr() const;
    Property* GetPropertyPtrByIndex(unsigned int index) const;

    bool PropertyMove(unsigned int source_index, unsigned int destination_index);

    Property* GetProperty(const IdType &Id) const;

private:
    IdType id = uuid();
    IdVector properties;

protected:
    virtual bool ToJSON(json_t *json);
    virtual bool FromJSON(json_t *json);
    virtual bool ToProtoBuf(ItemsDB::PropertySet *pbf) const;
    virtual bool FromProtoBuf(const ItemsDB::PropertySet &pbf);
};


