#include "producer.hpp"
#include "sqlite.hpp"
#include "exception.hpp"

IdType Producer::GetId() const { return Id; }
bool Producer::SetId(IdType value) { Id = value; return true; }

std::string Producer::GetName() const { return Name; }
bool Producer::SetName(std::string value) { Name = value; return true; }

std::string Producer::GetURL() const { return URL; }
bool Producer::SetURL(std::string value) { URL = value; return true; }

std::string Producer::GetShortDescription() const { return ShortDescription; }
bool Producer::SetShortDescription(std::string value) { ShortDescription = value; return true; }

std::string Producer::GetDescription() const { return Description; }
bool Producer::SetDescription(std::string value) { Description = value; return true; }

IdType Producer::GetImage() const { return Image; }
bool Producer::SetImage(const IdType &value) { Image = value; return true; }

bool Producer::ToJSON(json_t *json) {
    if (!IdToJson(json, "id", Id)) return false;
    if (!stringToJson(json, "name", Name)) return false;
    if (!stringToJson(json, "url", URL)) return false;
    if (!stringToJson(json, "short_description", ShortDescription)) return false;
    if (!stringToJson(json, "description", Description)) return false;
    if (!IdToJson(json, "image", Image)) return false;
    return true;
}

bool Producer::FromJSON(json_t *json) {
    if (!IdFromJson(json, "id", Id)) return log_error_parse("id");
    if (!json_parse_string(json, "name", Name)) return log_error_parse("name");
    if (!json_parse_string(json, "url", URL)) return log_error_parse("url");
    if (!json_parse_string(json, "short_description", ShortDescription)) return log_error_parse("short_description");
    if (!json_parse_string(json, "description", Description)) return log_error_parse("description");
    if (!IdFromJson(json, "image", Image)) return log_error_parse("image");
    return true;
}

bool Producer::ToProtoBuf(ItemsDB::Producer *pbf) const {
    pbf->set_id(Id);
    pbf->set_name(Name);
    pbf->set_url(URL);
    pbf->set_shortdescription(ShortDescription);
    pbf->set_description(Description);
    pbf->set_image(Image);
    return true;
}
bool Producer::FromProtoBuf(const ItemsDB::Producer &pbf) {
    Id = pbf.id();
    Name = pbf.name();
    URL = pbf.url();
    ShortDescription = pbf.shortdescription();
    Description = pbf.description();
    Image = pbf.image();
    return true;
}

Producer *Producers::NewProducer() {
    Producer* obj = new Producer();
    producers.push_back(obj);
    return obj;
}

bool Producers::DeleteProducer(Producer *producer) {
    producers.erase(std::remove(producers.begin(), producers.end(), producer), producers.end());
    delete producer;
    return true;
}

bool Producers::ToJSON(json_t *json) {
    json_t *json_producers = json_array();
    for(unsigned int i = 0; i < producers.size(); ++i) {
        json_t *json_obj = json_object();
        if (!producers[i]->ToJSON(json_obj)) { std::cout << "Render Producer " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_producers, json_obj);
    }
    if (json_object_set(json, "producers", json_producers) != 0) return log_error("producers");
    return true;
}

bool Producers::FromJSON(json_t *json) {
    json_t *producers_json = json_object_get(json, "producers");
    if (producers_json == NULL) return log_error("producers_json json is NULL");
    if (!json_is_array(producers_json)) return log_error("producers_json is not array");
    for (unsigned int i = 0; i < json_array_size(producers_json); ++i) {
        json_t *json_producer = json_array_get(producers_json, i);
        if (json_producer == NULL) return log_error("json_producer is NULL");

        if (!json_is_object(json_producer)) return log_error("json_producer is not object");
        Producer *producer = NewProducer();
        if (!producer->FromJSON(json_producer)) return false;
    }
    return true;
}


void Producers::clear() {
    for (unsigned int i = 0; i < producers.size(); ++i) delete producers[i];
    producers.clear();
}


bool Producers::ToSQLite(sqlite3 *db) {
    const char *sql = "CREATE TABLE Producers("  \
             "KEY    UNSIGNED BIG INT    PRIMARY KEY     NOT NULL," \
             "VALUE  BLOB                                NOT NULL);";
    char *zErrMsg = 0;
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        ExceptionSaver ss; ss << "Producers::ToSQLite SQL error: " << zErrMsg;
        sqlite3_free(zErrMsg);
    }

    for(unsigned int i = 0; i < producers.size(); ++i) {
        json_t *json = json_object();
        if (!producers[i]->ToJSON(json)) { std::cout << "Producers::ToSQLite Render Producer " << i << " to json error!" << std::endl; return false; }
        char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
        if (str_data == NULL) return log_error("Producers::ToSQLite Render Producer to json error");
        if(!writeBlob(db, "Producers", producers[i]->GetId(), str_data, strlen(str_data))) return false;
        delete[] str_data;
    }
    return true;
}
bool Producers::FromSQLite(sqlite3 *db) {
    std::vector<std::string> strs;
    if(!readBlob(db, "Producers", strs)){
        ExceptionSaver ss; ss << "Producers::FromSQLite SQL error: ";
    };
    for (std::string &str : strs) {
        json_error_t error;
        json_t *json = json_loads(str.c_str(), 0, &error);
        if (json == NULL) {
            std::cout << error.text << std::endl;
            return log_error("Producers::FromSQLite json is NULL");
        }
        Producer *producer = NewProducer();
        if (!producer->FromJSON(json)) return false;
    }
    return true;
}

bool Producers::ToProtoBuf(ItemsDB::Db *db) const {
    for(unsigned int i = 0; i < producers.size(); ++i) {
        ItemsDB::Producer* producer_pbf = db->add_producers();
        if (!producers[i]->ToProtoBuf(producer_pbf)) return log_error("Producers::ToProtoBuf producer error");
    }
    return true;
}

bool Producers::FromProtoBuf(const ItemsDB::Db &db) {
    for(unsigned int i = 0; i < db.producers_size(); ++i) {
        Producer *producer = new Producer();
        if(!producer->FromProtoBuf(db.producers(i))) return log_error("Producers::FromProtoBuf producer error");
        producers.push_back(producer);
    }
    return true;
}
