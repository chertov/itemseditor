#pragma once
#include <iostream>

#include <QVector>
#include <QString>
#include <QObject>
#include <QMap>
#include <vector>
#include <deque>
#include <memory>
#include <jansson.h>
#include "db.pb.h"

#include "tools.hpp"
#include "idtype.hpp"

enum ModelType {
    FOLDER,
    CATEGORY,
    ITEM
};

enum ValidateResult {
    OK,
    CHILDREN_IS_NULL,
    CHILDREN_INVALID_POINTER,
    CHILDREN_NO_ID,
    ITEM_UNKNOWN_PROPERTY,
    ITEM_UNKNOWN_ENUM_VALUE,
    ITEM_DONT_HAVE_PROPERTY,
    ITEM_DUPLICATE_ID,
    ITEM_DUPLICATE_ARTICLE,
    ITEM_DUPLICATE_CODE,
    ITEM_EMPTY_NAME
};

class PtrInfo;
class BaseProps;
class BaseFolder;
class FoldersCategories;
class Folder;
class Category;
class Item;

class BaseInterface {
protected:
    BaseInterface(ModelType type) { this->type = type; }
    virtual ~BaseInterface() { }

public:
    virtual void Clear();
    virtual std::vector<PtrInfo*> Childrens() const;
    virtual size_t ChildrenSize() const;
    virtual PtrInfo* Children(unsigned int index) const;
    ModelType Type() const { return type; }

private:
    ModelType type;
};

class PtrInfo : public BaseInterface {
public:
    virtual PtrInfo* GetParent() const;
    virtual void SetParent(PtrInfo* parent);

    BaseProps* ToBaseProps() const;
    BaseFolder* ToBaseFolder() const;
    Folder* ToFolder() const;
    Category* ToCategory() const;
    Item* ToItem() const;

protected:
    PtrInfo(ModelType type);
    virtual ~PtrInfo() { }
};

class BaseProps : public PtrInfo{
protected:
    BaseProps(ModelType type) : PtrInfo(type) { }
    virtual ~BaseProps() {}

public:
    IdType GetId() const;
    bool SetId(IdType value);

    std::string GetName() const;
    bool SetName(std::string value);

    std::string GetShortDescription() const;
    bool SetShortDescription(std::string value);

    std::string GetDescription() const;
    bool SetDescription(std::string value);

    bool GetShowOnSite() const;
    bool SetShowOnSite(bool value);

private:
    IdType Id = uuid();
    std::string Name;
    std::string ShortDescription;
    std::string Description;
    bool ShowOnSite;

protected:
    virtual bool ToJSON(json_t *json);
    virtual bool FromJSON(json_t *json);
    virtual bool ToProtoBuf(ItemsDB::BaseProps *pbf) const;
    virtual bool FromProtoBuf(const ItemsDB::BaseProps &pbf);
};

class BaseFolder : public BaseProps{
public:
    IdType GetImage() const;
    bool SetImage(const IdType &value);

    bool GetEditorExpanded() const;
    bool SetEditorExpanded(bool value);

    virtual void SetShowOnSiteForChildrens(bool value);

private:
    IdType Image = 0;
    bool EditorExpanded;

protected:
    BaseFolder(ModelType type);
    virtual ~BaseFolder() {}

    virtual bool ToJSON(json_t *json);
    virtual bool FromJSON(json_t *json);
    virtual bool ToProtoBuf(ItemsDB::BaseFolder *pbf) const;
    virtual bool FromProtoBuf(const ItemsDB::BaseFolder &pbf);
};
