#include "propertyset.hpp"

#include "db.hpp"
#include "parser.hpp"
#include "exception.hpp"

IdVector PropertySet::GetProperties() const {
    return properties;
}
unsigned int PropertySet::GetPropertiesSize() const {
    return properties.size();
}
IdType PropertySet::GetPropertyByIndex(unsigned int index) const {
    return properties[index];
}
bool PropertySet::SetProperties(const IdVector &value) {
    properties = value; return true;
}

PropertyList PropertySet::GetPropertiesPtr() const {
    PropertyList list;
    for(const IdType &property_id : properties)
        list.push_back(DB.PropertyById(property_id));
    return list;
}
Property* PropertySet::GetPropertyPtrByIndex(unsigned int index) const {
    if(index >= properties.size()) {
        ExceptionSaver ss; ss << "PropertySet::GetPropertyPtrByIndex: index out of range. properties size = " << properties.size() <<
            " in propertySet with id " << id;

    }
    return DB.PropertyById(properties[index]);
}

bool PropertySet::PropertyMove(unsigned int source_index, unsigned int destination_index) {
    return move_vector_element<IdType>(source_index, destination_index, properties);
}

Property* PropertySet::GetProperty(const IdType &Id) const {
    for(const IdType &prop_id: properties)
        if (prop_id == Id) {
            return DB.PropertyById(prop_id);
        }
    {
        ExceptionSaver ss; ss << "PropertySet::GetProperty: Can't find property with id = " << Id <<
            " in propertySet with id " << id;

    }
    return 0;
}

bool PropertySet::ToJSON(json_t *json) {
    json_t *json_properties = json_array();
    for(const IdType &property_id : properties) {
        json_t *json_obj = IdToJson(property_id);
        json_array_append(json_properties, json_obj);
    }
    if (json_object_set(json, "properties", json_properties) != 0) return log_error("json_properties");
    return true;
}

bool PropertySet::FromJSON(json_t *json) {
    properties.clear();
    json_t *properties_json = json_object_get(json, "properties");
    if (properties_json == NULL) { log_error("properties json is NULL"); return true; }
    if (!json_is_array(properties_json)) return log_error("properties is not array");
    for (unsigned int i = 0; i < json_array_size(properties_json); ++i) {
        json_t *json_property = json_array_get(properties_json, i);
        IdType property_id;
        if (!IdFromJson(json_property, property_id)) return false;
        properties.push_back(property_id);
    }
    return true;
}

bool PropertySet::ToProtoBuf(ItemsDB::PropertySet *pbf) const {
    for(const IdType &property : properties) pbf->add_properties(property);
    return true;
}
bool PropertySet::FromProtoBuf(const ItemsDB::PropertySet &pbf){
    properties.clear();
    for (unsigned int i = 0; i < pbf.properties_size(); ++i) {
        IdType id = pbf.properties(i);
        properties.push_back(id);
    }
    return true;
}
