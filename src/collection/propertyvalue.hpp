#pragma once
#include <iostream>

#include <QObject>
#include <QMap>
#include <vector>
#include <set>
#include <unordered_map>
#include <memory>
#include <jansson.h>
#include "db.pb.h"

#include "idtype.hpp"

class PropertyValue {
    friend class Item;
public:
    IdType GetPropertyId() const;
    bool SetPropertyId(const IdType &property_id);
    IdType PropertyId;

    IdType Enum = 0;
    int Int;
    double Real;
    bool Bool;
private:
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToProtoBuf(ItemsDB::PropertyValue *pbf) const;
    bool FromProtoBuf(const ItemsDB::PropertyValue &pbf);
};

Q_DECLARE_METATYPE(PropertyValue)

typedef std::vector<PropertyValue> PropertyValueList;

//class PropertyValues {
//public:
//    PropertyValue *NewPropertyValue();
//    bool DeletePropertyValue(PropertyValue *propertyValue);
//    unsigned int PropertyValuesSize() const;
//    PropertyValue* PropertyValueByIndex(unsigned int index) const;
//    PropertyValue* PropertyValueById(const IdType &id) const;

//protected:
//    bool ToJSON(json_t *json);
//    bool FromJSON(json_t *json);
//    void clear();

//private:
//    std::unordered_map<IdType, PropertyValue*> propertyValuesMap;
//    std::vector<PropertyValue*> propertyValues;
//};
