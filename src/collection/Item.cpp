#include "item.hpp"

#include "assert.hpp"

#include "parser.hpp"

#include "db.hpp"
#include "category.hpp"
#include "exception.hpp"

#include "sqlite.hpp"

Item::Item() : BaseProps(ITEM) {
    Volume = -1;
    price.Type = PRICE_RUB;
    price.Value = -1;
}

std::string Item::GetCode() const { return Code; }
bool Item::SetCode(std::string value) { Code = value; return true; }

std::string Item::GetArticle() const { return Article; }
bool Item::SetArticle(std::string value) { Article = value; return true; }

IdType Item::GetItemType() const { return ItemType; }
bool Item::SetItemType(const IdType &value) { ItemType = value; return true; }

IdType Item::GetProducer() const { return Producer; }
bool Item::SetProducer(const IdType &value) { Producer = value; return true; }

IdVector Item::GetImages() const { return Images; }
bool Item::SetImages(const IdVector &value) { Images = value; return true; }

PropertyValueList Item::GetProperties() const { return Properties; }
bool Item::SetProperties(const PropertyValueList &value) { Properties = value; return true; }

bool Item::GetPropertyValue(const IdType &property_id, PropertyValue &property_value) const {
    for (PropertyValue property : Properties)
        if (property.PropertyId == property_id) {
            property_value = property;
            return true;
        }
    return false;
}
bool Item::SetPropertyValue(const IdType &property_id, const PropertyValue &property_value) {
    for (PropertyValue &property : Properties)
        if (property.PropertyId == property_id) {
            property = property_value;
            return true;
        }
    Properties.push_back(property_value);
    return true;
}

PtrInfo* Item::GetParent() const {
    return DB.GetItemParent(GetId());
}
void Item::SetParent(PtrInfo* parent) {
    ExceptionSaver ss;
    ss << "Call Item::SetParent. This is a unreachable code.";
}

size_t Item::ChildrenSize() const { return 0; }

bool Item::ToJSON(json_t *json) {
    if(!BaseProps::ToJSON(json)) return false;

    if (!stringToJson(json, "code", Code)) return false;
    if (!stringToJson(json, "article", Article)) return false;
    if (!IdToJson(json, "type", ItemType)) return false;
    if (!IdToJson(json, "producer", Producer)) return false;

    json_t *json_properties = json_array();
    for(unsigned int i = 0; i < Properties.size(); ++i) {
        json_t *json_obj = json_object();
        if (!Properties[i].ToJSON(json_obj)) { std::cout << "Render PropertyValue " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_properties, json_obj);
    }
    if (json_object_set(json, "properties", json_properties) != 0) return log_error("properties");

    json_t *json_images = json_array();
    for(unsigned int i = 0; i < Images.size(); ++i) {
        //json_t *json_obj = json_stringn_nocheck(Images[i].data(), Images[i].size());
        json_t *json_obj = IdToJson(Images[i]);
        json_array_append(json_images, json_obj);
    }
    if (json_object_set(json, "images", json_images) != 0) return log_error("images");

    return true;
}

bool Item::FromJSON(json_t *json) {
    if(!BaseProps::FromJSON(json)) return false;

    if (!json_parse_string(json, "code", Code)) return log_error_parse("code");
    if (!json_parse_string(json, "article", Article)) return log_error_parse("article");
    if (!IdFromJson(json, "type", ItemType)) return log_error_parse("type");
    if (!IdFromJson(json, "producer", Producer)) return log_error_parse("producer");

    json_t *json_properties = json_object_get(json, "properties");
    if (json_properties == NULL) return log_error("json_properties is NULL");
    if (!json_is_array(json_properties)) return log_error("json_properties is not array");
    for (unsigned int i = 0; i < json_array_size(json_properties); ++i) {
        json_t *json_property = json_array_get(json_properties, i);
        if (json_property == NULL) return log_error("json_property is NULL");

        if (!json_is_object(json_property)) return log_error("json_property is not object");
        PropertyValue propertyValue;
        if (!propertyValue.FromJSON(json_property)) return false;
        Properties.push_back(propertyValue);
    }

    json_t *json_images = json_object_get(json, "images");
    if (json_images == NULL) return log_error("json_images is NULL");
    if (!json_is_array(json_images)) return log_error("json_images is not array");
    for (unsigned int i = 0; i < json_array_size(json_images); ++i) {
        json_t *json_image = json_array_get(json_images, i);
        if (json_image == NULL) return log_error("json_image is NULL");

        //if (!json_is_string(json_image)) return log_error("json_image not string");
        //std::string image = json_string_value(json_image);
        IdType image;
        if(!IdFromJson(json_image, image)) return log_error("json_image not id");
        Images.push_back(image);
    }

    return true;
}

bool Item::ToProtoBuf(ItemsDB::Item *pbf) const {
    if(!BaseProps::ToProtoBuf(pbf->mutable_baseprops())) log_error("Item::ToProtoBuf BaseProps error");

    pbf->set_code(Code);
    pbf->set_article(Article);
    pbf->set_itemtype(ItemType);
    pbf->set_producer(Producer);

    pbf->set_volume(Volume);
    switch (price.Type) {
    case PRICE_RUB:
        pbf->mutable_price()->set_type(ItemsDB::PRICE_RUB);
        break;
    case PRICE_EUR:
        pbf->mutable_price()->set_type(ItemsDB::PRICE_EUR);
        break;
    case PRICE_USD:
        pbf->mutable_price()->set_type(ItemsDB::PRICE_USD);
        break;
    default:
        ExceptionSaver ss;
        ss << "Unknown PriceCurrency";
        break;
    }
    pbf->mutable_price()->set_value(price.Value);

    for(unsigned int i = 0; i < Properties.size(); ++i) {
        ItemsDB::PropertyValue* propertyValue_pbf = pbf->add_properties();
        if (!Properties[i].ToProtoBuf(propertyValue_pbf)) return log_error("Item::ToProtoBuf propertyValue error");
    }
    for(const IdType &image : Images) pbf->add_images(image);
    return true;
}
bool Item::FromProtoBuf(const ItemsDB::Item &pbf) {
    if(!BaseProps::FromProtoBuf(pbf.baseprops())) log_error("Item::FromProtoBuf BaseFolder error");

    Code = pbf.code();
    Article = pbf.article();
    ItemType = pbf.itemtype();
    Producer = pbf.producer();

    Volume = pbf.volume();
    switch (pbf.price().type()) {
    case ItemsDB::PRICE_RUB:
        price.Type = PRICE_RUB;
        break;
    case ItemsDB::PRICE_EUR:
        price.Type = PRICE_EUR;
        break;
    case ItemsDB::PRICE_USD:
        price.Type = PRICE_USD;
        break;
    default:
        ExceptionSaver ss;
        ss << "Unknown PriceCurrency";
        break;
    }
    price.Value = pbf.price().value();

    Properties.clear();
    for (unsigned int i = 0; i < pbf.properties_size(); ++i) {
        PropertyValue propertyValue;
        if(!propertyValue.FromProtoBuf(pbf.properties(i))) return log_error("Item::FromProtoBuf propertyValue error");
        Properties.push_back(propertyValue);
    }
    Images.clear();
    for (unsigned int i = 0; i < pbf.images_size(); ++i) {
        IdType id = pbf.images(i);
        Images.push_back(id);
    }
    return true;
}


Item *Items::NewItem(Category *parent) {
    if(!DB.checkPtr(parent)) {
        ExceptionSaver ss;
        ss << "Items::NewItem. Can't checkPtr parent";
    }
    Item* obj = new Item();
    ItemsMap[obj->GetId()].parents.insert(parent);
    items.push_back(obj);
    ItemsMap[obj->GetId()].item = obj;
    return obj;
}

bool Items::DeleteItem(Item *item) {
//    for(auto parent : ItemsMap[item->GetId()]) {
//        if (parent == 0) return error("DeletePtr Item.parent == 0");
//        parent->ChildrenDrop(item);
//    }
//    ItemsMap.erase(item->GetId());
    ItemsMap.erase(item->GetId());
    items.erase(std::remove(items.begin(), items.end(), item), items.end());
    delete item;
    item = 0;
    return true;
}

void Items::clear() {
    for (unsigned int i = 0; i < items.size(); ++i) delete items[i];
    items.clear();
    ItemsMap.clear();
}

unsigned int Items::ItemsSize() const { return items.size(); }
Item* Items::ItemByIndex(unsigned int index) const {
    if (index >= items.size()) return 0;
    return items[index];
}
Item* Items::ItemById(const IdType &id) const {
    auto it = ItemsMap.find(id);
    if (it == ItemsMap.end()) {
        ExceptionSaver ss; ss << "Items::ItemById: Can't find item with id = " << id;
    }
    Item *item = it->second.item;
    if(item == 0 ) {
        ExceptionSaver ss;
        ss << "Items::ItemById: item == 0";
    }
    return item;
}


bool Items::ItemMove(unsigned int source_index, unsigned int destination_index) {
    return move_vector_element<Item*>(source_index, destination_index, items);
}


PtrInfo* Items::GetItemParent(const IdType &item_id) const {
    auto it = ItemsMap.find(item_id);
    if (it == ItemsMap.end()) {
        ExceptionSaver ss;
        ss << "Can't find parent of item with id = " << item_id;
    }
    if(it->second.parents.size() != 1) {
        ExceptionSaver ss;
        ss << "Item with id = " << item_id << " have incorrent parent size = " << it->second.parents.size();
    }
    return *(it->second.parents.begin());
}
std::set<Category*> Items::GetItemParents(const IdType &item_id) const {
    auto it = ItemsMap.find(item_id);
    if (it == ItemsMap.end()) {
        ExceptionSaver ss;
        ss << "Can't find parent of item with id = " << item_id;
    }
    if(it->second.parents.size() != 1) {
        ExceptionSaver ss;
        ss << "Item with id = " << item_id << " have incorrent parent size = " << it->second.parents.size();
    }
    return it->second.parents;
}
bool Items::SetItemParent(const IdType &item_id, Category *parent) {
    ItemsMap[item_id].parents.clear();
    ItemsMap[item_id].parents.insert(parent);
}

bool Items::ToJSON(json_t *json) {
    json_t *json_items = json_array();
    for(unsigned int i = 0; i < items.size(); ++i) {
        json_t *json_obj = json_object();
        if (!items[i]->ToJSON(json_obj)) { std::cout << "Render Item " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_items, json_obj);
    }
    if (json_object_set(json, "items", json_items) != 0) return log_error("items");
    return true;
}

bool Items::FromJSON(json_t *json) {
    json_t *items_json = json_object_get(json, "items");
    if (items_json == NULL) return log_error("items_json json is NULL");
    if (!json_is_array(items_json)) return log_error("items_json is not array");
    for (unsigned int i = 0; i < json_array_size(items_json); ++i) {
        json_t *json_item = json_array_get(items_json, i);
        if (json_item == NULL) return log_error("json_item is NULL");

        if (!json_is_object(json_item)) return log_error("json_item is not object");
        Item *item = new Item();
        if (!item->FromJSON(json_item)) return false;
        items.push_back(item);
        ItemsMap[item->GetId()].item = item;
    }
    return true;
}

bool Items::ToSQLite(sqlite3 *db) {
    const char *sql = "CREATE TABLE ITEMS("  \
             "KEY    UNSIGNED BIG INT    PRIMARY KEY     NOT NULL," \
             "VALUE  BLOB                                NOT NULL);";
    char *zErrMsg = 0;
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        ExceptionSaver ss; ss << "Items::ToSQLite SQL error: " << zErrMsg;
        sqlite3_free(zErrMsg);
    }

    for(unsigned int i = 0; i < items.size(); ++i) {
        json_t *json = json_object();
        if (!items[i]->ToJSON(json)) { std::cout << "Render Item " << i << " to json error!" << std::endl; return false; }
        char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
        if (str_data == NULL) return log_error("Render Item to json error");
        if(!writeBlob(db, "Items", items[i]->GetId(), str_data, strlen(str_data))) return false;
        delete[] str_data;
    }
    return true;
}

bool Items::FromSQLite(sqlite3 *db) {
    std::vector<std::string> strs;
    if(!readBlob(db, "Items", strs)){
        ExceptionSaver ss; ss << "Items::ToSQLite SQL error: ";
    };
    for (std::string &str : strs) {
        json_error_t error;
        json_t *json = json_loads(str.c_str(), 0, &error);
        if (json == NULL) {
            std::cout << error.text << std::endl;
            return log_error("Items::FromSQLite json is NULL");
        }
        Item *item = new Item();
        if (!item->FromJSON(json)) return false;
        items.push_back(item);
        ItemsMap[item->GetId()].item = item;
    }
    return true;
}

bool Items::ToProtoBuf(ItemsDB::Db *db) const {
    for(unsigned int i = 0; i < items.size(); ++i) {
        ItemsDB::Item* item_pbf = db->add_items();
        if (!items[i]->ToProtoBuf(item_pbf)) return log_error("Items::ToProtoBuf item error");
    }
    return true;
}

bool Items::FromProtoBuf(const ItemsDB::Db &db) {
    for(unsigned int i = 0; i < db.items_size(); ++i) {
        Item *item = new Item();
        if(!item->FromProtoBuf(db.items(i))) return log_error("Items::FromProtoBuf item error");
        items.push_back(item);
        ItemsMap[item->GetId()].item = item;
    }
    return true;
}
