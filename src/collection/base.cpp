#include "base.hpp"

#include "parser.hpp"

#include "db.hpp"
#include "category.hpp"
#include "folder.hpp"
#include "item.hpp"
#include "exception.hpp"

PtrInfo::PtrInfo(ModelType type) : BaseInterface(type) {
}
PtrInfo* PtrInfo::GetParent() const { assert(UNREACHABLE); return 0; }
void PtrInfo::SetParent(PtrInfo* parent) { assert(UNREACHABLE); }

BaseProps* PtrInfo::ToBaseProps() const {
    return static_cast<BaseProps*>(const_cast<PtrInfo*>(this));
}
BaseFolder* PtrInfo::ToBaseFolder() const {
    if(Type() != FOLDER && Type() != CATEGORY) {
        ExceptionSaver ss;
        ss << "Can't cast to BaseFolder* not FOLDER or CATEGROY type";

    }
    return static_cast<BaseFolder*>(const_cast<PtrInfo*>(this));
}
Folder* PtrInfo::ToFolder() const {
    if(Type() != FOLDER) {
        ExceptionSaver ss;
        ss << "Can't cast to Folder* not FOLDER type";
    }
    return static_cast<Folder*>(const_cast<PtrInfo*>(this));
}
Category* PtrInfo::ToCategory() const {
    if(Type() != CATEGORY) {
        ExceptionSaver ss;
        ss << "Can't cast to Category* not CATEGORY type";
    }
    return static_cast<Category*>(const_cast<PtrInfo*>(this));
}
Item* PtrInfo::ToItem() const {
    if(Type() != ITEM) {
        ExceptionSaver ss;
        ss << "Can't cast to Item* not ITEM type";
    }
    return static_cast<Item*>(const_cast<PtrInfo*>(this));
}

size_t BaseInterface::ChildrenSize() const { assert(UNREACHABLE); return 0; }
PtrInfo* BaseInterface::Children(unsigned int index) const { assert(UNREACHABLE); return 0; }
std::vector<PtrInfo*> BaseInterface::Childrens() const { assert(UNREACHABLE); }
void BaseInterface::Clear() { assert(UNREACHABLE); }

IdType BaseProps::GetId() const { return Id; }
bool BaseProps::SetId(IdType value) { Id = value; return true; }

std::string BaseProps::GetName() const { return Name; }
bool BaseProps::SetName(std::string value) { Name = value; return true; }

std::string BaseProps::GetShortDescription() const { return ShortDescription; }
bool BaseProps::SetShortDescription(std::string value) { ShortDescription = value; return true; }

std::string BaseProps::GetDescription() const { return Description; }
bool BaseProps::SetDescription(std::string value) { Description = value; return true; }

bool BaseProps::GetShowOnSite() const { return ShowOnSite; }
bool BaseProps::SetShowOnSite(bool value) { ShowOnSite = value; return true; }


bool BaseProps::ToJSON(json_t *json) {
    if (!IdToJson(json, "id", Id)) return false;
    if (!stringToJson(json, "name", Name)) return false;
    if (!stringToJson(json, "short_description", ShortDescription)) return false;
    if (!stringToJson(json, "description", Description)) return false;
    if (json_object_set(json, "show_on_site", json_boolean(ShowOnSite)) != 0) return log_error("show_on_site");
    return true;
}

bool BaseProps::FromJSON(json_t *json) {
    if (!IdFromJson(json, "id", Id)) return log_error_parse("id");
    if (!json_parse_string(json, "name", Name)) return log_error_parse("name");
    if (!json_parse_string(json, "short_description", ShortDescription)) return log_error_parse("short_description");
    if (!json_parse_string(json, "description", Description)) return log_error_parse("description");
    json_parse_bool(json, "show_on_site", ShowOnSite); //return log_error_parse("show_on_site");
    return true;
}

bool BaseProps::ToProtoBuf(ItemsDB::BaseProps *pbf) const {
    pbf->set_id(Id);
    pbf->set_name(Name);
    pbf->set_shortdescription(ShortDescription);
    pbf->set_description(Description);
    pbf->set_showonsite(ShowOnSite);
    return true;
}

bool BaseProps::FromProtoBuf(const ItemsDB::BaseProps &pbf) {
    Id = pbf.id();
    Name = pbf.name();
    ShortDescription = pbf.shortdescription();
    Description = pbf.description();
    ShowOnSite = pbf.showonsite();
    return true;
}

BaseFolder::BaseFolder(ModelType type) : BaseProps(type) { }

IdType BaseFolder::GetImage() const { return Image; }
bool BaseFolder::SetImage(const IdType &value) { Image = value; return true; }

bool BaseFolder::GetEditorExpanded() const { return EditorExpanded; }
bool BaseFolder::SetEditorExpanded(bool value) { EditorExpanded = value; return true; }

void BaseFolder::SetShowOnSiteForChildrens(bool value) {
    ExceptionSaver ss;
    ss << "BaseFolder SetShowOnSiteForChildrens is virtual method";
}

bool BaseFolder::ToJSON(json_t *json) {
    if(!BaseProps::ToJSON(json)) return false;

    if (!IdToJson(json, "image", Image)) return false;
    json_object_set(json, "editor_expanded", json_boolean(EditorExpanded));
    return true;
}

bool BaseFolder::FromJSON(json_t *json) {
    if(!BaseProps::FromJSON(json)) return false;

    if (!IdFromJson(json, "image", Image)) return log_error_parse("image");
    json_parse_bool(json, "editor_expanded", EditorExpanded);
    return true;
}

bool BaseFolder::ToProtoBuf(ItemsDB::BaseFolder *pbf) const {
    if(!BaseProps::ToProtoBuf(pbf->mutable_baseprops())) return false;
    pbf->set_image(Image);
    return true;
}

bool BaseFolder::FromProtoBuf(const ItemsDB::BaseFolder &pbf) {
    if(!BaseProps::FromProtoBuf(pbf.baseprops())) return false;
    Image = pbf.image();
    return true;
}
