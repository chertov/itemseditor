#include "property.hpp"
#include "parser.hpp"
#include "db.hpp"
#include "exception.hpp"
#include "sqlite.hpp"

IdType Property::GetId() const { return Id; }
bool Property::SetId(IdType value) { Id = value; return true; }

PropertyType Property::GetType() const { return Type; }
bool Property::SetType(PropertyType value) {
    if(value == PROPERTY_ENUM && enum_id == 0) {
        Enum *enum_ptr = DB.NewEnum();
        enum_id = enum_ptr->GetId();
    }
    Type = value;
    return true;
}

std::string Property::GetName() const { return Name; }
bool Property::SetName(std::string value) { Name = value; return true; }

std::string Property::GetDescription() const { return Description; }
bool Property::SetDescription(std::string value) { Description = value; return true; }

bool Property::GetShowOnSite() const { return ShowOnSite; }
bool Property::SetShowOnSite(bool value) { ShowOnSite = value; return true; }

bool Property::GetShowInSearch() const { return ShowInSearch; }
bool Property::SetShowInSearch(bool value) { ShowInSearch = value; return true; }

IdType Property::GetEnum() const {
    return enum_id;
}
bool Property::SetEnum(const IdType& enum_id) { this->enum_id = enum_id; return true; }

Enum *Property::GetEnumPtr() const {
    if (enum_id == 0) return 0;
    Enum* enum_ptr = DB.EnumById(enum_id);
    if(enum_ptr == 0) {
        ExceptionSaver ss; ss << "Property::GetEnumPtr: enum_ptr == 0";
    }
    return enum_ptr;
}

bool Property::operator==(const Property& other) { return Id == other.Id; }

bool Property::ToJSON(json_t *json) {
    if (!IdToJson(json, "id", Id)) return false;
    if (!stringToJson(json, "name", Name)) return false;
    if (!stringToJson(json, "description", Description)) return false;
    if (json_object_set(json, "show_in_search", json_boolean(ShowInSearch)) != 0) return log_error("show_search");
    if (json_object_set(json, "show_on_site", json_boolean(ShowOnSite)) != 0) return log_error("show_on_site");
    if (!stringToJson(json, "type", typeToStr(Type))) return false;

    if (Type == PROPERTY_ENUM)
        if (!IdToJson(json, "enum", enum_id)) return false;
    return true;
}
bool Property::FromJSON(json_t *json) {
    if (!IdFromJson(json, "id", Id)) return log_error_parse("type");
    if (!json_parse_string(json, "name", Name)) return log_error_parse("name");
    if (!json_parse_string(json, "description", Description)) return log_error_parse("description");
    if (!json_parse_bool(json, "show_in_search", ShowInSearch)) return log_error_parse("show_in_search");
    if (!json_parse_bool(json, "show_on_site", ShowOnSite)) return log_error_parse("show_on_site");

    std::string TypeStr;
    if (!json_parse_string(json, "type", TypeStr)) return log_error_parse("type");
    Type = strToType(TypeStr);
    if(Type == PROPERTY_ENUM)
        if (!IdFromJson(json, "enum", enum_id)) return log_error_parse("enum_id");
    return true;
}

bool Property::ToProtoBuf(ItemsDB::Property *pbf) const {
    pbf->set_id(Id);
    pbf->set_name(Name);
    pbf->set_description(Description);
    pbf->set_showinsearch(ShowInSearch);
    pbf->set_showonsite(ShowOnSite);
    pbf->set_type(static_cast<ItemsDB::PropertyType>(Type));
    if(Type == PROPERTY_ENUM)
        pbf->set_enumid(enum_id);
    else
        pbf->set_enumid(0);
    return true;
}
bool Property::FromProtoBuf(const ItemsDB::Property &pbf) {
    Id = pbf.id();
    Name = pbf.name();
    Description = pbf.description();
    ShowInSearch = pbf.showinsearch();
    ShowOnSite = pbf.showonsite();
    Type = static_cast<PropertyType>(pbf.type());
    if(Type == PROPERTY_ENUM)
        enum_id = pbf.enumid();
    else
        enum_id = 0;
    return true;
}

bool Property::GetEnumValue(const IdType &Id, std::string &value) const {
   if (Type != PROPERTY_ENUM) return false;
   Enum *enum_ptr = GetEnumPtr();
   value = enum_ptr->GetValue(Id);
   return true;
}

bool Properties::ToJSON(json_t *json) {
    json_t *json_properties = json_array();
    for(unsigned int i = 0; i < properties.size(); ++i) {
        json_t *json_obj = json_object();
        if (!properties[i]->ToJSON(json_obj)) { std::cout << "Render Property " << i << " to json error!" << std::endl; return false; }
        json_array_append(json_properties, json_obj);
    }
    if (json_object_set(json, "properties", json_properties) != 0) return log_error("properties");
    return true;
}

bool Properties::FromJSON(json_t *json) {
    json_t *json_properties = json_object_get(json, "properties");
    if (json_properties == NULL) return true; // return log_error("json_properties is NULL");
    if (!json_is_array(json_properties)) return log_error("json_properties is not array");
    for (unsigned int i = 0; i < json_array_size(json_properties); ++i) {
        json_t *json_property = json_array_get(json_properties, i);
        if (json_property == NULL) return log_error("json_property is NULL");

        if (!json_is_object(json_property)) return log_error("json_property is not object");
        Property *property = new Property();
        if (!property->FromJSON(json_property)) return false;
        properties.push_back(property);
        PropertiesMap[property->GetId()] = property;
    }
    return true;
}

Property* Properties::NewProperty() {
    Property* property = new Property();
    property->SetName("Новое ствойство");
    property->SetType(PROPERTY_BOOL);
    properties.push_back(property);
    PropertiesMap[property->GetId()] = property;
    return property;
}

bool Properties::DeleteProperty(Property* property) {
    PropertiesMap.erase(property->GetId());
    properties.erase(std::remove(properties.begin(), properties.end(), property), properties.end());
    delete property;
    property = 0;
    return true;
}

void Properties::clear() {
    for (unsigned int i = 0; i < properties.size(); ++i) delete properties[i];
    properties.clear();
    PropertiesMap.clear();
}

unsigned int Properties::PropertiesSize() const { return properties.size(); }
Property* Properties::PropertyByIndex(unsigned int index) const {
    if (index >= properties.size()) {
        ExceptionSaver ss; ss << "Properties::PropertyByIndex: index out of range. Max range = " << properties.size();

    }
    return properties[index];
}
Property* Properties::PropertyById(const IdType &id) const {
    auto it = PropertiesMap.find(id);
    if (it == PropertiesMap.end()) {
        ExceptionSaver ss; ss << "Properties::PropertyById: Can't find property with id = " << id;

    }
    return it->second;
}

bool Properties::ToSQLite(sqlite3 *db) {
    const char *sql = "CREATE TABLE Properties("  \
             "KEY    UNSIGNED BIG INT    PRIMARY KEY     NOT NULL," \
             "VALUE  BLOB                                NOT NULL);";
    char *zErrMsg = 0;
    int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
        ExceptionSaver ss; ss << "Properties::ToSQLite SQL error: " << zErrMsg;
        sqlite3_free(zErrMsg);
    }

    for(unsigned int i = 0; i < properties.size(); ++i) {
        json_t *json = json_object();
        if (!properties[i]->ToJSON(json)) { std::cout << "Properties::ToSQLite Render Property " << i << " to json error!" << std::endl; return false; }
        char *str_data = json_dumps(json, JSON_ENCODE_ANY | JSON_INDENT | JSON_COMPACT | JSON_SORT_KEYS);
        if (str_data == NULL) return log_error("Properties::ToSQLite Render Property to json error");
        if(!writeBlob(db, "Properties", properties[i]->GetId(), str_data, strlen(str_data))) return false;
        delete[] str_data;
    }
    return true;
}
bool Properties::FromSQLite(sqlite3 *db) {
    std::vector<std::string> strs;
    if(!readBlob(db, "Properties", strs)){
        ExceptionSaver ss; ss << "Properties::FromSQLite SQL error: ";
    };
    for (std::string &str : strs) {
        json_error_t error;
        json_t *json = json_loads(str.c_str(), 0, &error);
        if (json == NULL) {
            std::cout << error.text << std::endl;
            return log_error("Properties::FromSQLite json is NULL");
        }
        Property *property = new Property();
        if (!property->FromJSON(json)) return false;
        properties.push_back(property);
        PropertiesMap[property->GetId()] = property;
    }
    return true;
}

bool Properties::ToProtoBuf(ItemsDB::Db *db) const {
    for(unsigned int i = 0; i < properties.size(); ++i) {
        ItemsDB::Property* property_pbf = db->add_properties();
        if (!properties[i]->ToProtoBuf(property_pbf)) return log_error("Properties::ToProtoBuf property error");
    }
    return true;
}
bool Properties::FromProtoBuf(const ItemsDB::Db &db) {
    for(unsigned int i = 0; i < db.properties_size(); ++i) {
        Property *property = new Property();
        if(!property->FromProtoBuf(db.properties(i))) return log_error("Properties::FromProtoBuf itemType error");
        properties.push_back(property);
        PropertiesMap[property->GetId()] = property;
    }
    return true;
}

