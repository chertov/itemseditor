#pragma once
#include <iostream>

#include <QVector>
#include <QString>
#include <QObject>
#include <QMap>
#include <vector>
#include <deque>
#include <set>
#include <memory>
#include <jansson.h>
#include "sqlite/sqlite3.h"
#include "db.pb.h"

#include "base.hpp"

class FoldersCategories {
    friend class BaseInterface;
    friend class Db;

public:
    virtual void Clear();
    virtual std::vector<PtrInfo*> Childrens() const;
    virtual size_t ChildrenSize() const;
    virtual PtrInfo* Children(unsigned int index) const;

protected:
    virtual ~FoldersCategories() {}

    virtual bool ToJSON(json_t *json);
    virtual bool FromJSON(json_t *json, Folder *self);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db, Folder *self);
    virtual bool ToProtoBuf(ItemsDB::FoldersCategories *pbf) const;
    virtual bool FromProtoBuf(const ItemsDB::FoldersCategories &pbf, Folder *self);

    virtual void SetShowOnSiteForChildrens(bool value);

private:
    void ChildrensRemove();
    void ChildrenDrop(Folder *folder);
    void ChildrenDrop(Category *category);
    void ChildrenAppend(Folder *folder, int row = -1);
    void ChildrenAppend(Category *category, int row = -1);

    std::vector<IdType> folders;
    std::vector<IdType> categories;
};

class Folder : public BaseFolder, public FoldersCategories {
    friend class FoldersCategories;
    friend class Db;
    friend class Folders;
public:
    virtual PtrInfo* GetParent() const;
    virtual void SetParent(PtrInfo* parent);

    virtual void Clear();
    virtual std::vector<PtrInfo*> Childrens() const;
    virtual size_t ChildrenSize() const;
    virtual PtrInfo* Children(unsigned int index) const;

    virtual void SetShowOnSiteForChildrens(bool value);
private:
    Folder() : BaseFolder(FOLDER) { }
    virtual ~Folder() {}

    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);

    bool ToProtoBuf(ItemsDB::Folder *pbf) const;
    bool FromProtoBuf(const ItemsDB::Folder &pbf);
};

class Folders {
public:
    Folder *NewFolder(Folder *parent);
    void DeleteFolder(Folder *item);
    unsigned int FoldersSize() const;
    Folder* FolderByIndex(unsigned int index) const;
    Folder* FolderById(const IdType &id) const;
    bool FolderMove(unsigned int source_index, unsigned int destination_index);

    PtrInfo* GetFolderParent(const IdType &folder_id) const;
    bool SetFolderParent(const IdType &folder_id, Folder *parent);

protected:
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db);
    bool ToProtoBuf(ItemsDB::Db *db) const;
    bool FromProtoBuf(const ItemsDB::Db &db);
    void clear();

    // children - parents
    std::map<IdType, std::set<Folder*>> FoldersMap;

private:
    std::vector<Folder*> folders;

    // folder id - folder ptr
    std::map<IdType, Folder*> folders_ptr;
};
