#pragma once

#include <string>
#include "idtype.hpp"
#include "tools.hpp"
#include "parser.hpp"
#include "sqlite/sqlite3.h"
#include "db.pb.h"

class ItemType {
    friend class ItemsTypes;

public:
    IdType GetId() const;
    bool SetId(const IdType &value);

    std::string GetName() const;
    bool SetName(std::string value);

    std::string GetShortDescription() const;
    bool SetShortDescription(std::string value);

    std::string GetDescription() const;
    bool SetDescription(std::string value);

    IdType GetImage() const;
    bool SetImage(const IdType &value);

private:
    ItemType() {}
    virtual ~ItemType() {}

    virtual bool ToJSON(json_t *json);
    virtual bool FromJSON(json_t *json);
    virtual bool ToProtoBuf(ItemsDB::ItemType *pbf) const;
    virtual bool FromProtoBuf(const ItemsDB::ItemType &pbf);

    IdType Id = uuid();
    std::string Name = "Новый тип товаров";
    std::string ShortDescription;
    std::string Description;
    IdType Image = 0;
};

class ItemsTypes {
public:
    ItemType *NewItemType();
    bool DeleteItemType(ItemType *itemType);
    unsigned int ItemsTypesSize() const { return itemsTypes.size(); }
    ItemType* ItemTypeByIndex(unsigned int index) const {
        if (index >= itemsTypes.size()) return 0;
        return itemsTypes[index];
    }
    ItemType* ItemTypeById(IdType id) const {
        for (unsigned int i = 0; i < itemsTypes.size(); ++i)
            if (id == itemsTypes[i]->Id) return itemsTypes[i];
        return 0;
    }
    bool ItemTypeMove(unsigned int source_index, unsigned int destination_index) {
        return move_vector_element<ItemType*>(source_index, destination_index, itemsTypes);
    }
protected:
    std::vector<ItemType *> itemsTypes;
    bool ToJSON(json_t *json);
    bool FromJSON(json_t *json);
    bool ToSQLite(sqlite3 *db);
    bool FromSQLite(sqlite3 *db);
    bool ToProtoBuf(ItemsDB::Db *db) const;
    bool FromProtoBuf(const ItemsDB::Db &db);
    void clear();
};
