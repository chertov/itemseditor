#include "TreeView.hpp"

#include <iostream>
#include <QMimeData>
#include <QSizePolicy>
#include <QApplication>

#include "TreeModel.hpp"

TreeView::TreeView() : QTreeView() {
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setSelectionMode(QAbstractItemView::ExtendedSelection);

    setHeaderHidden(true);
    setAcceptDrops(true);
    setDropIndicatorShown(true);
    setDragEnabled(true);
    viewport()->setAcceptDrops(true);
    setDragDropMode(QAbstractItemView::DragDrop);
    setDefaultDropAction(Qt::MoveAction);
}

void TreeView::mousePressEvent(QMouseEvent *event) {
    QTreeView::mousePressEvent(event);

    const QModelIndex index = indexAt(event->pos());
    if (!index.isValid()) {
        const Qt::KeyboardModifiers modifiers = QApplication::keyboardModifiers();
        if (!(modifiers & Qt::ShiftModifier) && !(modifiers & Qt::ControlModifier))
        clearSelection();
    }
}
