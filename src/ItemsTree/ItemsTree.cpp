#include "ItemsTree.hpp"

ItemsTree::ItemsTree(QWidget *parent) : QWidget(parent) {
    ui.setupUi(this);

    model = new TreeModel();
    view = new TreeView();
    view->setModel(model);
    ui.forTreeView->addWidget(view);

    connect(view->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(itemSelectionChanged()));
    itemSelectionChanged();
}

ItemsTree::~ItemsTree() {

}

void ItemsTree::itemSelectionChanged() {
    std::vector<BaseProps*> items = GetSelection();
    if (items.size() == 1) {

        BaseProps* baseProps = items[0];

        ui.deleteButton->setEnabled(true);
        ui.newCategoryButton->setEnabled(false);
        ui.newFolderButton->setEnabled(false);
        ui.newItemButton->setEnabled(false);

        if(baseProps->Type() == FOLDER) {
            ui.newCategoryButton->setEnabled(true);
            ui.newFolderButton->setEnabled(true);
        }
        if(baseProps->Type() == CATEGORY) {
            ui.newItemButton->setEnabled(true);
        }
        if(baseProps->Type() == ITEM) {
        }
    } else {
        ui.newFolderButton->setEnabled(true);
        ui.newCategoryButton->setEnabled(true);
        ui.newItemButton->setEnabled(false);
        ui.deleteButton->setEnabled(false);
    }

    emit selectionChanged(items);
}

std::vector<BaseProps*> ItemsTree::GetSelection() {
    std::vector<BaseProps*> items;
    for(QModelIndex index: view->selectionModel()->selectedIndexes())
        items.push_back(static_cast<BaseProps*>(index.internalPointer()));
    //emit selectionChanged(items);
    return items;
}

void ItemsTree::newFolder() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() > 1) return;

    Folder* parent = 0;
    if (indexies.size() == 1) {
        parent = static_cast<Folder*>(indexies[0].internalPointer());
    }

    Folder *folder = DB.NewFolder(parent);
    folder->SetName("Новая папка");

    model->emitLayoutAboutToBeChanged();
    DB.appendFolder(parent, folder);
    model->emitLayoutChanged();

    if (indexies.size() == 1) view->setExpanded(indexies[0], true);
}

void ItemsTree::newCategory() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() > 1) return;

    Folder* parent = 0;
    if (indexies.size() == 1) {
        parent = static_cast<Folder*>(indexies[0].internalPointer());
    }

    Category *category = DB.NewCategory(parent);
    category->SetName("Новая категория");

    model->emitLayoutAboutToBeChanged();
    DB.appendCategory(parent, category);
    model->emitLayoutChanged();

    if (indexies.size() == 1) view->setExpanded(indexies[0], true);
}

void ItemsTree::newItem() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() != 1) return;

    Category* parent = static_cast<Category*>(indexies[0].internalPointer());
    Item *item = DB.NewItem(parent);
    item->SetName("Новый товар");

    model->emitLayoutAboutToBeChanged();
    DB.appendItem(parent, item);
    model->emitLayoutChanged();

    view->setExpanded(indexies[0], true);
}

void ItemsTree::deleteItem() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() == 1) {

        QModelIndex index = indexies[0];
        model->RemoveRow(index);

        index = view->currentIndex();
        if(model->hasIndex(index.row(), index.column(), index.parent())) {
            view->selectionModel()->select(index, QItemSelectionModel::Select);
        }
    }
}

void ItemsTree::reset() {
    view->reset();
}

void ItemsTree::emitLayoutAboutToBeChanged() {
    model->emitLayoutAboutToBeChanged();
}

void ItemsTree::emitLayoutChanged() {
    model->emitLayoutChanged();
}
