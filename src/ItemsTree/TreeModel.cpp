#include "TreeModel.hpp"
#include <QFileInfo>
#include <QDir>
#include <QVector>
#include <algorithm>
#include <QFileIconProvider>
#include <QDateTime>
#include <QDebug>
#include <QMimeData>

#include "collection/collections.hpp"
#include "exception.hpp"

TreeModel::TreeModel(QObject *parent) : QAbstractItemModel(parent) { }

TreeModel::~TreeModel() { }

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent)) {
        return QModelIndex();
    }

    if (!parent.isValid()) {
        Q_ASSERT(row < DB.ChildrenSize());
        PtrInfo* ptrInfo = const_cast<PtrInfo*>(DB.Children(row));
        if(ptrInfo == 0){
            ExceptionSaver ss; ss << "TreeModel::index: ptrInfo = 0";
        }
        return createIndex(row, column, ptrInfo);
    }

    const PtrInfo* parentInfo = DB.check(parent.internalPointer());
    if(parentInfo == 0){
        ExceptionSaver ss; ss << "TreeModel::index: parentInfo = 0";
    }
    Q_ASSERT(parentInfo != 0);
    //Q_ASSERT(parentInfo->mapped);
    Q_ASSERT(parentInfo->ChildrenSize() > row);
    return createIndex(row, column, parentInfo->Children(row));
}

QModelIndex TreeModel::parent(const QModelIndex &child) const {
    if (!child.isValid()) {
        return QModelIndex();
    }

    const PtrInfo* childInfo = DB.check(child.internalPointer());
    if (childInfo == 0) return QModelIndex();

    PtrInfo* parentInfo = childInfo->GetParent();
    if (parentInfo != 0) {
        int dst = findRow(parentInfo);
        if (dst == -777) return QModelIndex();
        return createIndex(dst, RamificationColumn, parentInfo);
    }
    else {
        return QModelIndex();
    }
}

int TreeModel::findRow(const PtrInfo *ptrInfo) const {
    Q_ASSERT(ptrInfo != 0);
    const std::vector<PtrInfo*>& parentInfoChildren = ptrInfo->GetParent() != 0 ? ptrInfo->GetParent()->Childrens(): DB.Childrens();
    auto position = std::find(parentInfoChildren.begin(), parentInfoChildren.end(), ptrInfo);
    Q_ASSERT(position != parentInfoChildren.end());
    return std::distance(parentInfoChildren.begin(), position);
}

int TreeModel::rowCount(const QModelIndex &index) const
{
    if (!index.isValid()) {
        size_t size = DB.ChildrenSize();
        return size;
    }
    const PtrInfo* ptrInfo = DB.check(index.internalPointer());
    Q_ASSERT(ptrInfo != 0);

    return ptrInfo->ChildrenSize();
}

bool TreeModel::hasChildren(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        const PtrInfo* parentInfo = DB.check(parent.internalPointer());
        if(parentInfo == 0){
            ExceptionSaver ss; ss << "TreeModel::hasChildren: parentInfo = 0";
        }
        Q_ASSERT(parentInfo != 0);
//        if (!parentInfo->mapped) {
//            return true;//QDir(parentInfo->fileInfo.absoluteFilePath()).count() > 0;
//        }
    }
    return QAbstractItemModel::hasChildren(parent);
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return ColumnCount;
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    const BaseProps* baseProps = static_cast<const BaseProps*>(DB.check(index.internalPointer()));
    if(baseProps == 0){
        ExceptionSaver ss; ss << "TreeModel::data: baseProps = 0";
    }
    Q_ASSERT(baseProps != 0);

    switch (index.column()) {
    case NameColumn:
        return nameData(baseProps, role);
    default:
        break;
    }
    return QVariant();
}

QVariant TreeModel::nameData(const BaseProps *baseProps, int role) const
{
    switch (role) {
    case Qt::EditRole:
        return QVariant(QString::fromStdString(baseProps->GetName()));
    case Qt::DisplayRole: {
        unsigned int count = baseProps->ChildrenSize();
//        if(count > 0)
//            return QVariant(QString::fromStdString(baseProps->GetName()) + QString("    <") + QString::number(count) + QString(">"));
//        else
//            return QVariant(QString::fromStdString(baseProps->GetName()));
        switch (baseProps->Type()) {
        case FOLDER:
            return QVariant(QString::fromStdString(baseProps->GetName()) + QString("    <") + QString::number(count) + QString(">"));
            break;
        case CATEGORY:
            return QVariant(QString::fromStdString(baseProps->GetName()) + QString("    <") + QString::number(count) + QString(">"));
            break;
        case ITEM: {
            Item *item = static_cast<Item*>(const_cast<BaseProps*>(baseProps));
            return QVariant(QString("<") + QString::fromStdString(item->GetArticle()) + QString(">    ") + QString::fromStdString(item->GetName()));
            break; }
        default:
            break;
        }
//        if (fileInfo.isRoot()) {
//            return fileInfo.absoluteFilePath();
//        }
//        else if (fileInfo.isDir()){
//            return fileInfo.fileName();
//        }
//        else {
//            return fileInfo.completeBaseName();
//        }
        break;}
    case Qt::DecorationRole:
        if (baseProps->Type() == FOLDER) return QVariant(folderIcon);
        if (baseProps->Type() == CATEGORY) return QVariant(categoryIcon);
        if (baseProps->Type() == ITEM) return QVariant(itemIcon);
        //return _metaProvider->icon(fileInfo);
        return QVariant();
    default:
        return QVariant();
    }
    Q_UNREACHABLE();
}

bool TreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) {
        return false;
    }
    if (role != Qt::EditRole) {
        return false;
    }
    if (index.column() != NameColumn) {
        return false;
    }

    QString newName = value.toString();

    BaseProps* baseProps = const_cast<BaseProps*>(static_cast<const BaseProps*>(DB.check(index.internalPointer())));
    baseProps->SetName(newName.toStdString());

    emit dataChanged(index, index.sibling(index.row(), ColumnCount));
    return true;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    const QStringList headers = {"Имя"};
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < headers.size()) {
        return headers[section];
    }
    return QVariant();
}

bool TreeModel::canFetchMore(const QModelIndex &parent) const
{
    if (!parent.isValid()) {
        return false;
    }

    const PtrInfo* parentInfo = DB.check(parent.internalPointer());
    Q_ASSERT(parentInfo != 0);
    if (parentInfo->Type() == ITEM) return false;
//    const NodeInfo* parentInfo = static_cast<const NodeInfo*>(parent.internalPointer());
//    Q_ASSERT(parentInfo != 0);
//    return !parentInfo->mapped;
    return false;
}

void TreeModel::fetchMore(const QModelIndex &parent)
{
//    Q_ASSERT(parent.isValid());
//    const PtrInfo* parentInfo = static_cast<const PtrInfo*>(parent.internalPointer());
//    //NodeInfo* parentInfo = static_cast<NodeInfo*>(parent.internalPointer());
//    Q_ASSERT(parentInfo != 0);
//    Q_ASSERT(!parentInfo->mapped);

//    const QFileInfo& fileInfo = parentInfo->fileInfo;
//    Q_ASSERT(fileInfo.isDir());

//    QDir dir = QDir(fileInfo.absoluteFilePath());
//    QFileInfoList children = dir.entryInfoList(QStringList(), QDir::AllEntries | QDir::NoDotAndDotDot, QDir::Name);

//    int insrtCnt = children.size() - 1;
//    if (insrtCnt < 0) {
//        insrtCnt = 0;
//    }
//    beginInsertRows(parent, 0, insrtCnt);
//    parentInfo->children.reserve(children.size());
//    for (const QFileInfo& entry: children) {
//        NodeInfo nodeInfo(entry, parentInfo);
//        nodeInfo.mapped = !entry.isDir();
//        parentInfo->children.push_back(std::move(nodeInfo));
//    }
//    parentInfo->mapped = true;
//    endInsertRows();
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if (index.isValid() && index.column() == NameColumn) {
//        const PtrInfo* nodeInfo = DB.check(index.internalPointer());
//        if (!nodeInfo->fileInfo.isRoot()) {
//            flags |= Qt::ItemIsEditable;
//        }
    }
    return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | flags;
    //return flags;
}

Qt::DropActions TreeModel::supportedDragActions() const {
    return Qt::CopyAction | Qt::MoveAction;
}
Qt::DropActions TreeModel::supportedDropActions() const {
    return Qt::CopyAction | Qt::MoveAction;
}

QMimeData *TreeModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = QAbstractItemModel::mimeData(indexes);
    //mimeData->setData("application/vnd.text.list", encodedData);
    bool notOnlyItem = false;
    QVector<const PtrInfo*> ptrVector;
    for(int i = 0; i < indexes.size(); ++i) {
        QModelIndex index = indexes[i];
        const PtrInfo* ptrInfo = DB.check(index.internalPointer());
        if (ptrInfo->Type() != ITEM)
            notOnlyItem = true;
        else
            ptrVector.push_back(ptrInfo);
    }
    mimeData->setProperty("itemsPtrs", QVariant::fromValue(ptrVector));
    mimeData->setProperty("size", QVariant(indexes.size()));
    mimeData->setProperty("notOnlyItem", QVariant(notOnlyItem));
    if (indexes.size() == 1) {
        QModelIndex index = indexes[0];
        const PtrInfo* ptrInfo = DB.check(index.internalPointer());
        mimeData->setProperty("ptr", qVariantFromValue(((void *) ptrInfo)));
    }
    return mimeData;
}

bool TreeModel::dropMimeData(const QMimeData *mime, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    //std::cout << "row: " << row << "  column: " << column << std::endl;
    const PtrInfo *destination = DB.check(parent.internalPointer());

    QVariant source_var_ptr = mime->property("ptr");
    PtrInfo *source = static_cast<PtrInfo*>(source_var_ptr.value<void *>());

    if (action == Qt::IgnoreAction)
        return true;

    unsigned int selected_items_count = mime->property("size").toInt();
    if (selected_items_count == 1) {
        if (destination == 0){
            if (source->Type() == FOLDER) {
                Folder *src  = static_cast<Folder*>(const_cast<PtrInfo*>(source));
                emitLayoutAboutToBeChanged();
                bool res = DB.moveFolder(0, src, row);
                emitLayoutChanged();
                return res;
            }
            if (source->Type() == CATEGORY) {
                Category *src  = static_cast<Category*>(const_cast<PtrInfo*>(source));
                emitLayoutAboutToBeChanged();
                bool res = DB.moveCategory(0, src, row);
                emitLayoutChanged();
                return res;
            }
        }
        else {
            if (source->Type() == ITEM && destination->Type() == CATEGORY) {
                Item *src  = static_cast<Item*>(const_cast<PtrInfo*>(source));
                Category *dest = static_cast<Category*>(const_cast<PtrInfo*>(destination));
                emitLayoutAboutToBeChanged();
                bool res = DB.moveItem(dest, src, row);
                emitLayoutChanged();
                return res;
            }
            if (source->Type() == FOLDER && destination->Type() == FOLDER) {
                Folder *src  = static_cast<Folder*>(const_cast<PtrInfo*>(source));
                Folder *dest = static_cast<Folder*>(const_cast<PtrInfo*>(destination));
                emitLayoutAboutToBeChanged();
                bool res = DB.moveFolder(dest, src, row);
                emitLayoutChanged();
                return res;
            }
            if (source->Type() == CATEGORY && destination->Type() == FOLDER) {
                Category *src  = static_cast<Category*>(const_cast<PtrInfo*>(source));
                Folder *dest = static_cast<Folder*>(const_cast<PtrInfo*>(destination));
                emitLayoutAboutToBeChanged();
                bool res = DB.moveCategory(dest, src, row);
                emitLayoutChanged();
                return res;
            }
        }
    }
    if (selected_items_count > 1) {
        if (destination == 0) return false;
        bool notOnlyItem = mime->property("notOnlyItem").toBool();
        if (notOnlyItem) return false;
        if (destination->Type() != CATEGORY) return false;
        Category *dest  = static_cast<Category*>(const_cast<PtrInfo*>(destination));

        QVector<const PtrInfo*> ptrVector = mime->property("itemsPtrs").value<QVector<const PtrInfo*>>();
        emitLayoutAboutToBeChanged();
        for (unsigned int i = 0; i < ptrVector.size(); ++i) {
            const PtrInfo* ptrInfo = ptrVector[i];
            Q_ASSERT(ptrInfo->Type() == ITEM);
            Item *src = static_cast<Item*>(const_cast<PtrInfo*>(ptrInfo));
            if(!DB.moveItem(dest, src, row+i)) {
                emitLayoutChanged();
                return false;
            }
        }
        emitLayoutChanged();
        return true;
    }

    return false;
}

void TreeModel::RemoveRow(const QModelIndex &index) {
    beginRemoveRows(index.parent(), index.row(), index.row() + 1);
    PtrInfo* ptrInfo = static_cast<PtrInfo*>(index.internalPointer());
    DB.DeletePtr(ptrInfo);
    endRemoveRows();
}
