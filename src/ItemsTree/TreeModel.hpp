#pragma once

#include <QAbstractItemModel>
#include <QVector>
#include <QIcon>

#include "collection/collections.hpp"

Q_DECLARE_METATYPE(QVector<const PtrInfo*>)
Q_DECLARE_METATYPE(QVector<int>)

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit TreeModel(QObject *parent = 0);
    ~TreeModel();

    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;

    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    bool canFetchMore(const QModelIndex &parent) const;
    void fetchMore(const QModelIndex &parent);

    Qt::ItemFlags flags(const QModelIndex &index) const;

    bool hasChildren(const QModelIndex &parent) const;

    void beginResetModel() { QAbstractItemModel::beginResetModel(); }
    void endResetModel() { QAbstractItemModel::endResetModel(); }

    void emitLayoutAboutToBeChanged() { emit layoutAboutToBeChanged(); }
    void emitLayoutChanged() { emit layoutChanged(); }

    Qt::DropActions supportedDragActions() const;
    Qt::DropActions supportedDropActions() const;

    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

    void RemoveRow(const QModelIndex &index);

private:
    enum Columns
    {
        RamificationColumn,
        NameColumn = RamificationColumn,
        ColumnCount
    };

    QIcon folderIcon = QIcon("folder.png");
    QIcon categoryIcon = QIcon("category.png");
    QIcon itemIcon = QIcon("item.png");

    int findRow(const PtrInfo* nodeInfo) const;
    QVariant nameData(const BaseProps* baseProps, int role) const;
};
