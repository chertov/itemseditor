#pragma once

#include <QTreeView>
#include <QDropEvent>
#include <QDragEnterEvent>

class TreeView : public QTreeView {

public:
    TreeView();
    void mousePressEvent(QMouseEvent *event);
};
