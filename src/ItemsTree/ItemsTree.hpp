#pragma once

#include "TreeModel.hpp"
#include "TreeView.hpp"

#include "ui_itemstree.h"

class ItemsTree : public QWidget {
    Q_OBJECT
public:
    ItemsTree(QWidget *parent = 0);
    ~ItemsTree();

    std::vector<BaseProps*> GetSelection();
    void reset();
    void emitLayoutAboutToBeChanged();
    void emitLayoutChanged();

private:
    Ui::ItemsTree ui;
    TreeModel *model;
    TreeView *view;

private slots:
    void newFolder();
    void newCategory();
    void newItem();
    void deleteItem();

public slots:
    void itemSelectionChanged();

signals:
    void selectionChanged(std::vector<BaseProps*> items);
};
