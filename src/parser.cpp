#include <iostream>
#include <sstream>

#include "parser.hpp"
#include "exception.hpp"

bool log_error(const char* str)
{
    std::cout << "JSON render error: " << str << std::endl;
    return false;
}

bool log_error_parse(const char* str)
{
    std::cout << "JSON parser error: " << str << std::endl;
    return false;
}

bool jansson_log_error(std::string str)
{
    std::cout << "Jansson parser error: " << str.c_str() << std::endl;
    return false;
}

bool json_parse_double_lib(const json_t *object, const char * key, double &val)
{
    if (object == NULL) return jansson_log_error("object is NULL");
    json_t *json = json_object_get(object, key);
    if (json == NULL)
        return jansson_log_error(std::string("Can't get json by key ") + std::string(key));
    if (!json_is_real(json))
    {
        if (!json_is_integer(json))
            return jansson_log_error("not real or integer");
        else
        {
            val = json_integer_value(json);
            return true;
        }
    }
    val = json_number_value(json);
    return true;
}
bool json_parse_array_double(const json_t *object, unsigned int index, double &val) {
    if (object == NULL) return jansson_log_error("object is NULL");
    json_t *json = json_array_get(object, index);
    if (json == NULL) {
        std::stringstream ss; ss << "Can't get json by index " << index;
        return jansson_log_error(ss.str());
    }
    if (!json_is_real(json)) return jansson_log_error("not real");
    val = json_number_value(json);
    return true;
}

bool json_parse_int(const json_t *object, const char * key, int &val)
{
    if (object == NULL) return jansson_log_error("object is NULL");
    json_t *json = json_object_get(object, key);
    if (json == NULL)
        return jansson_log_error(std::string("Can't get json by key ") + std::string(key));
    if (!json_is_integer(json)) return jansson_log_error("not integer");
    val = json_integer_value(json);
    return true;
}
bool json_parse_int(const json_t *object, const char * key, unsigned int &val)
{
    return json_parse_int(object, key, reinterpret_cast<int &>(val));
}
bool json_parse_bool(const json_t *object, const char * key, bool &val)
{
    if (object == NULL) return jansson_log_error("object is NULL");
    json_t *json = json_object_get(object, key);
    if (json == NULL)
        return jansson_log_error(std::string("Can't get json by key ") + std::string(key));
    if (json_is_true(json)) { val = true; return true; }
    if (json_is_false(json)) { val = false; return true; }
    return jansson_log_error("not boolean");
}
bool json_parse_string(const json_t *object, const char * key, std::string &val)
{
    if (object == NULL) return jansson_log_error("object is NULL");
    json_t *json = json_object_get(object, key);
    if (json == NULL)
        return jansson_log_error(std::string("Can't get json by key ") + std::string(key));
    if (!json_is_string(json)) return jansson_log_error("not string");
    val = json_string_value(json);
    return true;
}
