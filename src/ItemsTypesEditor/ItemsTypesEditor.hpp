#pragma once
#include "ui_ItemsTypesEditor.h"

#include <QtWidgets/QDialog>

#include "collection/collections.hpp"

#include "ItemsTypesModel.hpp"

class ItemsTypesEditor : public QDialog {
    Q_OBJECT
public:
    ItemsTypesEditor(QWidget *parent = 0);
    ~ItemsTypesEditor();

private:
    ItemsTypesModel *model;
    QListView *view;

    Ui::ItemsTypesEditor ui;
private slots:
    void addItemType();
    void removeItemType();
    void editItemType();
    void selectionChanged();
};
