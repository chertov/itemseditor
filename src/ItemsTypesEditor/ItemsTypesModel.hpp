#pragma once
#include <QAbstractListModel>
#include "collection/collections.hpp"

class ItemsTypesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ItemsTypesModel(QObject *parent = 0);
    ~ItemsTypesModel();

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void emitLayoutAboutToBeChanged() { emit layoutAboutToBeChanged(); }
    void emitLayoutChanged() { emit layoutChanged(); }

    Qt::DropActions supportedDragActions() const;
    Qt::DropActions supportedDropActions() const;

    QMimeData *mimeData(const QModelIndexList &indexes) const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

private:
    enum Columns
    {
        RamificationColumn,
        NameColumn = RamificationColumn,
        ColumnCount
    };
};
