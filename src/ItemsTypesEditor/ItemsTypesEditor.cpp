#include "ItemsTypesEditor.hpp"

#include <iostream>

ItemsTypesEditor::ItemsTypesEditor(QWidget *parent) : QDialog(parent) {
    ui.setupUi(this);

    model = new ItemsTypesModel();
    view = ui.view;
    view->setModel(model);
    connect(view->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
            this, SLOT(selectionChanged()));
    selectionChanged();
}

ItemsTypesEditor::~ItemsTypesEditor() { }

void ItemsTypesEditor::addItemType() {
    model->emitLayoutAboutToBeChanged();
    DB.NewItemType();
    model->emitLayoutChanged();
}

void ItemsTypesEditor::removeItemType() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() == 0) return;
    unsigned int row = indexies[0].row();
    if (row >= DB.ItemsTypesSize()) return;

    ItemType *itemType = DB.ItemTypeByIndex(row);
    model->emitLayoutAboutToBeChanged();
    DB.DeleteItemType(itemType);
    model->emitLayoutChanged();

    selectionChanged();
}
void ItemsTypesEditor::editItemType() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() == 0) return;
    unsigned int row = indexies[0].row();
    if (row >= DB.ItemsTypesSize()) return;

    model->emitLayoutAboutToBeChanged();
    ItemType *itemType = DB.ItemTypeByIndex(row);
    itemType->SetName(ui.nameEdit->text().toStdString());
    itemType->SetShortDescription(ui.shortDescriptionEdit->toPlainText().toStdString());
    itemType->SetDescription(ui.fullDescriptionEdit->toPlainText().toStdString());
    model->emitLayoutChanged();
}

void ItemsTypesEditor::selectionChanged() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() > 0 && DB.ItemsTypesSize() > 0)
        ui.removeButton->setEnabled(true);
    else
        ui.removeButton->setEnabled(false);

    if (indexies.size() == 0) return;
    unsigned int row = indexies[0].row();
    if (row >= DB.ItemsTypesSize()) return;

    ItemType *itemType = DB.ItemTypeByIndex(row);
    ui.nameEdit->setText(QString::fromStdString(itemType->GetName()));
    ui.shortDescriptionEdit->setPlainText(QString::fromStdString(itemType->GetShortDescription()));
    ui.fullDescriptionEdit->setPlainText(QString::fromStdString(itemType->GetDescription()));
}

