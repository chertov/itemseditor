#include "ItemsTypesModel.hpp"
#include <algorithm>
#include <QDebug>
#include <QMimeData>
#include <QIcon>
#include "collection/collections.hpp"

#include "tools.hpp"

ItemsTypesModel::ItemsTypesModel(QObject *parent) : QAbstractListModel(parent) {
}

ItemsTypesModel::~ItemsTypesModel() { }

int ItemsTypesModel::rowCount(const QModelIndex &) const {
    return DB.ItemsTypesSize();
}

int ItemsTypesModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return ColumnCount;
}

QVariant ItemsTypesModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return QVariant();
    if (index.row() >= DB.ItemsTypesSize()) return QVariant();

    ItemType *itemType = DB.ItemTypeByIndex(index.row());
    switch (index.column()) {
    case NameColumn:
        switch (role) {
        case Qt::EditRole:
            return QVariant(QString::fromStdString(itemType->GetName()));
        case Qt::DisplayRole:
            return QVariant(QString::fromStdString(itemType->GetName()));
        default:
            return QVariant();
        }
        break;
    default:
        break;
    }
    return QVariant();
}

QVariant ItemsTypesModel::headerData(int section, Qt::Orientation orientation, int role) const {
    const QStringList headers = {"Название"};
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < headers.size()) {
        return headers[section];
    }
    return QVariant();
}

Qt::ItemFlags ItemsTypesModel::flags(const QModelIndex &index) const {
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    return flags | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled; // | Qt::ItemIsEditable;
}

Qt::DropActions ItemsTypesModel::supportedDragActions() const {
    return Qt::MoveAction;
}
Qt::DropActions ItemsTypesModel::supportedDropActions() const {
    return Qt::MoveAction;
}

QMimeData *ItemsTypesModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = QAbstractItemModel::mimeData(indexes);
    if (indexes.size() == 1) {
        QModelIndex index = indexes[0];
        mimeData->setProperty("source_index", QVariant(index.row()));
    }
    return mimeData;
}

bool ItemsTypesModel::dropMimeData(const QMimeData *mime, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if (action == Qt::IgnoreAction) return true;

    int source_index = mime->property("source_index").toInt();

    emitLayoutAboutToBeChanged();
    bool res = DB.ItemTypeMove(source_index, row);
    emitLayoutChanged();

    return res;
}
