#pragma once
#include <sstream>
#include "assert.hpp"

class ExceptionSaver : public std::stringstream {
public:
    std::string path;
    ExceptionSaver(std::string path = "log.txt", std::string name = "");
    ~ExceptionSaver();
};
