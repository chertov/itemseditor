#include "exception.hpp"

#include <iostream>
#include <sstream>
#include <fstream>
//#include <execinfo.h>

#include <QString>
#include <QFile>

#include "logger.hpp"

ExceptionSaver::ExceptionSaver(std::string path, std::string name) {
    if (name.size()==0) name = path;
    if (path.size()!=0)
        this->path = path;
    else
        this->path = "log_0.txt";
}

ExceptionSaver::~ExceptionSaver() {
    std::cout << str() << std::endl;
    std::string utf_str = LOGSTR;
    utf_str.append("\n\r");
    utf_str.append(str());
    {
        unsigned int id = 0;
        QString path = QString("log_") + QString::number(id) + QString(".txt");
        while(QFile::exists(path)) {
            id++;
            path = QString("log_") + QString::number(id) + QString(".txt");
        }
        std::ofstream file(path.toStdString().data(), std::ios::out | std::ios::binary | std::ios::trunc);
        if (file.is_open())
            file << utf_str.data();
        file.close();
    }
    throw std::runtime_error(utf_str);
}
