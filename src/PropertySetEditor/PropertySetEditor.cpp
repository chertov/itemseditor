#include "PropertySetEditor.hpp"

#include <iostream>
#include "PropertyEditor/PropertyEditor.hpp"
#include "ItemsEditor/ItemsEditor.hpp"

PropertySetEditor::PropertySetEditor(QWidget *parent) : QWidget(parent) {
    ui.setupUi(this);

    model = new PropertySetModel();
    SetCategory(0);
    view = ui.propertySetView;
    view->setModel(model);
    connect(view->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
            this, SLOT(selectionChanged()));
    connect(view, SIGNAL(doubleClicked(const QModelIndex &)),
            this, SLOT(doubleClicked(const QModelIndex &)));
    selectionChanged();
}

PropertySetEditor::~PropertySetEditor() { }

void PropertySetEditor::SetCategory(Category *category) {
    model->emitLayoutAboutToBeChanged();
    this->category = category;
    model->category = category;
    model->emitLayoutChanged();
}

void PropertySetEditor::addProperty() {
    Property *property = DB.NewProperty();
//    property.SetName("Новое свойство");
//    property.SetType(PROPERTY_BOOL);

    PropertyEditor propertyEditor(property->GetId());
    int dialogCode = propertyEditor.exec();
    if(dialogCode == QDialog::Accepted) {
        model->emitLayoutAboutToBeChanged();
        auto properties = category->GetProperties();
        properties.push_back(property->GetId());
        category->SetProperties(properties);
        model->emitLayoutChanged();
    }
}

void PropertySetEditor::removeProperty() {
}

void PropertySetEditor::selectionChanged() {
}

void PropertySetEditor::doubleClicked(const QModelIndex &index) {
    if (index.row() < 0) return;
    if (index.row() >= category->GetProperties().size()) return;

    PropertyEditor propertyEditor(category->GetProperties()[index.row()]);
    int dialogCode = propertyEditor.exec();
    if(dialogCode == QDialog::Accepted) {
        model->emitLayoutAboutToBeChanged();
//        auto properties = category->GetProperties();
//        //properties[index.row()] = propertyEditor.property;
//        category->SetProperties(properties);
        model->emitLayoutChanged();
    }
}

void PropertySetEditor::editItemsProperties() {
    model->emitLayoutAboutToBeChanged();
    ItemsEditor itemsEditor(category);
    itemsEditor.exec();
    model->emitLayoutChanged();
}

