#pragma once
#include "ui_PropertySetEditor.h"

#include <QtWidgets/QDialog>

#include "collection/collections.hpp"

#include "PropertySetModel.hpp"

class PropertySetEditor : public QWidget {
    Q_OBJECT
public:
    PropertySetEditor(QWidget *parent = 0);
    ~PropertySetEditor();

    PropertySetModel *model;
    QListView *view;

    void SetCategory(Category *category);
    Category* GetCategory() { return category; }

    void emitLayoutAboutToBeChanged() { model->emitLayoutAboutToBeChanged(); }
    void emitLayoutChanged() { model->emitLayoutChanged(); }

private:
    Category *category;

    Ui::PropertySetEditor ui;
private slots:
    void addProperty();
    void removeProperty();
    void selectionChanged();
    void doubleClicked(const QModelIndex &index);
    void editItemsProperties();
};
