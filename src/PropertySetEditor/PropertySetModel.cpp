#include "PropertySetModel.hpp"
#include <algorithm>
#include <QDebug>
#include <QMimeData>
#include <QIcon>
#include "collection/collections.hpp"

#include "tools.hpp"

PropertySetModel::PropertySetModel(QObject *parent) : QAbstractListModel(parent) {
    category = 0;
}

PropertySetModel::~PropertySetModel() { }

int PropertySetModel::rowCount(const QModelIndex &) const {
    if(category != 0)
        return category->GetPropertiesSize();
    return 0;
}

int PropertySetModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return ColumnCount;
}

QVariant PropertySetModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant();
    if (category == 0) return QVariant();
    if (index.row() >= category->GetPropertiesSize()) return QVariant();

    Property *property = category->GetPropertyPtrByIndex(index.row());

    switch (index.column()) {
    case NameColumn:
        return nameData(property, role);
    default:
        break;
    }
    return QVariant();
}

QVariant PropertySetModel::nameData(Property *property, int role) const {
    QString left_scope = "  <";
    QString Name = QString::fromStdString(property->GetName());
    PropertyType ptype = property->GetType();
    if (ptype == PROPERTY_BOOL) Name += left_scope + "Да/Нет" + ">";
    if (ptype == PROPERTY_INT) Name += left_scope + "Целочисленное" + ">";
    if (ptype == PROPERTY_REAL) Name += left_scope + "Дробное" + ">";
    if (ptype == PROPERTY_ENUM) Name += left_scope + "Перечисление" + ">";
    if (ptype == PROPERTY_UNKNOWN) Name += left_scope + "Некорректный тип" + ">";

    switch (role) {
    case Qt::EditRole:
        return QVariant(QString::fromStdString(property->GetName()));
    case Qt::DisplayRole:
        return QVariant(Name);
    default:
        return QVariant();
    }
    Q_UNREACHABLE();
}

bool PropertySetModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) return false;
    if (category == 0) return false;
    if (index.row() >= category->GetPropertiesSize()) return false;

    if (index.isValid() && role == Qt::EditRole) {
        category->GetPropertyPtrByIndex(index.row())->SetName(value.toString().toStdString());
        emit dataChanged(index, index);
        return true;
    }
    return false;
}


QVariant PropertySetModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    const QStringList headers = {"Имя", "Тип"};
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < headers.size()) {
        return headers[section];
    }
    return QVariant();
}

Qt::ItemFlags PropertySetModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    return flags | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled; // | Qt::ItemIsEditable;
}

Qt::DropActions PropertySetModel::supportedDragActions() const {
    return Qt::MoveAction;
}
Qt::DropActions PropertySetModel::supportedDropActions() const {
    return Qt::MoveAction;
}

QMimeData *PropertySetModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = QAbstractItemModel::mimeData(indexes);
    if (indexes.size() == 1) {
        QModelIndex index = indexes[0];
        mimeData->setProperty("source_index", QVariant(index.row()));
    }
    return mimeData;
}

bool PropertySetModel::dropMimeData(const QMimeData *mime, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if (action == Qt::IgnoreAction) return true;

    int source_index = mime->property("source_index").toInt();

    emitLayoutAboutToBeChanged();
    bool res = category->PropertyMove(source_index, row);
    emitLayoutChanged();

    return res;
}
