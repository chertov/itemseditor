#include "ImagesList.hpp"

#include <iostream>
#include <fstream>

#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QImage>
#include <QClipboard>
#include <QUrl>
#include <QMessageBox>
#include <QBuffer>

#include "collection/collections.hpp"

ImagesList::ImagesList(QWidget *parent) : QDialog(parent) {
    global = true;
    Init();
    ui.collectionButton->setHidden(true);
}

ImagesList::ImagesList(std::vector<IdType> images, QWidget *parent) : QDialog(parent) {
    global = false;
    this->images = images;
    Init();
}

void ImagesList::Init() {
    ui.setupUi(this);

    Qt::WindowFlags flags = windowFlags();
    flags |= Qt::WindowMaximizeButtonHint;
    flags |= Qt::WindowContextHelpButtonHint;
    setWindowFlags( flags );

    view = ui.listView;
    view->setViewMode(QListView::IconMode);
    view->setIconSize(QSize(200,200));
    view->setResizeMode(QListView::Adjust);

    if(global)
        model = new ImageModel();
    else
        model = new ImageModel(images);
    view->setModel(model);

    connect(view->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(selectionChanged()));
    selectionChanged();
}

ImagesList::~ImagesList() {
}

void ImagesList::createImage() {
    model->createImage();
}

void ImagesList::removeImage() {

}

void ImagesList::fromCollection() {
    ImagesList editor;
    editor.SetSelectorMode();
    int dialogCode = editor.exec();
    if(dialogCode == QDialog::Accepted) {
        Image* image = editor.GetSelected();
        model->addImage(image->GetId());
    }
}

void ImagesList::selectionChanged() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();

    if(selectorMode) {
        ui.okButton->setEnabled(false);
        if (indexies.size() != 1) return;
        ui.okButton->setEnabled(true);
    } else {
        ui.removeButton->setEnabled(false);
        if (indexies.size() != 1) return;
        ui.removeButton->setEnabled(true);
    }
}

std::vector<IdType> ImagesList::GetImages() {
    return model->GetImages();
}

Image* ImagesList::GetSelected() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() != 1) return 0;
    if(!global) return 0;
    return DB.ImageByIndex(indexies[0].row());
}


void ImagesList::SetSelectorMode() {
    selectorMode = true;
    ui.createButton->setHidden(true);
    ui.removeButton->setHidden(true);
    ui.collectionButton->setHidden(true);
    selectionChanged();
}
