#pragma once
#include "ui_ImagePreview.h"

#include <QDialog>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <vector>
#include "collection/collections.hpp"

class ImagePreview : public QWidget {
    Q_OBJECT
public:
    ImagePreview(QWidget *parent = 0);
    ~ImagePreview();

    void SetBaseProps(BaseProps* props);

private:
    Ui::ImagePreview ui;
    BaseProps* props = 0;

    QGraphicsScene* scene;
    QGraphicsView* view;
    QGraphicsPixmapItem* item;

    void setQImage(const QImage *image);
private slots:

    void editImage();
    void clearImage();
};

