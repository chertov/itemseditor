#pragma once
#include "ui_ImagesList.h"

#include <QDialog>
#include <QListView>

#include <vector>
#include "collection/collections.hpp"

#include "ImageModel.hpp"

class ImagesList : public QDialog {
    Q_OBJECT
public:
    ImagesList(QWidget *parent = 0);
    ImagesList(std::vector<IdType> images, QWidget *parent = 0);
    ~ImagesList();

    std::vector<IdType> GetImages();
    Image* GetSelected();

    void SetSelectorMode();
private:
    Ui::ImagesList ui;

    ImageModel *model;
    QListView *view;

    bool global = true;
    bool selectorMode = false;
    std::vector<IdType> images;

    void Init();
private slots:
    void createImage();
    void removeImage();
    void fromCollection();

    void selectionChanged();
};

