#pragma once
#include "ui_ImageEditor.h"

#include <QDialog>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <vector>
#include "collection/collections.hpp"

class ImageEditor : public QDialog {
    Q_OBJECT
public:
    ImageEditor(Image* image = 0, QWidget *parent = 0);
    ~ImageEditor();

    Image* GetImage();
    void DisableCollectionButton();
//    std::string GetBase64();
//    void SetBase64(std::string image_data);

private:
    Ui::ImageEditor ui;

    QGraphicsScene* scene;
    QGraphicsView* view;
    QGraphicsPixmapItem* item;
    QImage image;
    QNetworkAccessManager *manager;
    Image* imagePtr;


    void setQImage(QImage &image);
private slots:

    void fromCollection();
    void fromFile();
    void fromClipboard();

    void replyFinished(QNetworkReply*);

};

