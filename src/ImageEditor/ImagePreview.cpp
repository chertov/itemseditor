#include "ImagePreview.hpp"

#include <iostream>
#include <fstream>

#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QImage>
#include <QClipboard>
#include <QUrl>
#include <QMessageBox>
#include <QBuffer>

#include "ImageEditor.hpp"
#include "ImagesList.hpp"

ImagePreview::ImagePreview(QWidget *parent) : QWidget(parent) {
    ui.setupUi(this);

    Qt::WindowFlags flags = windowFlags();
    flags |= Qt::WindowMaximizeButtonHint;
    flags |= Qt::WindowContextHelpButtonHint;
    setWindowFlags( flags );

    scene = new QGraphicsScene();
    view = ui.graphicsView;
    view->setScene(scene);
}

ImagePreview::~ImagePreview() {
}


void ImagePreview::SetBaseProps(BaseProps* props) {
    this->props = props;
    scene->clear();

    if(props == 0) return;
    if(props->Type() == FOLDER || props->Type() == CATEGORY) {
        BaseFolder* folder = static_cast<BaseFolder*>(props);
        Image* image = DB.ImageById(folder->GetImage());
        ui.imgLabel->setText(QString("Изображение: ") + QString::number(folder->GetImage()));
        if(image != 0) setQImage(image->GetPreview());
    }
    if(props->Type() == ITEM) {
        Item* item = static_cast<Item*>(props);
        if(item->GetImages().size() > 0) {
            ui.imgLabel->setText(QString("Изображение: ") + QString::number(item->GetImages()[0]));
            Image* image = DB.ImageById(item->GetImages()[0]);
            if(image != 0) setQImage(image->GetPreview());
        }
    }
}

void ImagePreview::setQImage(const QImage *image) {
    item = new QGraphicsPixmapItem(QPixmap::fromImage(*image));
    scene->clear();
    scene->addItem(item);
}

void ImagePreview::editImage() {
    if(props == 0) return;

    if(props->Type() == FOLDER || props->Type() == CATEGORY) {
        BaseFolder* folder = static_cast<BaseFolder*>(props);
        Image* image = DB.ImageById(folder->GetImage());
        ui.imgLabel->setText(QString("Изображение: ") + QString::number(folder->GetImage()));
        ImageEditor editor(image);
        int dialogCode = editor.exec();
        if(dialogCode == QDialog::Accepted) {
            Image *image = editor.GetImage();
            folder->SetImage(image->GetId());
        }
    }
    if(props->Type() == ITEM) {
        Item* item = static_cast<Item*>(props);
        ImagesList editor(item->GetImages());
        ui.imgLabel->setText(QString("Изображение: ") + QString::number(item->GetImages()[0]));
        int dialogCode = editor.exec();
        if(dialogCode == QDialog::Accepted)
            item->SetImages(editor.GetImages());
    }
}

void ImagePreview::clearImage() {

}

