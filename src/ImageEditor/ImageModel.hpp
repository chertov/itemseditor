#pragma once
#include <QAbstractListModel>
#include "collection/collections.hpp"

class ImageModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ImageModel(QObject *parent = 0);
    explicit ImageModel(std::vector<IdType> images, QObject *parent = 0);
    ~ImageModel();

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void emitLayoutAboutToBeChanged() { emit layoutAboutToBeChanged(); }
    void emitLayoutChanged() { emit layoutChanged(); }

    void createImage();
    void addImage(const IdType &image_id);
    std::vector<IdType> GetImages();

//    Qt::DropActions supportedDragActions() const;
//    Qt::DropActions supportedDropActions() const;

//    QMimeData *mimeData(const QModelIndexList &indexes) const;
//    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);

//    bool insertRows(int position, int rows, const QModelIndex &parent);
//    bool removeRows(int position, int rows, const QModelIndex &parent);

private:
    enum Columns
    {
        RamificationColumn,
        NameColumn = RamificationColumn,
        ColumnCount
    };

    bool global = true;
    std::vector<IdType> images;

    QVariant nameData(Image *image, int role) const;
};
