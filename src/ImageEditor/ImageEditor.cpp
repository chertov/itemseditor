#include "ImageEditor.hpp"

#include <iostream>
#include <fstream>

#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QImage>
#include <QClipboard>
#include <QUrl>
#include <QMessageBox>
#include <QBuffer>

#include "collection/collections.hpp"
#include "ImagesList.hpp"
#include "exception.hpp"

ImageEditor::ImageEditor(Image* image, QWidget *parent) : QDialog(parent) {
    ui.setupUi(this);

    Qt::WindowFlags flags = windowFlags();
    flags |= Qt::WindowMaximizeButtonHint;
    flags |= Qt::WindowContextHelpButtonHint;
    setWindowFlags( flags );

    scene = new QGraphicsScene();
    view = ui.graphicsView;
    view->setScene(scene);

    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
           this, SLOT(replyFinished(QNetworkReply*)));

    imagePtr = image;
    if(imagePtr != 0) {
        std::string png;
        if(!imagePtr->GetPNGData(png)) {
            ExceptionSaver ss; ss << "Can't load image png";
        };
        QByteArray ba = QByteArray::fromStdString(png);
        QImage image = QImage::fromData(ba);
        if(!image.isNull()) setQImage(image);
    }
}

ImageEditor::~ImageEditor() {
}

void ImageEditor::fromCollection() {
    ImagesList editor;
    editor.SetSelectorMode();
    int dialogCode = editor.exec();
    if(dialogCode == QDialog::Accepted) {
        imagePtr = editor.GetSelected();
        if(imagePtr != 0) {
            std::string png;
            if(!imagePtr->GetPNGData(png)) {
                ExceptionSaver ss; ss << "Can't load image png";
            };
            QByteArray ba = QByteArray::fromStdString(png);
            QImage image = QImage::fromData(ba);
            if(!image.isNull()) setQImage(image);
        }
    }
}

void ImageEditor::fromClipboard() {
    QClipboard *clipboard = QApplication::clipboard();
    QImage image = clipboard->image();
    if(!image.isNull()) {
        setQImage(image);
    } else {
        QString url_str = clipboard->text();

        QUrl url(url_str);
        if(url.isValid()) {
            manager->get(QNetworkRequest(url));
        }
    }
}

void ImageEditor::replyFinished(QNetworkReply *reply) {
    if (reply->error() == QNetworkReply::NoError) {
        QByteArray bytes = reply->readAll();
        QImage image = QImage::fromData(bytes);
        if(!image.isNull()) {
            setQImage(image);
        } else {
            QMessageBox::warning(this,
                QString("Не удалось загрузить изображение."),
                QString("В буффере обмена найдена ссылка: \n") +
                reply->url().toString() + "\n" +
                QString("К сожалению, изображение по данной ссылке загрузить не удалось."));
        }
    }
}

void ImageEditor::fromFile() {
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Открыть изображение"), "", tr("Файлы изображений (*.png *.jpg *.bmp)"));

    QImage image = QImage(fileName);
    if(!image.isNull()) {
        setQImage(image);
    } else {
        QMessageBox::warning(this,
            QString("Не удалось загрузить изображение."),
            fileName + "\n" +
            QString("К сожалению, изображение из данного фала загрузить не удалось."));
    }
}

void ImageEditor::setQImage(QImage &image) {
    this->image = image;
    item = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    scene->clear();
    scene->addItem(item);
}

Image* ImageEditor::GetImage() {
    if(imagePtr == 0) imagePtr = DB.NewImage();

    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QIODevice::WriteOnly);
    image.save(&buffer, "PNG"); // writes image into ba in PNG format
    imagePtr->SetPNGData(ba.toStdString());

    return imagePtr;
}

void ImageEditor::DisableCollectionButton() {
    ui.collectionButton->setHidden(true);
}
