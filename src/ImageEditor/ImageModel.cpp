#include "ImageModel.hpp"
#include <algorithm>
#include <QDebug>
#include <QMimeData>
#include <QIcon>
#include "collection/collections.hpp"
#include "tools.hpp"

#include "ImageEditor.hpp"

ImageModel::ImageModel(QObject *parent) : QAbstractListModel(parent) {
    global = true;
}
ImageModel::ImageModel(std::vector<IdType> images, QObject *parent) : QAbstractListModel(parent) {
    global = false;
    this->images = images;
}

ImageModel::~ImageModel() { }

int ImageModel::rowCount(const QModelIndex &) const {
    if(global) return DB.ImagesSize();
    return images.size();
}

int ImageModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return ColumnCount;
}

QVariant ImageModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return QVariant();
    Image *image;
    if(global) {
        if (index.row() >= DB.ImagesSize()) return QVariant();
        image = DB.ImageByIndex(index.row());
    } else {
        if (index.row() >= images.size()) return QVariant();
        image = DB.ImageById(images[index.row()]);
    }

    switch (index.column()) {
    case NameColumn:
        return nameData(image, role);
    default:
        break;
    }
    return QVariant();
}

QVariant ImageModel::nameData(Image *image, int role) const {
    switch (role) {
    case Qt::DisplayRole:
        return QVariant(QString::fromStdString(image->GetName()));
    case Qt::DecorationRole: {
//        QByteArray ba = QByteArray::fromBase64(QByteArray::fromStdString(image->GetBase64()));
//        QImage image = QImage::fromData(ba)
//        return QVariant(QIcon(QPixmap::fromImage(QImage::fromData(ba))));
        return QVariant(QIcon(QPixmap::fromImage(*image->GetPreview())));
    }
    default:
        return QVariant();
    }
    Q_UNREACHABLE();
}

Qt::ItemFlags ImageModel::flags(const QModelIndex &index) const {
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    return flags;
}

//Qt::DropActions EnumModel::supportedDragActions() const {
//    return Qt::MoveAction;
//}
//Qt::DropActions EnumModel::supportedDropActions() const {
//    return Qt::MoveAction;
//}

//QMimeData *EnumModel::mimeData(const QModelIndexList &indexes) const
//{
//    QMimeData *mimeData = QAbstractItemModel::mimeData(indexes);
//    if (indexes.size() == 1) {
//        QModelIndex index = indexes[0];
//        mimeData->setProperty("source_index", QVariant(index.row()));
//    }
//    return mimeData;
//}

//bool EnumModel::dropMimeData(const QMimeData *mime, Qt::DropAction action, int row, int column, const QModelIndex &parent)
//{
//    if (action == Qt::IgnoreAction) return true;

//    int source_index = mime->property("source_index").toInt();
//    emitLayoutAboutToBeChanged();
//    bool res = move_vector_element<EnumValue>(source_index, row, (*enumList));
//    emitLayoutChanged();

//    return res;
//}


//bool EnumModel::insertRows(int position, int rows, const QModelIndex &parent) {
//    if (position < 0) position = enumList->size();
//    beginInsertRows(QModelIndex(), position, position+rows-1);
//    for (int row = 0; row < rows; ++row) {
//        EnumValue enumValue;
//        enumValue.Value = (QString("Новое значение ") + QString::number(row)).toStdString();
//        (*enumList).insert((*enumList).begin()+position, enumValue);
//    }
//    endInsertRows();
//    return true;
//}

//bool EnumModel::removeRows(int position, int rows, const QModelIndex &parent) {
//    if (position < 0) return false;
//    if (position+rows > enumList->size()) return false;
//    beginRemoveRows(QModelIndex(), position, position+rows-1);
//    for (int row = 0; row < rows; ++row)
//        (*enumList).erase((*enumList).begin()+row);
//    endRemoveRows();
//    return true;
//}

void ImageModel::createImage() {
    ImageEditor editor;
    editor.DisableCollectionButton();
    int dialogCode = editor.exec();
    if(dialogCode == QDialog::Accepted) {
        emitLayoutAboutToBeChanged();
        Image *image = editor.GetImage();
        if(!global) images.push_back(image->GetId());
        emitLayoutChanged();
    }
}

std::vector<IdType> ImageModel::GetImages() {
    return images;
}

void ImageModel::addImage(const IdType &image_id) {
    emitLayoutAboutToBeChanged();
    if(!global) images.push_back(image_id);
    emitLayoutChanged();
}
