#pragma once
#include <iostream>
#include <sstream>

#include "jansson/jansson.h"
#ifdef JSON_INDENT
#undef JSON_INDENT
#endif
#ifdef JSON_COMPACT
#undef JSON_COMPACT
#endif
#define JSON_INDENT 4
#define JSON_COMPACT 0

bool log_error(const char* str);
bool log_error_parse(const char* str);

inline bool stringToJson(json_t *json, std::string name, std::string str) {
    json_t *json_str = json_stringn_nocheck(str.data(), str.size());
    if (json_object_set(json, name.c_str(), json_str) != 0)
        return log_error(name.c_str());
    return true;
}

bool json_parse_double_lib(const json_t *object, const char * key, double &val);
bool json_parse_array_double(const json_t *object, unsigned int index, double &val);

bool json_parse_int(const json_t *object, const char * key, int &val);
bool json_parse_int(const json_t *object, const char * key, unsigned int &val);
bool json_parse_bool(const json_t *object, const char * key, bool &val);
bool json_parse_string(const json_t *object, const char * key, std::string &val);
