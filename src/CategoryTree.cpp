#include "CategoryTree.hpp"

#include <iostream>
#include <QMimeData>

#include "ModelItem.hpp"

CategoryTree::CategoryTree() {
    setSelectionMode(QAbstractItemView::SingleSelection);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    //setSelectionMode(QAbstractItemView::ContiguousSelection);

    setHeaderHidden(true);
	setAcceptDrops(true);
	setDropIndicatorShown(true);
	setDragEnabled(true);
	viewport()->setAcceptDrops(true);
    setDragDropMode(QAbstractItemView::InternalMove);
    setDragDropMode(QAbstractItemView::DragDrop);
    setDefaultDropAction(Qt::MoveAction);
}

QStringList CategoryTree::mimeTypes() const {
    QStringList types = QTreeWidget::mimeTypes();
    //types << "application/vnd.text.list";
    return types;
}

QMimeData *CategoryTree::mimeData(const QList<QTreeWidgetItem *> items) const {
    QMimeData *mimeData = QTreeWidget::mimeData(items);
    //mimeData->setData("application/vnd.text.list", encodedData);
    bool notOnlyItem = false;
    for(int i = 0; i < items.size(); ++i) {
        ModelItem *item = reinterpret_cast<ModelItem*>(items[i]);
        if (item->Type() != ITEM) {
            notOnlyItem = true;
        }
    }
    mimeData->setProperty("size", QVariant(items.size()));
    mimeData->setProperty("notOnlyItem", QVariant(notOnlyItem));
    if (items.size() == 1)
        mimeData->setProperty("type", items[0]->data(0, Qt::UserRole));
    return mimeData;
}

bool CategoryTree::fromMime(const QMimeData *mime, const QPoint &pos) {
    ModelItem *destination = reinterpret_cast<ModelItem*>(this->itemAt(pos));

    unsigned int selected_items_count = mime->property("size").toInt();
    if (selected_items_count == 1) {
        ModelType source_type = static_cast<ModelType>(mime->property("type").toInt());
        if (destination == 0){
            if (source_type == FOLDER) return true;
            if (source_type == CATEGORY) return true;
        }
        else {
            if (source_type == ITEM && destination->Type() == CATEGORY) return true;
            if (source_type == FOLDER && destination->Type() == FOLDER) return true;
            if (source_type == CATEGORY && destination->Type() == FOLDER) return true;
        }
    }
    if (selected_items_count > 1) {
        if (destination == 0) return false;
        bool notOnlyItem = mime->property("notOnlyItem").toBool();
        if (notOnlyItem) return false;
        if (destination->Type() == CATEGORY) return true;
    }

    return false;
}

void CategoryTree::dragMoveEvent(QDragMoveEvent *event) {
    if (!fromMime(event->mimeData(), event->pos())) event->setDropAction(Qt::IgnoreAction);
    QTreeWidget::dragMoveEvent(event);
}

void CategoryTree::mousePressEvent(QMouseEvent *event) {
    QTreeWidget::mousePressEvent(event);
    QTreeWidgetItem *item = itemAt(event->pos());
    if (item == 0) selectionModel()->clear();
}


void CategoryTree::categoryItemToModelItem(Item *item, ModelItem* root_item){
    root_item->setText(0, QString(item->Name.c_str()));
}

void CategoryTree::folderToModelItem(Folder *folder, ModelItem* root_item){
    root_item->setText(0, QString(folder->Name.c_str()));
    root_item->setExpanded(folder->EditorExpanded);

    for(unsigned int i = 0; i < folder->Folders.size(); ++i) {
        ModelItem *item = new ModelItem(FOLDER);
        root_item->addChild(item);
        folderToModelItem(folder->Folders[i], item);
    }
    for(unsigned int i = 0; i < folder->Categories.size(); ++i) {
        ModelItem *item = new ModelItem(CATEGORY);
        root_item->addChild(item);
        categoryToModelItem(folder->Categories[i], item);
    }
}

void CategoryTree::categoryToModelItem(Category *category, ModelItem* root_item){
    root_item->setText(0, QString(category->Name.c_str()));
    root_item->setExpanded(category->EditorExpanded);

    for(unsigned int i = 0; i < category->Items.size(); ++i) {
        ModelItem *item = new ModelItem(ITEM);
        root_item->addChild(item);
        categoryItemToModelItem(category->Items[i], item);
    }
}

bool CategoryTree::FromDB(Db &db) {
    while(topLevelItemCount() != 0)
        delete topLevelItem(0);

    for(unsigned int i = 0; i < db.Folders.size(); ++i) {
        ModelItem *item = new ModelItem(FOLDER);
        addTopLevelItem(item);
        folderToModelItem(db.Folders[i], item);
    }
    for(unsigned int i = 0; i < db.Categories.size(); ++i) {
        ModelItem *item = new ModelItem(CATEGORY);
        addTopLevelItem(item);
        categoryToModelItem(db.Categories[i], item);
    }
    return true;
}


void CategoryTree::parseItem(ModelItem* root_item, Item *item) {
    item->Name = root_item->text(0).toStdString();
}

void CategoryTree::parseCategory(ModelItem* root_item, Category *category) {
    category->Name = root_item->text(0).toStdString();
    category->EditorExpanded = root_item->isExpanded();

    for (int i = 0; i < root_item->childCount(); ++i) {
        ModelItem *item = reinterpret_cast<ModelItem*>(root_item->child(i));
        if(item->Type() == ITEM) {
            Item *categoryItem = DB.NewItem(category);
            parseItem(item, categoryItem);
            category->Items.push_back(categoryItem);
        }
    }
}

void CategoryTree::parseFolder(ModelItem* root_item, Folder *folder) {
    folder->Name = root_item->text(0).toStdString();
    folder->EditorExpanded = root_item->isExpanded();

    for (int i = 0; i < root_item->childCount(); ++i) {
        ModelItem *item = reinterpret_cast<ModelItem*>(root_item->child(i));
        if(item->Type() == FOLDER) {
            Folder *newfolder = DB.NewFolder(folder);
            parseFolder(item, newfolder);
            folder->Folders.push_back(newfolder);
        }
        if(item->Type() == CATEGORY) {
            Category *category = DB.NewCategory(folder);
            parseCategory(item, category);
            folder->Categories.push_back(category);
        }
    }
}

bool CategoryTree::ToDB(Db &db) {
    for (int i = 0; i < topLevelItemCount(); ++i) {
        ModelItem *item = reinterpret_cast<ModelItem*>(topLevelItem(i));
        if(item->Type() == FOLDER) {
            Folder *newfolder = DB.NewFolder(0);
            parseFolder(item, newfolder);
            db.Folders.push_back(newfolder);
        }
        if(item->Type() == CATEGORY) {
            Category *category = DB.NewCategory(0);
            parseCategory(item, category);
            db.Categories.push_back(category);
        }
    }
    return true;
}
