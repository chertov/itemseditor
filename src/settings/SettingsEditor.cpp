#include "SettingsEditor.hpp"

#include <iostream>
#include <fstream>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QSettings>

#include "collection/db.hpp"
#include "exception.hpp"

SettingsEditor::SettingsEditor(QWidget *parent) : QDialog(parent) {
    ui.setupUi(this);

    //setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);

    QSettings settings;
    QString str = settings.value("ItemsEditor/images_path").toString();
    ui.ImagesPathEdit->setText(str);
}

SettingsEditor::~SettingsEditor() {

}

void SettingsEditor::set_imagespath() {
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setFileMode(QFileDialog::DirectoryOnly);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setNameFilter(QString("Директория"));
    dialog.setWindowTitle("Выбрать директорию для хранения изображений");
    if (!dialog.exec()) return;
    QStringList selectedFiles = dialog.selectedFiles();
    if(selectedFiles.size() != 1) return;
    std::string images_path = selectedFiles[0].toLocal8Bit().toStdString();
    DB.set_images_path(images_path);
}
