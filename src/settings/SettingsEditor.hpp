#pragma once
#include "ui_SettingsEditor.h"

#include <QtWidgets/QDialog>

class SettingsEditor : public QDialog
{
    Q_OBJECT

public:
    SettingsEditor(QWidget *parent = 0);
    ~SettingsEditor();

private:
    Ui::SettingsEditor ui;

public slots:
    void set_imagespath();
};
