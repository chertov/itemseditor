#pragma once

#include <iostream>
#include <sstream>
#include <fstream>
#include <QUuid>

#include "assert.hpp"

inline bool error(const char* str) {
    std::cout << str << std::endl;
    return false;
}

template <typename T>
bool move_vector_element(size_t start, size_t dst, std::vector<T> & v) {
    size_t length = 1;
    if (dst < 0) dst = v.size();
    if (dst > v.size()) dst = v.size();
    if (start < 0) return false;
    if (start >= v.size()) return false;

    typename std::vector<T>::iterator first, middle, last;
    if (start < dst)
    {
        first  = v.begin() + start;
        middle = first + length;
        last   = v.begin() + dst;
    }
    else
    {
        first  = v.begin() + dst;
        middle = v.begin() + start;
        last   = middle + length;
    }
    std::rotate(first, middle, last);
    return true;
}

class TXTSaver : public std::stringstream {
public:
    std::string path;
    TXTSaver(std::string path = "log.txt", std::string name = "");
    ~TXTSaver();
};

std::string getFileName(const std::string& path);

std::string str_trim(const std::string &str);
bool str_start_with(const std::string &str, const std::string &start);
bool str_empty(const std::string &str);
bool str_equal(const std::string &str1, const std::string &str2);

std::vector<std::string> str_split(const std::string& input_str, const std::string& separators, bool remove_empty = true);
std::string ReplaceString(std::string subject, const std::string &search, const std::string &replace);

unsigned long long getTickCount();
