#include "qteditor.hpp"
#include <QtWidgets/QApplication>

#include <QJsonArray>
#include "logger.hpp"
#include "exception.hpp"

// function to call if operator new can't allocate enough memory or error arises
void outOfMemHandler()
{
    std::cerr << "Unable to satisfy request for memory\n";
    std::abort();
}

static void *secure_malloc(size_t size) {
    /* Store the memory area size in the beginning of the block */
    void *ptr = malloc(size);
    if (ptr == 0) {
        LOG << "can't allocate " << size << " bytes";
        ExceptionSaver ss; ss << "secure_malloc";
    }
    return ptr;
}

static void secure_free(void *ptr) {
    free(ptr);
}

int main(int argc, char *argv[])
{
    std::set_new_handler(outOfMemHandler);
    json_set_alloc_funcs(secure_malloc, secure_free);

    QCoreApplication::setOrganizationName("ItemsEditor");
    QCoreApplication::setApplicationName("itemseditor.com");
    QCoreApplication::setApplicationName("Items Editor");

    QApplication a(argc, argv);
    qtEditor w;
    w.show();
    return a.exec();
}
