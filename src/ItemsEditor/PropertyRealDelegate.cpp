#include "PropertyRealDelegate.hpp"

#include <QDoubleSpinBox>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>
#include <QPainter>

#include <iostream>
#include "collection/itemwalker.hpp"

PropertyRealDelegate::PropertyRealDelegate(QObject *parent) : QItemDelegate(parent) { }

QWidget *PropertyRealDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const {
    QDoubleSpinBox* editor = new QDoubleSpinBox(parent);
    editor->setValue(0);
    editor->setDecimals(5);
    editor->setSingleStep(0.1);
    editor->setRange(-10000000000000, 10000000000000);
    return editor;
}

void PropertyRealDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) != QString("PropertyValue")) return;
    PropertyValue propertyValue = var.value<PropertyValue>();

    QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);
    if(propertyValue.PropertyId > 0)
        spinBox->setValue(propertyValue.Real);
    else
        spinBox->setValue(0);
}

void PropertyRealDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
    if(index.column() == 0) return;
    QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox*>(editor);

    PropertyValue newProp;
    newProp.PropertyId = prop_id;
    newProp.Real = spinBox->value();

    model->setData(index, QVariant::fromValue<PropertyValue>(newProp), Qt::EditRole);
}

void PropertyRealDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const {
    editor->setGeometry(option.rect);
}

void PropertyRealDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyleOptionViewItemV4 myOption = option;

    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) == QString("PropertyValue")) {
        PropertyValue propertyValue = var.value<PropertyValue>();
        if(propertyValue.PropertyId == 0) {
            myOption.text = QString("Не задано");
            painter->fillRect(option.rect, QColor(255, 198, 66));
        } else {
            myOption.text = QString::number(propertyValue.Real,'f', 6);
        }
    }
    if(QString(var.typeName()) == QString("WarningValue")) {
        WarningValue warning = var.value<WarningValue>();
        myOption.text = warning.message;
        painter->fillRect(option.rect, warning.color);
    }

    if(QString(var.typeName()) == QString("QString")) myOption.text = var.toString();
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
