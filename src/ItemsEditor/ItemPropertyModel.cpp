#include "ItemPropertyModel.hpp"
#include <algorithm>
#include <QDebug>
#include <QMimeData>
#include <QIcon>
#include "collection/collections.hpp"
#include "collection/itemwalker.hpp"

#include "tools.hpp"

#include "PropertyItemTypeDelegate.hpp"
#include "PropertyProducerDelegate.hpp"

ItemPropertyModel::ItemPropertyModel(QObject *parent) : QAbstractListModel(parent) {

}

ItemPropertyModel::~ItemPropertyModel() { }

int ItemPropertyModel::rowCount(const QModelIndex &) const {
    if (category == 0) return 0;
    return 2 + category->GetPropertiesSize();
}

int ItemPropertyModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return ColumnCount;
}

QModelIndex ItemPropertyModel::index(int row, int column, const QModelIndex &parent) const {
    if (category == 0) return QModelIndex();
    if (!hasIndex(row, column, parent)) return QModelIndex();
    if (column > 0)
        if(items.size() > 0)
            return createIndex(row, column, const_cast<Item*>(items[0]));
        else
            return createIndex(row, column, const_cast<Item*>(item));
    return createIndex(row, column, static_cast<void*>(0));
}

QVariant ItemPropertyModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return QVariant();
    if (category == 0) return QVariant();
    unsigned int row = index.row();
    unsigned int column = index.column();
    if (row >= 2 + category->GetPropertiesSize()) return QVariant();
    if (column >= ColumnCount) return QVariant();

    QVariant nameColumn;
    QVariant valueColumn;
    if(item != 0) {
        if(row == 0) {
            nameColumn = "Тип";
            valueColumn = QVariant::fromValue<ItemTypeValue>(ItemTypeValue(item->GetItemType()));
        }
        if(row == 1) {
            nameColumn = "Производитель";
            valueColumn = QVariant::fromValue<ProducerValue>(ProducerValue(item->GetProducer()));
        }
        if(row > 1) {
            Property *property = category->GetPropertyPtrByIndex(row-2);
            nameColumn = QVariant(QString::fromStdString(property->GetName()));

            PropertyValue propertyValue;
            bool prop_found = item->GetPropertyValue(property->GetId(), propertyValue);
            if(!prop_found) {
                PropertyValue newValue;
                newValue.PropertyId = 0;
                newValue.Enum = 0;
                valueColumn = QVariant::fromValue<PropertyValue>(newValue);
            } else
                valueColumn = QVariant::fromValue<PropertyValue>(propertyValue);
        }
    } else {
        if(items.size() == 0) return QVariant();
        if(row == 0) {
            nameColumn = "Тип";
            ItemTypeWalker walker;
            for(auto item : items) if(walker.Check(item) != ALL_EQUAL) break;
            switch(walker.Result()) {
            case ALL_EQUAL: {
                if(items[0]->GetItemType() == 0) {
                    WarningValue warning;
                    warning.message = "Не указан";
                    valueColumn = QVariant::fromValue<WarningValue>(warning);
                } else
                    valueColumn = QVariant::fromValue<ItemTypeValue>(ItemTypeValue(items[0]->GetItemType()));
                break; }
            case NOT_EQUAL: {
                WarningValue warning;
                warning.message = "Значения разные";
                warning.color = QColor(255,255,255);
                valueColumn = QVariant::fromValue<WarningValue>(warning);
                break; }
            }
        }
        if(row == 1) {
            nameColumn = "Производитель";
            ProducerWalker walker;
            for(auto item : items) if(walker.Check(item) != ALL_EQUAL) break;
            switch(walker.Result()) {
            case ALL_EQUAL:
                if(items[0]->GetProducer() == 0) {
                    WarningValue warning;
                    warning.message = "Не указан";
                    valueColumn = QVariant::fromValue<WarningValue>(warning);
                } else
                    valueColumn = QVariant::fromValue<ProducerValue>(ProducerValue(items[0]->GetProducer()));
                break;
            case NOT_EQUAL: {
                WarningValue warning;
                warning.message = "Значения разные";
                warning.color = QColor(255,255,255);
                valueColumn = QVariant::fromValue<WarningValue>(warning);
                break; }
            }
        }
        if(row > 1) {
            Property *property = category->GetPropertyPtrByIndex(row-2);
            nameColumn = QVariant(QString::fromStdString(property->GetName()));

            PropertyWalker walker(property);
            for(auto item : items) if(walker.Check(item) != ALL_EQUAL) break;

            switch(walker.Result()) {
            case ALL_EQUAL: {
                PropertyValue propertyValue;
                bool prop_found = items[0]->GetPropertyValue(property->GetId(), propertyValue);
                if(!prop_found) {
                    PropertyValue newValue;
                    newValue.PropertyId = 0;
                    newValue.Enum = 0;
                    valueColumn = QVariant::fromValue<PropertyValue>(newValue);
                } else
                    valueColumn = QVariant::fromValue<PropertyValue>(propertyValue);
                break; }
            case NOT_EQUAL: {
                WarningValue warning;
                warning.message = "Значения разные";
                warning.color = QColor(255,255,255);
                valueColumn = QVariant::fromValue<WarningValue>(warning);
                break; }
            case ITEM_INVALID: {
                WarningValue warning;
                warning.message = "Некорректные элементы";
                warning.color = QColor(255,0,0);
                valueColumn = QVariant::fromValue<WarningValue>(warning);
                break; }
            case ENUM_INVALID: {
                WarningValue warning;
                warning.message = "Некорректные значения перечислений";
                warning.color = QColor(255,0,0);
                valueColumn = QVariant::fromValue<WarningValue>(warning);
                break; }
            case PROPERTY_NOT_FOUND: {
                PropertyValue newValue;
                newValue.PropertyId = 0;
                newValue.Enum = 0;
                valueColumn = QVariant::fromValue<PropertyValue>(newValue);
                break; }
            }
        }
    }
    switch (column) {
    case NameColumn:
        switch (role) {
        case Qt::EditRole:
            return nameColumn;
        case Qt::DisplayRole:
            return nameColumn;
        default:
            return QVariant();
        break; }
    case ValueColumn:
        switch (role) {
        case Qt::EditRole:
            return valueColumn;
        case Qt::DisplayRole:
            return valueColumn;
        default:
            return QVariant();
        break; }
    default:
        break;
    }
    return QVariant();
}

bool ItemPropertyModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (!index.isValid()) return false;
    if (category == 0) return false;
    if (index.column() != 1) return false;

    unsigned int row = index.row();
    if (row >= 2 + category->GetPropertiesSize()) return false;
    if (role == Qt::EditRole) {
        if(items.size() == 0) {
            if (item == 0) return false;
            if (row == 0) item->SetItemType(value.toULongLong());
            if (row == 1) item->SetProducer(value.toULongLong());
            if (row > 1) {
                PropertyValue newValue = value.value<PropertyValue>();
                item->SetPropertyValue(newValue.PropertyId, newValue);
            }
        } else {
            if (row == 0) for(Item* item : items) item->SetItemType(value.toULongLong());
            if (row == 1) for(Item* item : items) item->SetProducer(value.toULongLong());
            if (row > 1) for(Item* item : items) {
                PropertyValue newValue = value.value<PropertyValue>();
                item->SetPropertyValue(newValue.PropertyId, newValue);
            }
        }
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

QVariant ItemPropertyModel::headerData(int section, Qt::Orientation orientation, int role) const {
    QStringList headers = {"Параметр", "Значение"};
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < headers.size())
        return headers[section];
    return QVariant();
}

Qt::ItemFlags ItemPropertyModel::flags(const QModelIndex &index) const {
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    if(index.column() == ValueColumn) return flags | Qt::ItemIsEditable;
    return flags;
}

void ItemPropertyModel::SetItem(Item* item) {
    emitLayoutAboutToBeChanged();
    this->item = 0;
    this->category = 0;
    this->items.clear();

    if(item != 0)
        category = static_cast<Category*>(item->GetParent());
    else
        category = 0;
    if(category == 0) return;
    this->item = item;
    one_category = true;
    emitLayoutChanged();
}

void ItemPropertyModel::SetItems(std::vector<Item*> items) {
    emitLayoutAboutToBeChanged();
    item = 0;
    category = 0;
    this->items.clear();

    if(items[0] == 0) return;
    category = static_cast<Category*>(items[0]->GetParent());
    if(category == 0) return;
    one_category = true;
    for (Item* item : items) {
        if (item == 0) return;
        if (category != item->GetParent())
            one_category = false;
    }
    this->items = items;
    emitLayoutChanged();
}
