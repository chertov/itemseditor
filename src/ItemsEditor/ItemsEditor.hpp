#pragma once
#include "ui_ItemsEditor.h"

#include <QDialog>
#include <QTableView>
#include <QItemDelegate>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>

#include <vector>

#include "collection/collections.hpp"

#include "ItemsModel.hpp"
#include "ItemPropertyWidget.hpp"

class SaveButton : public QPushButton {
    Q_OBJECT
public:
    SaveButton(QWidget *parent, unsigned int property_id) : QPushButton(parent) {
        this->property_id = property_id;
        setText("Сохранить для выделенных");
        connect(this, SIGNAL(clicked()), this, SLOT(clicked()));
    }
    ~SaveButton() {}
public slots:
    void clicked() {
        emit save_for_property_id(property_id);
    }
signals:
    void save_for_property_id(const unsigned int &);
private:
    unsigned int property_id;
};

class ItemsEditor : public QDialog
{
    Q_OBJECT

public:
    ItemsEditor(Category *category, QWidget *parent = 0);
    ~ItemsEditor();

private:
    Ui::ItemsEditor ui;
    ItemsModel *items;
    QTableView *itemsView;
    Category *category;

    ItemPropertyWidget *propertyWidget;

public slots:
    void itemsSelectionChanged();
};

