#pragma once

#include <string>
#include <vector>

#include <QItemDelegate>
#include "collection/enum.hpp"

class QModelIndex;
class QWidget;
class QVariant;

class PropertyEnumDelegate : public QItemDelegate
{
Q_OBJECT
public:
    PropertyEnumDelegate(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    bool SetProperty(const IdType &property_id);
private:
    Enum* enum_ptr;
    IdType prop_id;
};
