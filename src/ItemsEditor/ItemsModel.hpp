#pragma once
#include <QAbstractListModel>
#include "collection/collections.hpp"

class ItemsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ItemsModel(Category *category, QObject *parent = 0);
    ~ItemsModel();

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role);
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void emitLayoutAboutToBeChanged() { emit layoutAboutToBeChanged(); }
    void emitLayoutChanged() { emit layoutChanged(); }

    Category *category;

    enum Columns
    {
        RamificationColumn,
        NameColumn = RamificationColumn,
        CodeColumn,
        ArticleColumn,
        TypeColumn,
        ProducerColumn,
        ImageColumn,
        ColumnCount
    };
private:
    QStringList headers;

    QVariant nameData(const Item* item, int role) const;
};
