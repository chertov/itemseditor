#include "ItemsEditor.hpp"

#include <iostream>
#include <fstream>
#include <QtWidgets/QLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QLabel>
#include <QtWidgets/QComboBox>
#include <QModelIndexList>
#include <QSettings>
#include <QTextStream>

#include "collection/collections.hpp"
#include "collection/itemwalker.hpp"

#include "PropertyBoolDelegate.hpp"
#include "PropertyIntDelegate.hpp"
#include "PropertyRealDelegate.hpp"
#include "PropertyEnumDelegate.hpp"
#include "PropertyItemTypeDelegate.hpp"
#include "PropertyProducerDelegate.hpp"
#include "PropertyImageDelegate.hpp"

ItemsEditor::ItemsEditor(Category *cat, QWidget *parent) : QDialog(parent) {
    ui.setupUi(this);

    Qt::WindowFlags flags = windowFlags();
    flags |= Qt::WindowMaximizeButtonHint;
    flags |= Qt::WindowContextHelpButtonHint;
    setWindowFlags( flags );

    category = cat;

    setWindowTitle(windowTitle() + QString(" \"") + QString::fromStdString(category->GetName()) + QString("\""));

    items = new ItemsModel(category);
    itemsView = ui.itemsTableView;
    itemsView->setModel(items);

    propertyWidget = new ItemPropertyWidget(this);
    ui.splitter->addWidget(propertyWidget);
    propertyWidget->SetItem(0);

    QSettings settings;
    QString str = settings.value("ItemsEditor/splitter").toString();
    QTextStream in(&str); in >> *ui.splitter;

    itemsView->setItemDelegateForColumn(ItemsModel::TypeColumn, new PropertyItemTypeDelegate());
    itemsView->setItemDelegateForColumn(ItemsModel::ProducerColumn, new PropertyProducerDelegate());
    itemsView->setItemDelegateForColumn(ItemsModel::ImageColumn, new PropertyImageDelegate());

    for (unsigned int i = 0; i < category->GetPropertiesSize(); ++i){
        Property *property = category->GetPropertyPtrByIndex(i);
        IdType prop_id = property->GetId();
        PropertyType type = property->GetType();
        if(type == PROPERTY_BOOL) {
            PropertyBoolDelegate *delegate = new PropertyBoolDelegate();
            delegate->prop_id = prop_id;
            itemsView->setItemDelegateForColumn(i + ItemsModel::ColumnCount, delegate);
        }
        if(type == PROPERTY_INT) {
            PropertyIntDelegate *delegate = new PropertyIntDelegate();
            delegate->prop_id = prop_id;
            itemsView->setItemDelegateForColumn(i + ItemsModel::ColumnCount, delegate);
        }
        if(type == PROPERTY_REAL) {
            PropertyRealDelegate *delegate = new PropertyRealDelegate();
            delegate->prop_id = prop_id;
            itemsView->setItemDelegateForColumn(i + ItemsModel::ColumnCount, delegate);
        }
        if(type == PROPERTY_ENUM) {
            PropertyEnumDelegate *delegate = new PropertyEnumDelegate();
            delegate->SetProperty(prop_id);
            itemsView->setItemDelegateForColumn(i + ItemsModel::ColumnCount, delegate);
        }
    }
    connect(itemsView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(itemsSelectionChanged()));
    itemsSelectionChanged();
}

ItemsEditor::~ItemsEditor() {

}

void ItemsEditor::itemsSelectionChanged() {
    Q_ASSERT(category != 0);
    QModelIndexList selected_rows = itemsView->selectionModel()->selectedRows();
    ui.countLabel->setText(QString::number(selected_rows.size()));

    if (selected_rows.size() == 0) {
        propertyWidget->setHidden(true);
    } else
        propertyWidget->setHidden(false);

    if (selected_rows.size() == 1) {
        BaseProps* baseProps = static_cast<BaseProps*>(selected_rows[0].internalPointer());
        if(baseProps->Type() == ITEM) {
            propertyWidget->SetItem(static_cast<Item*>(baseProps));
        }
    }
    if (selected_rows.size() > 1) {
        std::vector<Item*> items;
        for(QModelIndex index: selected_rows) {
            BaseProps* baseProps = static_cast<BaseProps*>(index.internalPointer());
            if(baseProps->Type() == ITEM)
                items.push_back(static_cast<Item*>(baseProps));
        }
        propertyWidget->SetItems(items);
    }

    QSettings settings;
    QString str; QTextStream out(&str); out << *ui.splitter;
    settings.setValue("ItemsEditor/splitter", str);
}
