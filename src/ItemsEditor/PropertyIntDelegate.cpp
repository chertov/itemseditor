#include "PropertyIntDelegate.hpp"

#include <QSpinBox>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>
#include <QPainter>
#include <QStyleOptionViewItem>

#include <iostream>
#include "collection/itemwalker.hpp"

PropertyIntDelegate::PropertyIntDelegate(QObject *parent) : QItemDelegate(parent) { }

QWidget *PropertyIntDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const {
    QSpinBox* editor = new QSpinBox(parent);
    editor->setValue(0);
    return editor;
}

void PropertyIntDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) != QString("PropertyValue")) return;
    PropertyValue propertyValue = var.value<PropertyValue>();

    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);
    if(propertyValue.PropertyId > 0)
        spinBox->setValue(propertyValue.Int);
    else
        spinBox->setValue(0);
}

void PropertyIntDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
    if(index.column() == 0) return;
    QSpinBox *spinBox = static_cast<QSpinBox*>(editor);

    PropertyValue newProp;
    newProp.PropertyId = prop_id;
    newProp.Int = spinBox->value();

    model->setData(index, QVariant::fromValue<PropertyValue>(newProp), Qt::EditRole);
}

void PropertyIntDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const {
    editor->setGeometry(option.rect);
}

void PropertyIntDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyleOptionViewItem myOption = option;

    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) == QString("PropertyValue")) {
        PropertyValue propertyValue = var.value<PropertyValue>();
        if(propertyValue.PropertyId == 0) {
            myOption.text = QString("Не задано");
            painter->fillRect(option.rect, QColor(255, 198, 66));
        } else {
            myOption.text = QString::number(propertyValue.Int);
        }
    }
    if(QString(var.typeName()) == QString("WarningValue")) {
        WarningValue warning = var.value<WarningValue>();
        myOption.text = warning.message;
        painter->fillRect(option.rect, warning.color);
    }

    if(QString(var.typeName()) == QString("QString")) myOption.text = var.toString();
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
