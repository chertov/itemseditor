#pragma once
#include "ui_ItemPropertyWidget.h"

#include <QWidget>
#include <QTableView>
#include <QItemDelegate>

#include <vector>

#include "collection/collections.hpp"

#include "ItemPropertyModel.hpp"

class ItemPropertyWidget : public QWidget {
    Q_OBJECT
public:
    ItemPropertyWidget(QWidget *parent = 0);
    ~ItemPropertyWidget();

    void SetItem(Item* item);
    void SetItems(std::vector<Item*> items);

private:
    Ui::ItemPropertyWidget ui;
    ItemPropertyModel *model;
    QTableView *view;

public slots:
};
