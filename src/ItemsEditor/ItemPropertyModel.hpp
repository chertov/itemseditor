#pragma once
#include <QAbstractListModel>
#include "collection/collections.hpp"

class ItemPropertyModel : public QAbstractListModel {
    Q_OBJECT
public:
    explicit ItemPropertyModel(QObject *parent = 0);
    ~ItemPropertyModel();

    void SetItem(Item* item);
    void SetItems(std::vector<Item*> items);
    bool OneCategory() { return one_category; }

    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void emitLayoutAboutToBeChanged() { emit layoutAboutToBeChanged(); }
    void emitLayoutChanged() { emit layoutChanged(); }

private:
    Item *item = 0;
    std::vector<Item*> items;
    Category *category = 0;
    bool one_category;

    enum Columns
    {
        RamificationColumn,
        NameColumn = RamificationColumn,
        ValueColumn,
        ColumnCount
    };

//    QVariant nameData(const Item* item, int role) const;
};
