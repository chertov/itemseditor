#include "ItemPropertyWidget.hpp"

#include <iostream>
#include <fstream>
#include <QtWidgets/QLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QLabel>
#include <QtWidgets/QComboBox>
#include <QModelIndexList>

#include "collection/collections.hpp"

#include "PropertyBoolDelegate.hpp"
#include "PropertyIntDelegate.hpp"
#include "PropertyRealDelegate.hpp"
#include "PropertyEnumDelegate.hpp"
#include "PropertyItemTypeDelegate.hpp"
#include "PropertyProducerDelegate.hpp"
#include "PropertyImageDelegate.hpp"

ItemPropertyWidget::ItemPropertyWidget(QWidget *parent) : QWidget(parent) {
    ui.setupUi(this);

    model = new ItemPropertyModel();
    view = ui.tableView;
    view->setModel(model);
    view->horizontalHeader()->setStretchLastSection(true);
    view->resizeColumnsToContents(); // not necessarily but can make look better

}

ItemPropertyWidget::~ItemPropertyWidget() {

}

void ItemPropertyWidget::SetItem(Item* item) {
    if(item == 0)
        setHidden(true);
    else
        setHidden(false);
    ui.multiCategoryWarning->setHidden(true);
    model->SetItem(item);

    view->setItemDelegateForRow(0, new PropertyItemTypeDelegate());
    view->setItemDelegateForRow(1, new PropertyProducerDelegate());

    if(item != 0) {
        Category *category = static_cast<Category*>(item->GetParent());
        for (unsigned int i = 0; i < category->GetPropertiesSize(); ++i){
            Property *property = category->GetPropertyPtrByIndex(i);
            IdType prop_id = property->GetId();
            PropertyType type = property->GetType();
            if(type == PROPERTY_BOOL) {
                PropertyBoolDelegate *delegate = new PropertyBoolDelegate();
                delegate->prop_id = prop_id;
                view->setItemDelegateForRow(i+2, delegate);
            }
            if(type == PROPERTY_INT) {
                PropertyIntDelegate *delegate = new PropertyIntDelegate();
                delegate->prop_id = prop_id;
                view->setItemDelegateForRow(i+2, delegate);
            }
            if(type == PROPERTY_REAL) {
                PropertyRealDelegate *delegate = new PropertyRealDelegate();
                delegate->prop_id = prop_id;
                view->setItemDelegateForRow(i+2, delegate);
            }
            if(type == PROPERTY_ENUM) {
                PropertyEnumDelegate *delegate = new PropertyEnumDelegate();
                delegate->SetProperty(prop_id);
                view->setItemDelegateForRow(i+2, delegate);
            }
        }
    }
}

void ItemPropertyWidget::SetItems(std::vector<Item*> items) {
    if(items.size() == 0)
        setHidden(true);
    else {
        setHidden(false);
        model->SetItems(items);
        ui.multiCategoryWarning->setHidden(model->OneCategory());
    }
}
