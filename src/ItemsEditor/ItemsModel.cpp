#include "ItemsModel.hpp"
#include <algorithm>
#include <QDebug>
#include <QMimeData>
#include <QIcon>
#include "collection/collections.hpp"

#include "tools.hpp"
#include "PropertyItemTypeDelegate.hpp"
#include "PropertyProducerDelegate.hpp"

ItemsModel::ItemsModel(Category *category, QObject *parent) : QAbstractListModel(parent) {
    this->category = category;
    headers = QStringList({"Название", "Заказной номер", "Артикул", "Тип", "Производитель", "Изображение"});
    for (unsigned int i = 0; i < category->GetPropertiesSize(); ++i)
        headers.append(QString::fromStdString(category->GetPropertyPtrByIndex(i)->GetName()));
}

ItemsModel::~ItemsModel() { }

int ItemsModel::rowCount(const QModelIndex &) const {
    if(category == 0) return 0;
    return category->ChildrenSize();
}

QModelIndex ItemsModel::index(int row, int column, const QModelIndex &parent) const {
    if (category == 0) return QModelIndex();
    if (!hasIndex(row, column, parent)) return QModelIndex();
    if (row < 0) return QModelIndex();
    if (row >= category->ChildrenSize()) return QModelIndex();
    return createIndex(row, column, category->Children(row)->ToItem());
}

int ItemsModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return ColumnCount + category->GetPropertiesSize();
}

QVariant ItemsModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return QVariant();
    if (category == 0) return QVariant();
    if (index.row() >= category->ChildrenSize()) return QVariant();

    const Item* item = category->Children(index.row())->ToItem();

    unsigned int column = index.column();
    if(column < ColumnCount)
        switch (index.column()) {
        case NameColumn:
            switch (role) {
            case Qt::EditRole:
                return QVariant(QString::fromStdString(item->GetName()));
            case Qt::DisplayRole:
                return QVariant(QString::fromStdString(item->GetName()));
            default:
                return QVariant();
            }
            break;
        case ArticleColumn:
            switch (role) {
            case Qt::EditRole:
                return QVariant(QString::fromStdString(item->GetArticle()));
            case Qt::DisplayRole:
                return QVariant(QString::fromStdString(item->GetArticle()));
            default:
                return QVariant();
            }
            break;
        case CodeColumn:
            switch (role) {
            case Qt::EditRole:
                return QVariant(QString::fromStdString(item->GetCode()));
            case Qt::DisplayRole:
                return QVariant(QString::fromStdString(item->GetCode()));
            default:
                return QVariant();
            }
            break;
        case TypeColumn:
            switch (role) {
            case Qt::EditRole:
                return QVariant::fromValue<ItemTypeValue>(ItemTypeValue(item->GetItemType()));
            case Qt::DisplayRole:
                return QVariant::fromValue<ItemTypeValue>(ItemTypeValue(item->GetItemType()));
            default:
                return QVariant();
            }
            break;
        case ProducerColumn:
            switch (role) {
            case Qt::EditRole:
                return QVariant::fromValue<ProducerValue>(ProducerValue(item->GetProducer()));
            case Qt::DisplayRole:
                return QVariant::fromValue<ProducerValue>(ProducerValue(item->GetProducer()));
            default:
                return QVariant();
            }
            break;
        case ImageColumn:
            switch (role) {
//            case Qt::EditRole:
//                return QVariant(QString::fromStdString(item->Image));
//            case Qt::DisplayRole:
//                return QVariant(QString::fromStdString(item->Image));
            default:
                return QVariant();
            }
            break;
        default:
            break;
        }
    else {
        unsigned int property_id = column - ColumnCount;
        if (property_id >= category->GetPropertiesSize()) return QVariant();
        Property *property = category->GetPropertyPtrByIndex(property_id);
        PropertyValue propertyValue;
        bool prop_found = item->GetPropertyValue(property->GetId(), propertyValue);
        if(!prop_found) {
            PropertyValue newValue;
            newValue.PropertyId = 0;
            newValue.Enum = 0;
            return QVariant::fromValue<PropertyValue>(newValue);
        }
        return QVariant::fromValue<PropertyValue>(propertyValue);
    }
    return QVariant();
}

bool ItemsModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    if (!index.isValid()) return false;
    if (category == 0) return false;
    if (role == Qt::EditRole) {
        if (index.row() >= category->ChildrenSize()) return false;
        Item* item = category->Children(index.row())->ToItem();
        unsigned int column = index.column();
        if(column < ColumnCount) {
            switch (column) {
            case NameColumn: item->SetName(value.toString().toStdString()); break;
            case ArticleColumn: {
                item->SetArticle(value.toString().toStdString());
                emit headerDataChanged(Qt::Vertical, index.row(), index.row());
                break; }
            case CodeColumn: item->SetCode(value.toString().toStdString()); break;
            case TypeColumn: item->SetItemType(value.toULongLong()); break;
            case ProducerColumn: item->SetProducer(value.toULongLong()); break;
//            case ImageColumn: item->Image = value.toString().toStdString(); break;
            default:
                break;
            }
            emit dataChanged(index, index);
            return true;
        } else {
            PropertyValue newValue = value.value<PropertyValue>();
            PropertyValue propertyValue;
            bool prop_found = item->GetPropertyValue(newValue.PropertyId, propertyValue);
            if(!prop_found) {
                PropertyValueList properties = item->GetProperties();
                properties.push_back(newValue);
                item->SetProperties(properties);
            } else
                item->SetPropertyValue(newValue.PropertyId, newValue);
            emit dataChanged(index, index);
            return true;
        }
    }
    return false;
}

QVariant ItemsModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < headers.size())
        return headers[section];

    if (orientation == Qt::Vertical)
        if(role == Qt::DisplayRole || role == Qt::EditRole)
            if(section >= 0 && section < category->ChildrenSize())
                return QString::fromStdString(category->Children(section)->ToItem()->GetArticle());
    return QVariant();
}

bool ItemsModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role) {
    if (orientation == Qt::Vertical && role == Qt::EditRole)
        if(section >= 0 && section < category->ChildrenSize()) {
            category->Children(section)->ToItem()->SetArticle(value.toString().toStdString());
            emit dataChanged(index(section, ArticleColumn), index(section, ArticleColumn));
            emit headerDataChanged(Qt::Vertical, section, section);
            return true;
        }
    return false;
}

Qt::ItemFlags ItemsModel::flags(const QModelIndex &index) const {
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    return flags | Qt::ItemIsEditable; // | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;
}
