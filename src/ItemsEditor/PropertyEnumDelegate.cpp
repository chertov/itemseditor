#include "PropertyEnumDelegate.hpp"

#include <QComboBox>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>
#include <QPainter>

#include <iostream>
#include "collection/itemwalker.hpp"
#include "collection/db.hpp"

PropertyEnumDelegate::PropertyEnumDelegate(QObject *parent) : QItemDelegate(parent) {
}

bool PropertyEnumDelegate::SetProperty(const IdType &property_id) {
    prop_id = property_id;
    Property* prop = DB.PropertyById(property_id);
    enum_ptr = prop->GetEnumPtr();
}

QWidget *PropertyEnumDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const {
    QComboBox* editor = new QComboBox(parent);
    for (EnumValue enumValue : enum_ptr->GetValues())
        editor->addItem(QString::fromStdString(enumValue.GetValue()));
    return editor;
}

void PropertyEnumDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) != QString("PropertyValue")) return;
    PropertyValue propertyValue = var.value<PropertyValue>();

    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    EnumValueList enumvalues = enum_ptr->GetValues();
    for(unsigned int i = 0; i < enumvalues.size(); ++i)
        if (propertyValue.Enum == enumvalues[i].GetId()) {
            comboBox->setCurrentIndex(i);
            break;
        }
}

void PropertyEnumDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
    if(index.column() == 0) return;
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    unsigned int idx = comboBox->currentIndex();
    EnumValueList enumValues = enum_ptr->GetValues();
    if (idx >= enumValues.size()) return;

    PropertyValue newProp;
    newProp.PropertyId = prop_id;
    newProp.Enum = enumValues[idx].GetId();

    model->setData(index, QVariant::fromValue<PropertyValue>(newProp), Qt::EditRole);
}

void PropertyEnumDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const {
    editor->setGeometry(option.rect);
}

void PropertyEnumDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyleOptionViewItemV4 myOption = option;

    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) == QString("PropertyValue")) {
        PropertyValue propertyValue = var.value<PropertyValue>();
        if(propertyValue.PropertyId == 0) {
            myOption.text = QString("Не задано");
            painter->fillRect(option.rect, QColor(255, 198, 66));
        } else {
            myOption.text = QString::fromStdString(enum_ptr->GetValue(propertyValue.Enum));
        }
    }
    if(QString(var.typeName()) == QString("WarningValue")) {
        WarningValue warning = var.value<WarningValue>();
        myOption.text = warning.message;
        painter->fillRect(option.rect, warning.color);
    }

    if(QString(var.typeName()) == QString("QString")) myOption.text = var.toString();
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
