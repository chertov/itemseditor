#include "PropertyItemTypeDelegate.hpp"

#include <QComboBox>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>
#include <QPainter>

#include <iostream>
#include "collection/collections.hpp"
#include "collection/itemwalker.hpp"

PropertyItemTypeDelegate::PropertyItemTypeDelegate(QObject *parent) : QItemDelegate(parent) {
}

QWidget *PropertyItemTypeDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const {
    QComboBox* editor = new QComboBox(parent);
    for(unsigned int i = 0; i < DB.ItemsTypesSize(); ++i) {
        ItemType *itemType = DB.ItemTypeByIndex(i);
        editor->addItem(QString::fromStdString(itemType->GetName()), QVariant(itemType->GetId()));
    }
    return editor;
}

void PropertyItemTypeDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) != QString("ItemTypeValue")) return;
    ItemTypeValue itemTypeValue = var.value<ItemTypeValue>();

    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    if(itemTypeValue.Id > 0)
        for (unsigned int i = 0; i < comboBox->count(); ++i) {
            if(comboBox->itemData(i).toULongLong() == itemTypeValue.Id)
                comboBox->setCurrentIndex(i);
        }
}

void PropertyItemTypeDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    QString itemTypeID = comboBox->itemData(comboBox->currentIndex()).toString();

    model->setData(index, QVariant(itemTypeID), Qt::EditRole);
}

void PropertyItemTypeDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const {
    editor->setGeometry(option.rect);
}

void PropertyItemTypeDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyleOptionViewItemV4 myOption = option;
    QVariant var = index.model()->data(index, Qt::EditRole);

    if(QString(var.typeName()) == QString("ItemTypeValue")) {
        ItemTypeValue itemTypeValue = var.value<ItemTypeValue>();
        if(itemTypeValue.Id == 0) {
            myOption.text = QString("Не указан");
            painter->fillRect(option.rect, QColor(255, 198, 66));
        } else {
            ItemType *itemType = DB.ItemTypeById(itemTypeValue.Id);
            if(itemType != 0)
                myOption.text = QString::fromStdString(itemType->GetName());
            else{
                myOption.text = QString("Некорректное значение");
                painter->fillRect(option.rect, QColor(255, 66, 66));
            }
        }
    }
    if(QString(var.typeName()) == QString("WarningValue")) {
        WarningValue warning = var.value<WarningValue>();
        myOption.text = warning.message;
        painter->fillRect(option.rect, warning.color);
    }

    if(QString(var.typeName()) == QString("QString")) myOption.text = var.toString();
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
