#include "PropertyImageDelegate.hpp"

#include <QComboBox>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>
#include <QPainter>

#include <iostream>
#include "collection/collections.hpp"

PropertyImageDelegate::PropertyImageDelegate(QObject *parent) : QItemDelegate(parent) {
}

QWidget *PropertyImageDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const {
//    QComboBox* editor = new QComboBox(parent);
//    for(unsigned int i = 0; i < DB.ProducersSize(); ++i) {
//        Producer *producer = DB.ProducerByIndex(i);
//        editor->addItem(QString::fromStdString(producer->Name), QVariant(QString::fromStdString(producer->Id)));
//    }
//    return editor;

    return parent;
}

void PropertyImageDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
//    QComboBox *comboBox = static_cast<QComboBox*>(editor);

//    QString producerID = index.model()->data(index, Qt::EditRole).toString();
//    if(producerID.size() > 0) {
//        for (unsigned int i = 0; i < comboBox->count(); ++i) {
//            if(comboBox->itemData(i).toString() == producerID)
//                comboBox->setCurrentIndex(i);
//        }
//    }
}

void PropertyImageDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
//    QComboBox *comboBox = static_cast<QComboBox*>(editor);
//    QString producerID = comboBox->itemData(comboBox->currentIndex()).toString();

//    model->setData(index, QVariant(producerID), Qt::EditRole);
}

void PropertyImageDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const {
    editor->setGeometry(option.rect);
}

void PropertyImageDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyleOptionViewItemV4 myOption = option;

    Item* item = static_cast<Item*>(index.internalPointer());
    if(item->GetImages().size() == 0) {
        myOption.text = QString("Не задано");
        painter->fillRect(option.rect, QColor(255, 198, 66));
    } else {
        QImage image("rele.jpg");
        image = image.scaled(option.rect.size(), Qt::KeepAspectRatio);
        painter->drawImage(option.rect.x(), option.rect.y(), image);

////        Producer *producer = DB.ProducerById(item->Producer);
////        if(producer != 0)
////            myOption.text = QString::fromStdString(producer->Name);
////        else{
////            myOption.text = QString("Некорректный тип товара");
////            painter->fillRect(option.rect, QColor(255, 66, 66));
////        }
    }

    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
