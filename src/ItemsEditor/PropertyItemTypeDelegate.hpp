#pragma once

#include <string>
#include <vector>

#include "collection/idtype.hpp"
#include <QItemDelegate>

class QModelIndex;
class QWidget;
class QVariant;

class ItemTypeValue {
public:
    ItemTypeValue(const IdType id = 0) { Id = id; }
    IdType Id = 0;
};
Q_DECLARE_METATYPE(ItemTypeValue)

class PropertyItemTypeDelegate : public QItemDelegate
{
Q_OBJECT
public:
    PropertyItemTypeDelegate(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
private:

};
