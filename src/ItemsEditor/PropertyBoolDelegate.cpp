#include "PropertyBoolDelegate.hpp"

#include <QComboBox>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>
#include <QPainter>
#include <QDebug>

#include <iostream>
#include "collection/itemwalker.hpp"

PropertyBoolDelegate::PropertyBoolDelegate(QObject *parent) : QItemDelegate(parent) { }

QWidget *PropertyBoolDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const {
    QComboBox* editor = new QComboBox(parent);
    editor->addItem("Да");
    editor->addItem("Нет");
    return editor;
}

void PropertyBoolDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) != QString("PropertyValue")) return;
    PropertyValue propertyValue = var.value<PropertyValue>();

    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    if(propertyValue.Bool)
        comboBox->setCurrentIndex(0);
    else
        comboBox->setCurrentIndex(1);
}

void PropertyBoolDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
    if(index.column() == 0) return;
    QComboBox *comboBox = static_cast<QComboBox*>(editor);

    PropertyValue newProp;
    newProp.PropertyId = prop_id;
    if(comboBox->currentIndex() == 0) newProp.Bool = true;
    if(comboBox->currentIndex() == 1) newProp.Bool = false;

    model->setData(index, QVariant::fromValue<PropertyValue>(newProp), Qt::EditRole);
}

void PropertyBoolDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const {
    editor->setGeometry(option.rect);
}

void PropertyBoolDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyleOptionViewItemV4 myOption = option;

    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) == QString("PropertyValue")) {
        PropertyValue propertyValue = var.value<PropertyValue>();
        if(propertyValue.PropertyId == 0) {
            myOption.text = QString("Не задано");
            painter->fillRect(option.rect, QColor(255, 198, 66));
        } else {
            if (propertyValue.Bool)
                myOption.text = "Да";
            else
                myOption.text = "Нет";
        }
    }
    if(QString(var.typeName()) == QString("WarningValue")) {
        WarningValue warning = var.value<WarningValue>();
        myOption.text = warning.message;
        painter->fillRect(option.rect, warning.color);
    }

    if(QString(var.typeName()) == QString("QString")) myOption.text = var.toString();
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
