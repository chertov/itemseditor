#pragma once

#include <string>
#include <vector>

#include <QItemDelegate>
#include "collection/idtype.hpp"

class QModelIndex;
class QWidget;
class QVariant;

class PropertyRealDelegate : public QItemDelegate
{
Q_OBJECT
public:
    PropertyRealDelegate(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    IdType prop_id;
};
