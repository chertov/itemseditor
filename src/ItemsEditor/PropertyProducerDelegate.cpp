#include "PropertyProducerDelegate.hpp"

#include <QComboBox>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>
#include <QPainter>

#include <iostream>
#include "collection/collections.hpp"
#include "collection/itemwalker.hpp"

PropertyProducerDelegate::PropertyProducerDelegate(QObject *parent) : QItemDelegate(parent) {
}

QWidget *PropertyProducerDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &/* option */, const QModelIndex &/* index */) const {
    QComboBox* editor = new QComboBox(parent);
    for(unsigned int i = 0; i < DB.ProducersSize(); ++i) {
        Producer *producer = DB.ProducerByIndex(i);
        editor->addItem(QString::fromStdString(producer->GetName()), QVariant(producer->GetId()));
    }
    return editor;
}

void PropertyProducerDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const {
    QVariant var = index.model()->data(index, Qt::EditRole);
    if(QString(var.typeName()) != QString("ProducerValue")) return;
    ProducerValue producer = var.value<ProducerValue>();

    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    if(producer.Id > 0)
        for (unsigned int i = 0; i < comboBox->count(); ++i) {
            if(comboBox->itemData(i).toULongLong() == producer.Id)
                comboBox->setCurrentIndex(i);
        }
}

void PropertyProducerDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const {
    QComboBox *comboBox = static_cast<QComboBox*>(editor);
    QString producerID = comboBox->itemData(comboBox->currentIndex()).toString();

    model->setData(index, QVariant(producerID), Qt::EditRole);
}

void PropertyProducerDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &/* index */) const {
    editor->setGeometry(option.rect);
}

void PropertyProducerDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyleOptionViewItemV4 myOption = option;

    QVariant var = index.model()->data(index, Qt::EditRole);

    if(QString(var.typeName()) == QString("ProducerValue")) {
        ProducerValue producerValue = var.value<ProducerValue>();
        if(producerValue.Id == 0) {
            myOption.text = QString("Не указан");
            painter->fillRect(option.rect, QColor(255, 198, 66));
        } else {
            Producer *producer = DB.ProducerById(producerValue.Id);
            if(producer != 0)
                myOption.text = QString::fromStdString(producer->GetName());
            else{
                myOption.text = QString("Некорректное значение");
                painter->fillRect(option.rect, QColor(255, 66, 66));
            }
        }
    }
    if(QString(var.typeName()) == QString("WarningValue")) {
        WarningValue warning = var.value<WarningValue>();
        myOption.text = warning.message;
        painter->fillRect(option.rect, warning.color);
    }

    if(QString(var.typeName()) == QString("QString")) myOption.text = var.toString();
    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &myOption, painter);
}
