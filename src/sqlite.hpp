#pragma once
#include <string>
#include <vector>

#include "sqlite/sqlite3.h"
#include "collection/idtype.hpp"

bool writeBlob(
  sqlite3 *db,                  /* Database to insert data into */
  const std::string &table,
  const IdType &key,            /* Null-terminated key string */
  const char *data,             /* Pointer to blob of data */
  unsigned int size             /* Length of data pointed to by zBlob */
);
bool readBlob(sqlite3 *db, const std::string &table, std::vector<std::string> &strs);

bool setKey(
  sqlite3 *db,                  /* Database to insert data into */
  const std::string &key,            /* Null-terminated key string */
  const char *data,             /* Pointer to blob of data */
  unsigned int size             /* Length of data pointed to by zBlob */
);
bool getKey(sqlite3 *db, const std::string &key, std::string &str);


