#include "qteditor.hpp"

#include <iostream>
#include <fstream>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QAction>
#include <QMessageBox>
#include <QDateTime>
#include <QFileDialog>
#include <QSettings>

#include "ModelItem.hpp"
#include "collection/collections.hpp"

#include "PropertyEditor/PropertyEditor.hpp"
#include "ItemsEditor/ItemsEditor.hpp"
#include "DescriptionEditor/DescriptionEditor.hpp"
#include "ProducerEditor/ProducerEditor.hpp"
#include "ItemsTypesEditor/ItemsTypesEditor.hpp"
#include "ImageEditor/ImagesList.hpp"
#include "settings/SettingsEditor.hpp"

qtEditor::qtEditor(QWidget *parent) : QMainWindow(parent) {
    ui.setupUi(this);

    itemsTree = new ItemsTree(this);
    ui.forTreeView->addWidget(itemsTree);

    setWindowIcon(QIcon("app.png"));

    QAction *itemsTypesMenu = ui.menuBar->addAction("Типы товаров");
    connect(itemsTypesMenu, SIGNAL(triggered(bool)), this, SLOT(editItemsTypes()));
    QAction *producersMenu = ui.menuBar->addAction("Производители");
    connect(producersMenu, SIGNAL(triggered(bool)), this, SLOT(editProducers()));
    QAction *imagesMenu = ui.menuBar->addAction("Изображения");
    connect(imagesMenu, SIGNAL(triggered(bool)), this, SLOT(imagesListShow()));
    QAction *settingsMenu = ui.menuBar->addAction("Настройки");
    connect(settingsMenu, SIGNAL(triggered(bool)), this, SLOT(settings()));
    QAction *importMenu = ui.menuBar->addAction("Помощь");
    connect(importMenu, SIGNAL(triggered(bool)), this, SLOT(cancelButton()));

//    QMenu *exportMenu = ui.menuBar->addMenu("Экспорт");
//    QAction *fromJsonMenu = exportMenu->addAction("Открыть Json");
//    connect(fromJsonMenu, SIGNAL(triggered(bool)), this, SLOT(FromJson()));
//    QAction *toJsonMenu = exportMenu->addAction("Сохранить Json");
//    connect(toJsonMenu, SIGNAL(triggered(bool)), this, SLOT(ToJson()));

//    QAction *fromSQLiteMenu = exportMenu->addAction("Открыть SQLite");
//    connect(fromSQLiteMenu, SIGNAL(triggered(bool)), this, SLOT(FromSQLite()));
//    QAction *toSQLiteMenu = exportMenu->addAction("Сохранить SQLite");
//    connect(toSQLiteMenu, SIGNAL(triggered(bool)), this, SLOT(ToSQLite()));

//    QAction *fromProtoBuf = exportMenu->addAction("Открыть ProtoBuf");
//    connect(fromProtoBuf, SIGNAL(triggered(bool)), this, SLOT(FromProtoBuf()));
//    QAction *toProtoBuf = exportMenu->addAction("Сохранить ProtoBuf");
//    connect(toProtoBuf, SIGNAL(triggered(bool)), this, SLOT(ToProtoBuf()));

    QVBoxLayout *vBox = static_cast<QVBoxLayout*>(ui.groupBox->layout());
    imagePreview = new ImagePreview(this);
    vBox->insertWidget(vBox->count()-1, imagePreview);
    propertySetEditor = new PropertySetEditor(this);
    vBox->insertWidget(vBox->count()-1, propertySetEditor);
    propertyWidget = new ItemPropertyWidget(this);
    vBox->insertWidget(vBox->count()-1, propertyWidget);

    connect(itemsTree, SIGNAL(selectionChanged(std::vector<BaseProps*>)), this, SLOT(selectionChanged(std::vector<BaseProps*>)));
    selectionChanged(itemsTree->GetSelection());
}

qtEditor::~qtEditor() { }

void qtEditor::settings() {
    SettingsEditor settingsEditor(this);
    int dialogCode = settingsEditor.exec();
    if(dialogCode == QDialog::Accepted) {
    }
}

void qtEditor::selectionChanged(std::vector<BaseProps*> items) {
    ui.groupBox->setHidden(true);
    if(items.size() > 0)
        ui.groupBox->setHidden(false);
    propertySetEditor->setHidden(true);
    propertySetEditor->setEnabled(false);
    propertySetEditor->SetCategory(0);
    imagePreview->SetBaseProps(0);
    propertyWidget->SetItem(0);

    if (items.size() == 1) {
        BaseProps* baseProps = items[0];
        imagePreview->SetBaseProps(baseProps);
        ui.showOnSitecheckBox->setChecked(baseProps->GetShowOnSite());

        ui.NameEdit->setText(QString::fromStdString(baseProps->GetName()));

        if(baseProps->Type() == CATEGORY) {
            propertySetEditor->setEnabled(true);
            propertySetEditor->setHidden(false);
            propertySetEditor->SetCategory(static_cast<Category*>(baseProps));
        }
        if(baseProps->Type() == ITEM) {
            propertyWidget->SetItem(static_cast<Item*>(baseProps));
        }
    } else {
        propertyWidget->setEnabled(true);
        std::vector<Item*> selection_items;
        for(BaseProps* baseProps: items) {
            if(baseProps->Type() == ITEM)
                selection_items.push_back(static_cast<Item*>(baseProps));
        }
        propertyWidget->SetItems(selection_items);
    }
}

void qtEditor::updateItem() {
    std::vector<BaseProps*> indexies = itemsTree->GetSelection();
    if (indexies.size() == 1) {
        BaseProps* parent = indexies[0];
        itemsTree->emitLayoutAboutToBeChanged();
        parent->SetName(ui.NameEdit->text().toStdString());
        parent->SetShowOnSite(ui.showOnSitecheckBox->checkState() == Qt::Checked);
        itemsTree->emitLayoutChanged();
    }
}

QString qtEditor::dateFileName() {
    QDateTime datetime = QDateTime::currentDateTime();

    QDate date = datetime.date();
    int day = date.day();
    QString day_str = QString::number(day);
    if (day < 10) day_str = QString("0") + day_str;
    int month = date.month();
    QString month_str = QString::number(month);
    if (month < 10) month_str = QString("0") + month_str;
    int year = date.year();
    QString year_str = QString::number(year);
    if (year < 10) year_str = QString("0") + year_str;

    QTime time = datetime.time();
    int hour = time.hour();
    QString hour_str = QString::number(hour);
    if (hour < 10) hour_str = QString("0") + hour_str;
    int minute = time.minute();
    QString minute_str = QString::number(minute);
    if (minute < 10) minute_str = QString("0") + minute_str;

    return day_str + QString(".") + month_str + QString(".") + year_str + QString("_") +
            hour_str + QString(".") + minute_str;
}

void qtEditor::cancelButton() {
    //DB.printStat();
    QMessageBox msgBox;
    msgBox.setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    msgBox.setText("Бог в помощь.");
    msgBox.setWindowTitle(QString(" "));
    msgBox.exec();
}

void qtEditor::editDescription() {
    std::vector<BaseProps*> indexies = itemsTree->GetSelection();
    if (indexies.size() == 1) {
        BaseProps* props = indexies[0];
        DescriptionEditor editor(props);
        int dialogCode = editor.exec();
        if(dialogCode == QDialog::Accepted) {
            itemsTree->emitLayoutAboutToBeChanged();
            if(editor.FullDescriptionValid()) props->SetDescription(editor.FullDescription().toStdString());
            if(editor.ShortDescriptionValid()) props->SetShortDescription(editor.ShortDescription().toStdString());
            itemsTree->emitLayoutChanged();
        }
    }
}

void qtEditor::editItemsTypes() {
    ItemsTypesEditor editor;
    editor.exec();
}

void qtEditor::editProducers() {
    ProducerEditor editor;
    editor.exec();
}

void qtEditor::imagesListShow() {
    ImagesList editor;
    editor.exec();
}



void qtEditor::save() {
    ToProtoBuf();
}

void qtEditor::open() {
    FromProtoBuf();
}

std::string qtEditor::OpenDialog(const QString &ext) {
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setNameFilter(QString("Файл данных (*.") + ext + QString(")"));
    dialog.setWindowTitle("Открыть базу данных");
    QSettings settings;
    QString path = settings.value("ItemsEditor/save_path").toString();
    QDir dir(path);
    if(dir.exists())
        dialog.setDirectory(path);
    else
        dialog.setDirectory(QDir::homePath());
    if (!dialog.exec()) return std::string();
    settings.setValue("ItemsEditor/save_path", dialog.directory().absolutePath());
    QStringList selectedFiles = dialog.selectedFiles();
    if(selectedFiles.size() != 1) return std::string();
    return selectedFiles[0].toLocal8Bit().toStdString();
}
QStringList qtEditor::SaveDialog(const QString &ext) {
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilter(QString("Файл данных (*.") + ext + QString(")"));
    dialog.setWindowTitle("Сохранить базу данных");
    QSettings settings;
    QString path = settings.value("ItemsEditor/save_path").toString();
    QDir dir(path);
    if(dir.exists())
        dialog.setDirectory(path);
    else
        dialog.setDirectory(QDir::homePath());
    QString filename = dateFileName() + QString(".") + ext;
    dialog.selectFile(filename);
    if (!dialog.exec()) return QStringList();
    settings.setValue("ItemsEditor/save_path", dialog.directory().absolutePath());
    return dialog.selectedFiles();
}

void qtEditor::ToJson() {
    QStringList selectedFiles = SaveDialog("json");
    if(selectedFiles.size() != 1) return;
    std::string fileName = selectedFiles[0].toLocal8Bit().toStdString();
    if(!DB.ToJSON(fileName)) {
        QMessageBox msgBox;
        msgBox.setText("Не удалось сохранить базу данных");
        msgBox.exec();
    }
}
void qtEditor::FromJson() {
    std::string filename = OpenDialog("json");
    if(filename.size() == 0) return;
    if(!DB.FromJSON(filename)) {
        QMessageBox msgBox;
        msgBox.setText("Не удалось прочитать базу данных");
        msgBox.exec();
    }
    itemsTree->reset();
}

void qtEditor::ToSQLite() {
    QStringList selectedFiles = SaveDialog("db");
    if(selectedFiles.size() != 1) return;
    std::string fileName = selectedFiles[0].toLocal8Bit().toStdString();
    if(!DB.ToSQLite(fileName)) {
        QMessageBox msgBox;
        msgBox.setText("Не удалось сохранить базу данных");
        msgBox.exec();
    }
}
void qtEditor::FromSQLite() {
    std::string filename = OpenDialog("db");
    if(filename.size() == 0) return;
    if(!DB.FromSQLite(filename)) {
        QMessageBox msgBox;
        msgBox.setText("Не удалось прочитать базу данных");
        msgBox.exec();
    }
    itemsTree->reset();
}

void qtEditor::ToProtoBuf() {
    QStringList selectedFiles = SaveDialog("pbf");
    if(selectedFiles.size() != 1) return;
    std::string fileName = selectedFiles[0].toLocal8Bit().toStdString();
    if(!DB.ToProtocolBuffers(fileName)) {
        QMessageBox msgBox;
        msgBox.setText("Не удалось сохранить базу данных");
        msgBox.exec();
    }
}
void qtEditor::FromProtoBuf() {
    std::string filename = OpenDialog("pbf");
    if(filename.size() == 0) return;
    if(!DB.FromProtocolBuffers(filename)) {
        QMessageBox msgBox;
        msgBox.setText("Не удалось прочитать базу данных");
        msgBox.exec();
    }
    itemsTree->reset();
}
