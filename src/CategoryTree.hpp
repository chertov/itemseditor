#pragma once

#include <QTreeWidget>
#include <QDropEvent>
#include <QDragEnterEvent>

#include "collection/collections.hpp"
#include "collection/db.hpp"
#include "ModelItem.hpp"

class CategoryTree : public QTreeWidget {
public:
    CategoryTree();

    virtual QStringList mimeTypes() const;
    virtual QMimeData *mimeData(const QList<QTreeWidgetItem *> items) const;
    virtual void dragMoveEvent(QDragMoveEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    bool fromMime(const QMimeData *mime, const QPoint &pos);

    bool FromDB(Db &db);
    bool ToDB(Db &db);
private:

    void categoryItemToModelItem(Item *item, ModelItem* root_item);
    void folderToModelItem(Folder *folder, ModelItem* root_item);
    void categoryToModelItem(Category *category, ModelItem* root_item);

    void parseItem(ModelItem* root_item, Item *item);
    void parseCategory(ModelItem* root_item, Category *category);
    void parseFolder(ModelItem* root_item, Folder *folder);
};
