#include "ProducerEditor.hpp"

#include <iostream>

ProducerEditor::ProducerEditor(QWidget *parent) : QDialog(parent) {
    ui.setupUi(this);

    model = new ProducerModel();
    view = ui.producerView;
    view->setModel(model);
    connect(view->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)),
            this, SLOT(selectionChanged()));
    selectionChanged();
}

ProducerEditor::~ProducerEditor() { }

void ProducerEditor::addProducer() {
    model->emitLayoutAboutToBeChanged();
    DB.NewProducer();
    model->emitLayoutChanged();
}

void ProducerEditor::removeProducer() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() == 0) return;
    unsigned int row = indexies[0].row();
    if (row >= DB.ProducersSize()) return;

    Producer *producer = DB.ProducerByIndex(row);
    model->emitLayoutAboutToBeChanged();
    DB.DeleteProducer(producer);
    model->emitLayoutChanged();

    selectionChanged();
}
void ProducerEditor::editProducer() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() == 0) return;
    unsigned int row = indexies[0].row();
    if (row >= DB.ProducersSize()) return;

    model->emitLayoutAboutToBeChanged();
    Producer *producer = DB.ProducerByIndex(row);
    producer->SetName(ui.nameEdit->text().toStdString());
    producer->SetURL(ui.urlEdit->text().toStdString());
    producer->SetShortDescription(ui.shortDescriptionEdit->toPlainText().toStdString());
    producer->SetDescription(ui.fullDescriptionEdit->toPlainText().toStdString());
    model->emitLayoutChanged();
}

void ProducerEditor::selectionChanged() {
    QModelIndexList indexies = view->selectionModel()->selectedIndexes();
    if (indexies.size() > 0 && DB.ProducersSize() > 0)
        ui.removeButton->setEnabled(true);
    else
        ui.removeButton->setEnabled(false);

    if (indexies.size() == 0) return;
    unsigned int row = indexies[0].row();
    if (row >= DB.ProducersSize()) return;

    Producer *producer = DB.ProducerByIndex(row);
    ui.nameEdit->setText(QString::fromStdString(producer->GetName()));
    ui.urlEdit->setText(QString::fromStdString(producer->GetURL()));
    ui.shortDescriptionEdit->setPlainText(QString::fromStdString(producer->GetShortDescription()));
    ui.fullDescriptionEdit->setPlainText(QString::fromStdString(producer->GetDescription()));
}



