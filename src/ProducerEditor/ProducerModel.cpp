#include "ProducerModel.hpp"
#include <algorithm>
#include <QDebug>
#include <QMimeData>
#include <QIcon>
#include "collection/collections.hpp"

#include "tools.hpp"

ProducerModel::ProducerModel(QObject *parent) : QAbstractListModel(parent) {
}

ProducerModel::~ProducerModel() { }

int ProducerModel::rowCount(const QModelIndex &) const {
    return DB.ProducersSize();
}

int ProducerModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent)
    return ColumnCount;
}

QVariant ProducerModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid()) return QVariant();
    if (index.row() >= DB.ProducersSize()) return QVariant();

    Producer *producer = DB.ProducerByIndex(index.row());
    switch (index.column()) {
    case NameColumn:
        switch (role) {
        case Qt::EditRole:
            return QVariant(QString::fromStdString(producer->GetName()));
        case Qt::DisplayRole:
            return QVariant(QString::fromStdString(producer->GetName()));
        default:
            return QVariant();
        }
        break;
    default:
        break;
    }
    return QVariant();
}

QVariant ProducerModel::headerData(int section, Qt::Orientation orientation, int role) const {
    const QStringList headers = {"Название"};
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole && section < headers.size()) {
        return headers[section];
    }
    return QVariant();
}

Qt::ItemFlags ProducerModel::flags(const QModelIndex &index) const {
    Qt::ItemFlags flags = QAbstractItemModel::flags(index);
    return flags | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled; // | Qt::ItemIsEditable;
}

Qt::DropActions ProducerModel::supportedDragActions() const {
    return Qt::MoveAction;
}
Qt::DropActions ProducerModel::supportedDropActions() const {
    return Qt::MoveAction;
}

QMimeData *ProducerModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = QAbstractItemModel::mimeData(indexes);
    if (indexes.size() == 1) {
        QModelIndex index = indexes[0];
        mimeData->setProperty("source_index", QVariant(index.row()));
    }
    return mimeData;
}

bool ProducerModel::dropMimeData(const QMimeData *mime, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if (action == Qt::IgnoreAction) return true;

    int source_index = mime->property("source_index").toInt();

    emitLayoutAboutToBeChanged();
    bool res = DB.ProducerMove(source_index, row);
    emitLayoutChanged();

    return res;
}
