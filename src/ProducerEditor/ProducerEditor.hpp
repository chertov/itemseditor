#pragma once
#include "ui_ProducerEditor.h"

#include <QtWidgets/QDialog>

#include "collection/collections.hpp"

#include "ProducerModel.hpp"

class ProducerEditor : public QDialog {
    Q_OBJECT
public:
    ProducerEditor(QWidget *parent = 0);
    ~ProducerEditor();

private:
    ProducerModel *model;
    QListView *view;

    Ui::ProducerEditor ui;
private slots:
    void addProducer();
    void removeProducer();
    void editProducer();
    void selectionChanged();
};
