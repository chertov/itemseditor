#-------------------------------------------------
#
# Project created by QtCreator 2015-12-12T00:27:16
#
#-------------------------------------------------

QT       += core gui

ROOT_DIRECTORY = $$PWD
BUILD_DIRECTORY = $${ROOT_DIRECTORY}/build
#$${LIB_DIRECTORY}
DESTDIR = $${ROOT_DIRECTORY}/../../bin/
OBJECTS_DIR = $${BUILD_DIRECTORY}
MOC_DIR = $${BUILD_DIRECTORY}
RCC_DIR = $${BUILD_DIRECTORY}
UI_DIR = $${BUILD_DIRECTORY}

greaterThan(QT_MAJOR_VERSION, 4) {
QT += widgets
QT += network
CONFIG += c++11
}else {
QMAKE_CXXFLAGS += -std=c++0x
DEFINES +="'Q_UNREACHABLE()=(void)1'"
}


APP_NAME = ItemsEditor
Debug: TARGET = $${APP_NAME}_d
Release: TARGET = $${APP_NAME}
TEMPLATE = app

ROOT_DIRECTORY = $$PWD
BUILD_DIRECTORY = $${ROOT_DIRECTORY}/build
#$${LIB_DIRECTORY}

DESTDIR = $${ROOT_DIRECTORY}/../../bin/
OBJECTS_DIR = $${BUILD_DIRECTORY}
MOC_DIR = $${BUILD_DIRECTORY}
RCC_DIR = $${BUILD_DIRECTORY}
UI_DIR = $${BUILD_DIRECTORY}

DEFINES += HAVE_STDINT_H

LIBS += -L$${ROOT_DIRECTORY}
LIBS += -lstdc++
#LIBS += C:/temp/protobuf-3.0.0-beta-2/bin/libprotobuf.a
LIBS += -lprotobuf
# LIBS += -L/libprotobuf.a #libprotobuf.a

INCLUDEPATH +=  ./../../src/jansson/ \
                ./../../src/ \
                ./../../src/protobuf/

HEADERS  += ./../../src/assert.hpp\
            ./../../src/collection/idtype.hpp\
            ./../../src/collection/collections.hpp\
            ./../../src/collection/base.hpp\
            ./../../src/collection/enumvalue.hpp\
            ./../../src/collection/enum.hpp\
            ./../../src/collection/propertyvalue.hpp\
            ./../../src/collection/property.hpp\
            ./../../src/collection/propertyset.hpp\
            ./../../src/collection/producer.hpp\
            ./../../src/collection/category.hpp\
            ./../../src/collection/folder.hpp\
            ./../../src/collection/item.hpp\
            ./../../src/collection/itemtype.hpp\
            ./../../src/collection/itemwalker.hpp\
            ./../../src/collection/image.hpp\
            ./../../src/collection/db.hpp\
            ./../../src/ModelItem.hpp\
            ./../../src/qteditor.hpp\
            ./../../src/DescriptionEditor/DescriptionEditor.hpp\
            ./../../src/ItemsTypesEditor/ItemsTypesEditor.hpp\
            ./../../src/ItemsTypesEditor/ItemsTypesModel.hpp\
            ./../../src/ProducerEditor/ProducerEditor.hpp\
            ./../../src/ProducerEditor/ProducerModel.hpp\
            ./../../src/PropertyEditor/PropertyEditor.hpp\
            ./../../src/PropertyEditor/EnumModel.hpp\
            ./../../src/PropertySetEditor/PropertySetEditor.hpp\
            ./../../src/PropertySetEditor/PropertySetModel.hpp\
            ./../../src/ItemsEditor/PropertyBoolDelegate.hpp\
            ./../../src/ItemsEditor/PropertyIntDelegate.hpp\
            ./../../src/ItemsEditor/PropertyRealDelegate.hpp\
            ./../../src/ItemsEditor/PropertyEnumDelegate.hpp\
            ./../../src/ItemsEditor/PropertyItemTypeDelegate.hpp\
            ./../../src/ItemsEditor/PropertyProducerDelegate.hpp\
            ./../../src/ItemsEditor/PropertyImageDelegate.hpp\
            ./../../src/ItemsEditor/ItemsModel.hpp\
            ./../../src/ItemsEditor/ItemPropertyModel.hpp\
            ./../../src/ItemsEditor/ItemPropertyWidget.hpp\
            ./../../src/ItemsEditor/ItemsEditor.hpp\
            ./../../src/ItemsTree/TreeModel.hpp\
            ./../../src/ItemsTree/TreeView.hpp\
            ./../../src/ItemsTree/ItemsTree.hpp\
            ./../../src/ImageEditor/ImageEditor.hpp\
            ./../../src/ImageEditor/ImagesList.hpp\
            ./../../src/ImageEditor/ImageModel.hpp\
            ./../../src/ImageEditor/ImagePreview.hpp\
            ./../../src/settings/SettingsEditor.hpp\
            ./../../src/parser.hpp\
            ./../../src/tools.hpp\
            ./../../src/logger.hpp\
            ./../../src/exception.hpp\
            ./../../src/jansson/lookup3.h\
            ./../../src/jansson/strbuffer.h\
            ./../../src/jansson/jansson.h\
            ./../../src/jansson/jansson_private.h\
            ./../../src/jansson/hashtable.h\
            ./../../src/jansson/utf.h\
            ./../../src/sqlite.hpp\
            ./../../src/sqlite/sqlite3.h\
            ./../../src/sqlite/sqlite3ext.h\
            ./../../src/protobuf/db.pb.h

SOURCES +=  ./../../src/main.cpp\
            ./../../src/collection/idtype.cpp\
            ./../../src/collection/base.cpp\
            ./../../src/collection/enumvalue.cpp\
            ./../../src/collection/enum.cpp\
            ./../../src/collection/propertyvalue.cpp\
            ./../../src/collection/property.cpp\
            ./../../src/collection/propertyset.cpp\
            ./../../src/collection/producer.cpp\
            ./../../src/collection/category.cpp\
            ./../../src/collection/folder.cpp\
            ./../../src/collection/item.cpp\
            ./../../src/collection/itemtype.cpp\
            ./../../src/collection/image.cpp\
            ./../../src/collection/db.cpp\
            ./../../src/DescriptionEditor/DescriptionEditor.cpp\
            ./../../src/ItemsTypesEditor/ItemsTypesEditor.cpp\
            ./../../src/ItemsTypesEditor/ItemsTypesModel.cpp\
            ./../../src/ProducerEditor/ProducerEditor.cpp\
            ./../../src/ProducerEditor/ProducerModel.cpp\
            ./../../src/PropertyEditor/PropertyEditor.cpp\
            ./../../src/PropertyEditor/EnumModel.cpp\
            ./../../src/PropertySetEditor/PropertySetEditor.cpp\
            ./../../src/PropertySetEditor/PropertySetModel.cpp\
            ./../../src/ItemsEditor/PropertyBoolDelegate.cpp\
            ./../../src/ItemsEditor/PropertyIntDelegate.cpp\
            ./../../src/ItemsEditor/PropertyRealDelegate.cpp\
            ./../../src/ItemsEditor/PropertyEnumDelegate.cpp\
            ./../../src/ItemsEditor/PropertyItemTypeDelegate.cpp\
            ./../../src/ItemsEditor/PropertyProducerDelegate.cpp\
            ./../../src/ItemsEditor/PropertyImageDelegate.cpp\
            ./../../src/ItemsEditor/ItemsModel.cpp\
            ./../../src/ItemsEditor/ItemPropertyModel.cpp\
            ./../../src/ItemsEditor/ItemPropertyWidget.cpp\
            ./../../src/ItemsEditor/ItemsEditor.cpp\
            ./../../src/ItemsTree/TreeModel.cpp\
            ./../../src/ItemsTree/TreeView.cpp\
            ./../../src/ItemsTree/ItemsTree.cpp\
            ./../../src/ImageEditor/ImageEditor.cpp\
            ./../../src/ImageEditor/ImagesList.cpp\
            ./../../src/ImageEditor/ImageModel.cpp\
            ./../../src/ImageEditor/ImagePreview.cpp\
            ./../../src/settings/SettingsEditor.cpp\
            ./../../src/qteditor.cpp\
            ./../../src/parser.cpp\
            ./../../src/tools.cpp\
            ./../../src/logger.cpp\
            ./../../src/exception.cpp\
            ./../../src/jansson/dump.c\
            ./../../src/jansson/hashtable_seed.c\
            ./../../src/jansson/value.c\
            ./../../src/jansson/error.c\
            ./../../src/jansson/memory.c\
            ./../../src/jansson/strconv.c\
            ./../../src/jansson/hashtable.c\
            ./../../src/jansson/pack_unpack.c\
            ./../../src/jansson/utf.c\
            ./../../src/jansson/load.c\
            ./../../src/jansson/strbuffer.c\
            ./../../src/sqlite.cpp\
            ./../../src/sqlite/sqlite3.c\
            ./../../src/protobuf/db.pb.cc

FORMS    += ./../../src/qteditor.ui\
            ./../../src/propertyEditor/PropertyEditor.ui\
            ./../../src/PropertySetEditor/PropertySetEditor.ui\
            ./../../src/ItemsEditor/ItemsEditor.ui\
            ./../../src/ItemsEditor/ItemPropertyWidget.ui\
            ./../../src/ImageEditor/ImageEditor.ui\
            ./../../src/ImageEditor/ImagesList.ui\
            ./../../src/ImageEditor/ImagePreview.ui\
            ./../../src/ItemsTree/ItemsTree.ui\
            ./../../src/DescriptionEditor/DescriptionEditor.ui\
            ./../../src/ProducerEditor/ProducerEditor.ui\
            ./../../src/ItemsTypesEditor/ItemsTypesEditor.ui\
            ./../../src/settings/SettingsEditor.ui

OTHER_FILES += ./../../src/protobuf/db.proto
